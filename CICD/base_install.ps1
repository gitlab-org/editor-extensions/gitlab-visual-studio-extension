choco install visualstudio2022-workload-visualstudioextensionbuildtools --force --passive -y

# Add Language Server repository as NuGet package source
& C:\ProgramData\chocolatey\lib\nuget.commandline\tools\nuget.exe sources add `
  -Name "languageServerSource" `
  -Source "https://gitlab.com/api/v4/projects/46519181/packages/nuget/index.json"

& C:\ProgramData\chocolatey\lib\nuget.commandline\tools\nuget.exe install MSBuildTasks
& C:\ProgramData\chocolatey\lib\nuget.commandline\tools\nuget.exe restore