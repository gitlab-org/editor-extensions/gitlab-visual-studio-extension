
if ($env:CI_COMMIT_BRANCH -and $env:CI_COMMIT_BRANCH -eq $env:CI_DEFAULT_BRANCH) {
  Write-Output "On default branch '${env:CI_DEFAULT_BRANCH}', pull in latest changes so we have an updated vsixmanifest."
  git branch
  git checkout main
  git pull
} else {
  Write-Output "On feature branch '${env:CI_COMMIT_BRANCH}', skipping git commands."
}

# Print out the vsixmanifest
cat .\GitLab.Extension\source.extension.vsixmanifest

# Build with Debug target for tests
& $env:MSBUILD_PATH GitLab.Extension.sln `
  /target:Clean `
  /target:Build `
  /p:DeployExtension=false `
  /p:Configuration=Debug
$good_exit_status = $?
if ($good_exit_status) {
    # noop - continue on to build Release version
  } else {
    exit 1
  }

# Build with Release for the VSIX installer
& $env:MSBUILD_PATH GitLab.Extension.sln `
  /target:Clean `
  /target:Build `
  /p:DeployExtension=false `
  /p:Configuration=Release
$good_exit_status = $?
if ($good_exit_status) {
  exit 0
} else {
  exit 1
}