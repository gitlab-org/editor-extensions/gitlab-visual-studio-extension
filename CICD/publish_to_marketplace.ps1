Write-Output "Please wait a few minutes for the Visual Studio Extension Marketplace to verify this upload before the new version of the extension goes live."
& "C:\Program Files (x86)\Microsoft Visual Studio\2022\BuildTools\VSSDK\VisualStudioIntegration\Tools\Bin\VsixPublisher.exe" publish -payload .\GitLab.Extension\bin\Release\GitLab.Extension.vsix -publishManifest .\vs-publish.json -personalAccessToken $env:VISUAL_STUDIO_MARKETPLACE_DEPLOY_TOKEN
$good_exit_status = $?
if ($good_exit_status) {
  exit 0
} else {
  exit 1
}
