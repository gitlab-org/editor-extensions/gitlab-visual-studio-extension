using System;
using System.Diagnostics.CodeAnalysis;
using System.Threading.Tasks;
using GitLab.Extension.CodeSuggestions;
using GitLab.Extension.CodeSuggestions.Model;
using GitLab.Extension.LanguageServer;
using GitLab.Extension.Workspace;
using Moq;
using NUnit.Framework;
using Serilog;

namespace GitLab.Extension.Tests.CodeSuggestions
{
    [TestFixture]
    [SuppressMessage("Style", "VSTHRD200:Use \"Async\" suffix for async methods")]
    public class GitLabCodeSuggestionTelemetryControllerTests
    {
        private const string TestTelemetryId = "test-telemetry-id";
        private const int OptionId = 2;
        
        private Mock<IWorkspaceLsClientProvider> _mockLsClientProvider;
        private Mock<ILsClient> _mockLsClient;
        private Mock<ILogger> _mockLogger;
        private GitLabCodeSuggestionTelemetryController _controller;

        private static readonly WorkspaceId _testWorkspaceId =
            new WorkspaceId(
                "TestSolution",
                "path/to/solution");
        
        private static readonly Completion _completion = new Completion(TestTelemetryId, "Hello world", "stream-id-1", OptionId);
        
        [SetUp]
        public void SetUp()
        {
            _mockLsClientProvider = new Mock<IWorkspaceLsClientProvider>();
            _mockLsClient = new Mock<ILsClient>();
            _mockLogger = new Mock<ILogger>();
            _controller = new GitLabCodeSuggestionTelemetryController(_mockLsClientProvider.Object, _mockLogger.Object);
            
            _mockLsClientProvider.Setup(p => p.GetClientAsync(It.IsAny<WorkspaceId>())).ReturnsAsync(_mockLsClient.Object);
        }
        
        [Test]
        public async Task OnShownAsync_ShouldSendShownTelemetry()
        {
            // Act
            await _controller.OnShownAsync(_completion.UniqueTrackingId, _testWorkspaceId);

            // Assert
            _mockLsClient.Verify(client => client.SendGitlabTelemetryCodeSuggestionShownAsync(TestTelemetryId), Times.Once);
            _mockLogger.VerifyNoOtherCalls();
        }
        
        [Test]
        public async Task OnRejectedAsync_AfterOnShownAsync_ShouldSendRejectedTelemetry()
        {
            // Arrange
            await _controller.OnShownAsync(_completion.UniqueTrackingId, _testWorkspaceId);
            
            // Act
            await _controller.OnRejectedAsync(_completion.UniqueTrackingId, _testWorkspaceId);

            // Assert
            _mockLsClient.Verify(client => client.SendGitlabTelemetryCodeSuggestionRejectedAsync(TestTelemetryId), Times.Once);
            _mockLogger.VerifyNoOtherCalls();
        }
        
        [Test]
        public async Task OnAcceptedAsync_AfterOnShownAsync_ShouldSendAcceptedTelemetry()
        {
            // Arrange
            await _controller.OnShownAsync(_completion.UniqueTrackingId, _testWorkspaceId);
            
            // Act
            await _controller.OnAcceptedAsync(_completion.UniqueTrackingId, _testWorkspaceId, _completion.OptionId);

            // Assert
            _mockLsClient.Verify(client => client.SendGitlabTelemetryCodeSuggestionAcceptedAsync(TestTelemetryId, OptionId), Times.Once);
            _mockLogger.VerifyNoOtherCalls();
        }
        
        [Test]
        public async Task OnShownAsync_WhenExceptionOccurs_ShouldLogError()
        {
            // Arrange
            _mockLsClient.Setup(client => client.SendGitlabTelemetryCodeSuggestionShownAsync(It.IsAny<string>())).ThrowsAsync(new Exception("Test exception"));
            
            // Act
            await _controller.OnShownAsync(_completion.UniqueTrackingId, _testWorkspaceId);

            // Assert
            _mockLogger.Verify(logger => logger.Error(It.IsAny<Exception>(), It.IsAny<string>(), TestTelemetryId), Times.Once);
        }
        
        [Test]
        public async Task OnShownAndAcceptedAsync_ForSameProposal_ShouldProcessBoth()
        {
            // Act
            await _controller.OnShownAsync(_completion.UniqueTrackingId, _testWorkspaceId);
            await _controller.OnAcceptedAsync(_completion.UniqueTrackingId, _testWorkspaceId, _completion.OptionId);

            // Assert
            _mockLsClient.Verify(client => client.SendGitlabTelemetryCodeSuggestionShownAsync(TestTelemetryId), Times.Once);
            _mockLsClient.Verify(client => client.SendGitlabTelemetryCodeSuggestionAcceptedAsync(TestTelemetryId, OptionId), Times.Once);
        }
    }
}
