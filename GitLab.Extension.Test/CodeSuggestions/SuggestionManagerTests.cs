using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using GitLab.Extension.CodeSuggestions;
using GitLab.Extension.CodeSuggestions.Model;
using GitLab.Extension.LanguageServer;
using GitLab.Extension.LanguageServer.Models;
using GitLab.Extension.Status;
using GitLab.Extension.Status.Models;
using GitLab.Extension.Utility.Results;
using GitLab.Extension.Workspace;
using GitLab.Extension.Workspace.Model;
using Microsoft.VisualStudio.Shell;
using Microsoft.VisualStudio.Text;
using Microsoft.VisualStudio.Text.Editor;
using Microsoft.VisualStudio.Text.Formatting;
using Moq;
using NUnit.Framework;
using Serilog;
using CompletionItem = GitLab.Extension.LanguageServer.Models.CompletionItem;

namespace GitLab.Extension.Tests.CodeSuggestions
{
    [TestFixture]
    public class SuggestionManagerTests
    {
        private const string InsertText = "Bits flow endlessly\n Data river, swift and bright\n Pixels bloom downstream";
        private const string StreamId = "stream-1";
        private const string UniqueRequestId = "unique-id-1";
        
        private Mock<ILsClient> _mockLsClient;
        private Mock<IWorkspaceLsClientProvider> _mockWorkspaceLsClientProvider;
        private Mock<IGitLabCodeSuggestionTelemetryController> _mockTelemetryController;
        private Mock<IFeatureStateManager> _mockFeatureStateManager;
        private Mock<ISuggestionsAdornmentsFactory> _mockSuggestionsAdornmentsFactory;
        private Mock<ISuggestionsAdornments> _mockSuggestionsAdornments;
        private Mock<ILogger> _mockLogger;
        
        private ISuggestionEventBroker _eventBroker;
        private TextDocumentInfo _textDocumentInfo;
        
        [SetUp]
        public void SetUp()
        {
            _eventBroker = new SuggestionEventBroker();
            _mockLogger = new Mock<ILogger>();
            _mockLogger.Setup(m => m.ForContext<SuggestionManager>()).Returns(_mockLogger.Object);
            _mockLsClient = new Mock<ILsClient>();
            _mockWorkspaceLsClientProvider = new Mock<IWorkspaceLsClientProvider>();
            _mockTelemetryController = new Mock<IGitLabCodeSuggestionTelemetryController>();
            _mockFeatureStateManager = new Mock<IFeatureStateManager>();
            _mockSuggestionsAdornmentsFactory = new Mock<ISuggestionsAdornmentsFactory>();
            _mockSuggestionsAdornments = new Mock<ISuggestionsAdornments>();
            _mockSuggestionsAdornmentsFactory.Setup(m => m.Create(It.IsAny<IWpfTextView>(), It.IsAny<ISuggestionEventBroker>())).Returns(_mockSuggestionsAdornments.Object);
            _textDocumentInfo = new TextDocumentInfo(GetTextViewMock().Object, new WorkspaceId("TestApp1", "path/to/app"), string.Empty, string.Empty);
        }
        
        [Test]
        public async Task GetFirstCompletionsAsync_WithFeatureNotAvailable_ShouldReturnNothing()
        {
            // Arrange
            await ThreadHelper.JoinableTaskFactory.SwitchToMainThreadAsync();
            
            var suggestionManager = new SuggestionManager(
                _textDocumentInfo,
                _mockWorkspaceLsClientProvider.Object,
                _mockTelemetryController.Object,
                _eventBroker,
                _mockFeatureStateManager.Object,
                _mockSuggestionsAdornmentsFactory.Object,
                _mockLogger.Object);
            
            SetupClientForNonStreaming();
            _mockFeatureStateManager.Setup(s => s.IsAvailable(Feature.CODE_SUGGESTIONS)).Returns(false);
            using var cts = new CancellationTokenSource(TimeSpan.FromSeconds(120));

            // Act
            var result = await suggestionManager.GetFirstCompletionsAsync(cts.Token);

            // Assert
            Assert.IsNotNull(result);
            Assert.IsEmpty(result);

            _mockLsClient.Verify(client => client.SendTextDocumentCompletionAsync(
                It.IsAny<string>(),
                It.IsAny<uint>(),
                It.IsAny<uint>(),
                CompletionTriggerKind.Automatic,
                It.IsAny<CancellationToken>()), Times.Never);
        }

        [Test]
        public async Task GetFirstCompletionsAsync_WithNonStreamingCompletions_ShouldReturnSuggestion()
        {
            // Arrange
            await ThreadHelper.JoinableTaskFactory.SwitchToMainThreadAsync();
            
            var suggestionManager = new SuggestionManager(
                _textDocumentInfo,
                _mockWorkspaceLsClientProvider.Object,
                _mockTelemetryController.Object,
                _eventBroker,
                _mockFeatureStateManager.Object,
                _mockSuggestionsAdornmentsFactory.Object,
                _mockLogger.Object);
            
            SetupClientForNonStreaming();
            _mockFeatureStateManager.Setup(s => s.IsAvailable(Feature.CODE_SUGGESTIONS)).Returns(true);
            using var cts = new CancellationTokenSource(TimeSpan.FromSeconds(120));

            // Act
            var result = await suggestionManager.GetFirstCompletionsAsync(cts.Token);

            // Assert
            Assert.IsNotNull(result);
            Assert.IsNotEmpty(result);
            Assert.IsFalse(result[0].IsStreaming);
            Assert.AreEqual(InsertText, result[0].InsertText);
            Assert.AreEqual(UniqueRequestId, result[0].UniqueTrackingId);

            _mockLsClient.Verify(client => client.SendTextDocumentCompletionAsync(
                It.IsAny<string>(),
                It.IsAny<uint>(),
                It.IsAny<uint>(),
                CompletionTriggerKind.Automatic,
                It.IsAny<CancellationToken>()), Times.Once);
            
            _mockSuggestionsAdornments.Verify(adornment => adornment.RenderAdornment(It.IsAny<SuggestionDisplaySession>()), Times.AtLeast(1));
        }
        
        [Test]
        public async Task GetFirstCompletionsAsync_WithStreamingCompletions_ShouldReturnChunk()
        {
            // Arrange
            await ThreadHelper.JoinableTaskFactory.SwitchToMainThreadAsync();
            
            var suggestionManager = new SuggestionManager(
                _textDocumentInfo,
                _mockWorkspaceLsClientProvider.Object,
                _mockTelemetryController.Object,
                _eventBroker,
                _mockFeatureStateManager.Object,
                _mockSuggestionsAdornmentsFactory.Object,
                _mockLogger.Object);
            
            SetupClientForStreaming();
            _mockFeatureStateManager.Setup(s => s.IsAvailable(Feature.CODE_SUGGESTIONS)).Returns(true);
            using var cts = new CancellationTokenSource(TimeSpan.FromSeconds(5));

            // Act
            var task = suggestionManager.GetFirstCompletionsAsync(cts.Token);
            
            // simulate chunk received later
            Task.Run(async () =>
            {
                await Task.Delay(100);
                _eventBroker.Publish(new SuggestionUpdatedEvent(new CompletionChunk(InsertText, StreamId, false)));
            });

            var result = await task;

            // Assert
            Assert.IsNotNull(result);
            Assert.IsNotEmpty(result);
            Assert.IsTrue(result[0].IsStreaming);
            Assert.AreEqual(InsertText, result[0].InsertText);
            Assert.AreEqual(UniqueRequestId, result[0].UniqueTrackingId);

            _mockLsClient.Verify(client => client.SendTextDocumentCompletionAsync(
                It.IsAny<string>(),
                It.IsAny<uint>(),
                It.IsAny<uint>(),
                CompletionTriggerKind.Automatic,
                It.IsAny<CancellationToken>()), Times.Once);
        }
        
        [Test]
        public async Task GetFirstCompletionsAsync_WithNonStreamingCompletions_ShouldRequestForMoreSuggestions()
        {
            // Arrange
            await ThreadHelper.JoinableTaskFactory.SwitchToMainThreadAsync();
            
            var suggestionManager = new SuggestionManager(
                _textDocumentInfo,
                _mockWorkspaceLsClientProvider.Object,
                _mockTelemetryController.Object,
                _eventBroker,
                _mockFeatureStateManager.Object,
                _mockSuggestionsAdornmentsFactory.Object,
                _mockLogger.Object);
            
            SetupClientForNonStreaming();
            _mockFeatureStateManager.Setup(s => s.IsAvailable(Feature.CODE_SUGGESTIONS)).Returns(true);
            using var cts = new CancellationTokenSource(TimeSpan.FromSeconds(5));

            // Act
           _ = await suggestionManager.GetFirstCompletionsAsync(cts.Token);
            
            // simulate first suggestion displayed to user
            Task.Run(async () =>
            {
                _eventBroker.Publish(new SuggestionStateShownEvent(_textDocumentInfo.TextView, UniqueRequestId, 1, null));
            });
            
            // ensure we sent request for another suggestion
            await Task.Delay(100);

            // Assert
            _mockLsClient.Verify(client => client.SendTextDocumentCompletionAsync(
                It.IsAny<string>(),
                It.IsAny<uint>(),
                It.IsAny<uint>(),
                CompletionTriggerKind.Automatic,
                It.IsAny<CancellationToken>()), Times.Exactly(1));
            _mockLsClient.Verify(client => client.SendTextDocumentCompletionAsync(
                It.IsAny<string>(),
                It.IsAny<uint>(),
                It.IsAny<uint>(),
                CompletionTriggerKind.Invoked,
                It.IsAny<CancellationToken>()), Times.Exactly(1));
        }
        
        [Test]
        public async Task GetFirstCompletionsAsync_WithStreamingCompletions_ShouldNotRequestForMoreSuggestions()
        {
            // Arrange
            await ThreadHelper.JoinableTaskFactory.SwitchToMainThreadAsync();
            
            var suggestionManager = new SuggestionManager(
                _textDocumentInfo,
                _mockWorkspaceLsClientProvider.Object,
                _mockTelemetryController.Object,
                _eventBroker,
                _mockFeatureStateManager.Object,
                _mockSuggestionsAdornmentsFactory.Object,
                _mockLogger.Object);
            
            SetupClientForStreaming();
            _mockFeatureStateManager.Setup(s => s.IsAvailable(Feature.CODE_SUGGESTIONS)).Returns(true);
            using var cts = new CancellationTokenSource(TimeSpan.FromSeconds(5));

            // Act
            var task = suggestionManager.GetFirstCompletionsAsync(cts.Token);
            
            // simulate first chunk and suggestion displayed to user
            Task.Run(async () =>
            {
                await Task.Delay(100);
                _eventBroker.Publish(new SuggestionUpdatedEvent(new CompletionChunk(InsertText, StreamId, false)));
                await Task.Delay(100);
                _eventBroker.Publish(new SuggestionStateShownEvent(_textDocumentInfo.TextView, UniqueRequestId, 1, null));
            });

            await task;
            
            // ensure we sent request for another suggestion
            await Task.Delay(100);

            // Assert
            _mockLsClient.Verify(client => client.SendTextDocumentCompletionAsync(
                It.IsAny<string>(),
                It.IsAny<uint>(),
                It.IsAny<uint>(),
                CompletionTriggerKind.Automatic,
                It.IsAny<CancellationToken>()), Times.Exactly(1));
            _mockLsClient.Verify(client => client.SendTextDocumentCompletionAsync(
                It.IsAny<string>(),
                It.IsAny<uint>(),
                It.IsAny<uint>(),
                CompletionTriggerKind.Invoked,
                It.IsAny<CancellationToken>()), Times.Never);
        }

        private void SetupClientForNonStreaming()
        {
            _mockLsClient.Setup(client => client.SendTextDocumentCompletionAsync(
                It.IsAny<string>(),
                It.IsAny<uint>(),
                It.IsAny<uint>(),
                It.IsAny<CompletionTriggerKind>(),
                It.IsAny<CancellationToken>()))
            .ReturnsAsync(Result.Ok(new CompletionItemList()
            {
                Items = new List<CompletionItem>
                {
                    new CompletionItem
                    {
                        InsertText = InsertText,
                        Command = new Extension.LanguageServer.Models.Command()
                        {
                            CommandName = "gitlab.ls.codeSuggestionAccepted",
                            Arguments = new List<object>() { UniqueRequestId }
                        }
                    }
                }
            }));

            _mockWorkspaceLsClientProvider.Setup(provider => provider.GetClientAsync(It.IsAny<WorkspaceId>()))
                .ReturnsAsync(_mockLsClient.Object);
        }
        
        private void SetupClientForStreaming()
        {
            var items = 
            _mockLsClient.Setup(client => client.SendTextDocumentCompletionAsync(
                    It.IsAny<string>(),
                    It.IsAny<uint>(),
                    It.IsAny<uint>(),
                    It.IsAny<CompletionTriggerKind>(),
                    It.IsAny<CancellationToken>()))
                .ReturnsAsync(Result.Ok(new CompletionItemList()
                {
                    Items = new List<CompletionItem>
                    {
                        new CompletionItem
                        {
                            InsertText = InsertText,
                            Command = new Extension.LanguageServer.Models.Command()
                            {
                                CommandName = "gitlab.ls.startStreaming",
                                Arguments = new List<object> { StreamId, UniqueRequestId }
                            }
                        }
                    }
                }));

            _mockWorkspaceLsClientProvider.Setup(provider => provider.GetClientAsync(It.IsAny<WorkspaceId>()))
                .ReturnsAsync(_mockLsClient.Object);
        }

        private Mock<IWpfTextView> GetTextViewMock()
        {
            var mockTextView = new Mock<IWpfTextView>();
            mockTextView.Setup(tv => tv.Caret).Returns(Mock.Of<ITextCaret>());
            
            var textSnapshotMock = new Mock<ITextSnapshot>();
            var textSnapshotLineMock = new Mock<ITextSnapshotLine>();
            textSnapshotMock.Setup(m => m.GetLineFromPosition(It.IsAny<int>())).Returns(textSnapshotLineMock.Object);
            textSnapshotLineMock.Setup(m => m.LineNumber).Returns(0);
            textSnapshotLineMock.Setup(m => m.Start).Returns(new SnapshotPoint(textSnapshotMock.Object, 0));
            
            mockTextView.Setup(tv => tv.Caret.Position).Returns(
                new CaretPosition(
                    new VirtualSnapshotPoint(new SnapshotPoint(textSnapshotMock.Object, 0)), 
                    Mock.Of<IMappingPoint>(),
                    PositionAffinity.Successor));
            mockTextView.Setup(tv => tv.Caret.ContainingTextViewLine).Returns(Mock.Of<ITextViewLine>());
            
            var mockAdornmentLayer = new Mock<IAdornmentLayer>();
            mockTextView.Setup(tv => tv.GetAdornmentLayer(It.IsAny<string>())).Returns(mockAdornmentLayer.Object);
            var formattedLineSourceMock = new Mock<IFormattedLineSource>();
            formattedLineSourceMock.Setup(fts => fts.DefaultTextProperties.FontRenderingEmSize).Returns(12.0f);
            mockTextView.Setup(tv => tv.FormattedLineSource).Returns(formattedLineSourceMock.Object);
            
            var wpfTextLineMock = new Mock<IWpfTextViewLine>();
            mockTextView.Setup(tv => tv.GetTextViewLineContainingBufferPosition(It.IsAny<SnapshotPoint>())).Returns(wpfTextLineMock.Object);
            wpfTextLineMock.Setup(l => l.GetCharacterBounds(It.IsAny<SnapshotPoint>())).Returns(new TextBounds());

            return mockTextView;
        }
    }
}
