using System;
using System.Collections.Generic;
using GitLab.Extension.CodeSuggestions;
using GitLab.Extension.CodeSuggestions.Model;
using Microsoft.VisualStudio.Language.Proposals;
using Microsoft.VisualStudio.Language.Suggestions;
using Microsoft.VisualStudio.Text;
using Microsoft.VisualStudio.Text.Editor;
using Moq;
using NUnit.Framework;
using Serilog.Core;

#pragma warning disable CS0618 // Type or member is obsolete

namespace GitLab.Extension.Tests.CodeSuggestions
{
    [TestFixture]
    public class SuggestionServiceListenerTests
    {
        private Mock<SuggestionServiceBase> _mockSuggestionServiceBase;
        private Mock<ISuggestionEventBroker> _mockEventBroker;
        private Mock<IWpfTextView> _mockTextView;

        // ReSharper disable once NotAccessedField.Local
        // we only need to keep this reference around so that the GC doesn't collect
        private SuggestionServiceListener _adaptor;

        private static readonly string ProviderName = "test-provider-name";

        private static readonly GitLabProposal TestProposal =
            new GitLabProposal(
                new GitLabProposalMetadata("test-telemetry-id"),
                null,
                new List<ProposedEdit>(),
                new VirtualSnapshotPoint());

        [SetUp]
        public void SetUp()
        {
            _mockSuggestionServiceBase = new Mock<SuggestionServiceBase>();
            _mockTextView = new Mock<IWpfTextView>();
            _mockEventBroker = new Mock<ISuggestionEventBroker>();
            _adaptor = new SuggestionServiceListener(_mockEventBroker.Object, Logger.None);
            _adaptor.Initialize(_mockSuggestionServiceBase.Object);
        }

        // generate tests for SuggestionServiceListener.cs
        [Test]
        public void Initialize_ShouldRegisterEventHandlers()
        {
            // Arrange
            var mockSuggestionServiceBase = new Mock<SuggestionServiceBase>();

            // Act
            _adaptor.Initialize(mockSuggestionServiceBase.Object);

            // Assert
            mockSuggestionServiceBase.VerifyAdd(
                x => x.ProposalDisplayed += It.IsAny<EventHandler<ProposalDisplayedEventArgs>>(), Times.Once);
            mockSuggestionServiceBase.VerifyAdd(
                x => x.ProposalRejected += It.IsAny<EventHandler<ProposalRejectedEventArgs>>(), Times.Once);
            mockSuggestionServiceBase.VerifyAdd(
                x => x.SuggestionAccepted += It.IsAny<EventHandler<SuggestionAcceptedEventArgs>>(), Times.Once);
            mockSuggestionServiceBase.VerifyAdd(
                x => x.SuggestionDismissed += It.IsAny<EventHandler<SuggestionDismissedEventArgs>>(), Times.Once);
        }

        [Test]
        public void HandleProposalDisplayed_WithGitLabProposal_ShouldPublishShownEvent()
        {
            // Arrange
            var mockView = new Mock<IWpfTextView>();
            var args = new ProposalDisplayedEventArgs(ProviderName, mockView.Object, TestProposal);

            // Act
            _mockSuggestionServiceBase.Raise(x => x.ProposalDisplayed += null, args);

            // Assert
            _mockEventBroker.Verify(x => x.Publish(It.Is<SuggestionStateShownEvent>(e =>
                e.TextView == mockView.Object &&
                e.UniqueTrackingId == TestProposal.GitLabProposalMetadata.UniqueRequestId &&
                e.OptionId == TestProposal.GitLabProposalMetadata.OptionId &&
                e.StreamId == TestProposal.GitLabProposalMetadata.StreamId)), Times.Once);
        }
        
        [Test]
        public void HandleProposalRejected_WithGitLabProposal_ShouldPublishRejectedEvent()
        {
            // Arrange
            var args = new ProposalRejectedEventArgs(ProviderName, _mockTextView.Object, TestProposal, TestProposal, ReasonForUpdate.Diverged);

            // Act
            _mockSuggestionServiceBase.Raise(x => x.ProposalRejected += null, args);

            // Assert
            _mockEventBroker.Verify(x => x.Publish(It.Is<SuggestionStateRejectedEvent>(e =>
                e.TextView == _mockTextView.Object &&
                e.UniqueTrackingId == null &&
                e.OptionId == null &&
                e.StreamId == null)), Times.Once);
        }

        [Test]
        public void HandleSuggestionAccepted_WithGitLabProposal_ShouldPublishAcceptedEvent()
        {
            // Arrange
            var args = new SuggestionAcceptedEventArgs(ProviderName, _mockTextView.Object, TestProposal, TestProposal, ReasonForAccept.AcceptedByCommand);

            // Act
            _mockSuggestionServiceBase.Raise(x => x.SuggestionAccepted += null, args);

            // Assert
            _mockEventBroker.Verify(x => x.Publish(It.Is<SuggestionStateAcceptedEvent>(e =>
                e.TextView == _mockTextView.Object &&
                e.UniqueTrackingId == null &&
                e.OptionId == null &&
                e.StreamId == null)), Times.Once);
        }

        [Test]
        public void HandleSuggestionDismissed_WithGitLabProposal_ShouldPublishDismissedEvent()
        {
            // Arrange
            var args = new SuggestionDismissedEventArgs(ProviderName, _mockTextView.Object, TestProposal, TestProposal, ReasonForDismiss.DismissedAfterBackspace);

            // Act
            _mockSuggestionServiceBase.Raise(x => x.SuggestionDismissed += null, args);

            // Assert
            _mockEventBroker.Verify(x => x.Publish(It.Is<SuggestionStateDismissedEvent>(e =>
                e.TextView == _mockTextView.Object &&
                e.UniqueTrackingId == null &&
                e.OptionId == null &&
                e.StreamId == null)), Times.Once);
        }
        
        [Test]
        public void Dispose_ShouldUnregisterEventHandlers()
        {
            // Arrange
            var mockSuggestionServiceBase = new Mock<SuggestionServiceBase>();
            var listener = new SuggestionServiceListener(_mockEventBroker.Object, Logger.None);
            listener.Initialize(mockSuggestionServiceBase.Object);

            // Act
            listener.Dispose();

            // Assert
            mockSuggestionServiceBase.VerifyRemove(
                x => x.ProposalDisplayed -= It.IsAny<EventHandler<ProposalDisplayedEventArgs>>(), Times.Once);
            mockSuggestionServiceBase.VerifyRemove(
                x => x.ProposalRejected -= It.IsAny<EventHandler<ProposalRejectedEventArgs>>(), Times.Once);
            mockSuggestionServiceBase.VerifyRemove(
                x => x.SuggestionAccepted -= It.IsAny<EventHandler<SuggestionAcceptedEventArgs>>(), Times.Once);
            mockSuggestionServiceBase.VerifyRemove(
                x => x.SuggestionDismissed -= It.IsAny<EventHandler<SuggestionDismissedEventArgs>>(), Times.Once);
        }
    }
}
