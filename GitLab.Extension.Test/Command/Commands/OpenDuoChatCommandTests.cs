﻿using GitLab.Extension.Command;
using Microsoft.VisualStudio.Shell;
using Moq;
using NUnit.Framework;
using System;
using System.ComponentModel.Design;
using System.Threading.Tasks;
using GitLab.Extension.Webviews;
using GitLab.Extension.Webviews.DuoChat;

namespace GitLab.Extension.Tests.Command.Commands
{
    [TestFixture]
    public class OpenDuoChatCommandTests
    {
        private Mock<AsyncPackage> _mockAsyncPackage;
        private Mock<IServiceProvider> _mockPackageServiceProvider;
        private Mock<IMenuCommandService> _mockCommandService;
        private Mock<IWebviewController<DuoChatToolWindow>> _duoChatController;
        private OpenDuoChatCommand _command;

        [SetUp]
        public void SetUp()
        {
            _duoChatController = new Mock<IWebviewController<DuoChatToolWindow>>();
            _mockPackageServiceProvider = new Mock<IServiceProvider>();
            _mockCommandService = new Mock<IMenuCommandService>();

            _mockPackageServiceProvider
                .Setup(x => x.GetService(It.Is<Type>(p => p == typeof(IMenuCommandService))))
                .Returns(_mockCommandService.Object);

            _command = new OpenDuoChatCommand(_duoChatController.Object);
        }

        [Test]
        public async Task InitializeAsync_AddCommandToMenuAsync()
        {
            // Act
            await _command.InitializeAsync(_mockPackageServiceProvider.Object);

            // Assert
            _mockCommandService.Verify(x => x.AddCommand(
                It.Is<OleMenuCommand>(omc => omc.CommandID.Equals(_command.CommandID))),
                Times.Once
            );
        }
    }
}
