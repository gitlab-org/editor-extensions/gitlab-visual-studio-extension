﻿using GitLab.Extension.Command;
using GitLab.Extension.SettingsUtil;
using Microsoft.VisualStudio.Shell;
using Moq;
using NUnit.Framework;
using System;
using System.ComponentModel.Design;
using System.Threading.Tasks;

namespace GitLab.Extension.Tests.Command.Commands
{
    [TestFixture]
    public class ToggleCodeSuggestionsCommandTests
    {
        private Mock<ISettings> _mockSettings;
        private Mock<IServiceProvider> _mockPackageServiceProvider;
        private Mock<IMenuCommandService> _mockCommandService;
        private ToggleCodeSuggestionsCommand _command;

        [SetUp]
        public void SetUp()
        {
            _mockSettings = new Mock<ISettings>();
            _mockPackageServiceProvider = new Mock<IServiceProvider>();
            _mockCommandService = new Mock<IMenuCommandService>();

            _mockPackageServiceProvider
                .Setup(x => x.GetService(It.Is<Type>(p => p == typeof(IMenuCommandService))))
                .Returns(_mockCommandService.Object);

            _command = new ToggleCodeSuggestionsCommand(_mockSettings.Object);
        }

        [Test]
        public async Task InitializeAsync_AddCommandToMenuAsync()
        {
            // Act
            await _command.InitializeAsync(_mockPackageServiceProvider.Object);

            // Assert
            _mockCommandService.Verify(x => x.AddCommand(
                It.Is<OleMenuCommand>(omc => omc.CommandID.Equals(_command.CommandID))),
                Times.Once
            );
        }
    }
}
