﻿using NUnit.Framework;
using GitLab.Extension.CodeSuggestions;
using GitLab.Extension.LanguageServer;

namespace GitLab.Extension.Tests
{
    [TestFixture]
    internal class LanguageManagerTests
    {
        [Test]
        public void CheckCodeSuggestionsTest()
        {
            var lang1 = new Language("C#", "csharp", LsLanguageId.CSharp);
            var lang2 = new Language("Batch Files", "bat", LsLanguageId.WindowsBat, new[] {"bat"}, new char[0]);
            lang2.FeatureCodeSuggestions = false;

            var languages = new[] { lang1, lang2 };

            var lm = new LanguageManager();
            lm.Languages = languages;

            Assert.IsTrue(lm.CheckFeatureCodeSuggestion("csharp", "cs"),
                "Expected csharp check to pass.");
            Assert.IsTrue(lm.CheckFeatureCodeSuggestion("code++.csharp", "cs"),
                "Expected code++.csharp check to pass.");
            Assert.IsFalse(lm.CheckFeatureCodeSuggestion("plaintext", "bat"),
                "Expected batch files check to fail.");

            lm = new LanguageManager();
            Assert.IsTrue(lm.CheckFeatureCodeSuggestion("csharp", "cs"),
                "Expected csharp check to pass.");
            Assert.IsTrue(lm.CheckFeatureCodeSuggestion("code++.ruby", "xxx"),
                "Expected code++.ruby check to pass.");
            Assert.IsTrue(lm.CheckFeatureCodeSuggestion("plaintext", "rs"),
                "Expected rust extension check to pass.");
        }

        [Test]
        public void GetExtensionFromFilenameTest()
        {
            var lm = new LanguageManager();

            Assert.AreEqual("", lm.GetExtensionFromFilename(null));
            Assert.AreEqual("", lm.GetExtensionFromFilename(""));
            Assert.AreEqual("", lm.GetExtensionFromFilename("."));
            Assert.AreEqual("cs", lm.GetExtensionFromFilename(".cs"));
            Assert.AreEqual("cs", lm.GetExtensionFromFilename("..cs"));
            Assert.AreEqual("cs", lm.GetExtensionFromFilename("foo.bar.cs"));
        }
    }
}
