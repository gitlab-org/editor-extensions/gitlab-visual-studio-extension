﻿using NUnit.Framework;
using System;
using System.Threading.Tasks;
using GitLab.Extension.LanguageServer;
using GitLab.Extension.SettingsUtil;
using System.Threading;
using System.IO;
using System.Net;
using System.Reactive.Linq;
using Autofac;
using GitLab.Extension.CodeSuggestions;
using GitLab.Extension.CodeSuggestions.Model;
using Serilog;
using GitLab.Extension.LanguageServer.Models;

namespace GitLab.Extension.Tests.LanguageServer
{
    [TestFixture]
    public class LsClientTsTests : TestBase
    {
        private ISettings _settings;

        [SetUp]
        public void Setup()
        {
            LsCommon.KillLanguageServer();

            Assert.IsFalse(LsCommon.IsLanguageServerRunning(out var _), 
                "Expected no language servers to be running at start of test (Setup)");

            CreateBuilder()
                .RegisterLogging()
                .RegisterCodeSuggestions()
                .RegisterSettings()
                .RegisterStatus()
                .RegisterLanguageServer(LsImpl.TypeScript)
                .BuildScope();

            _settings = _scope.Resolve<ISettings>();
            TestData.ResetSettings(_settings);
            Log.Information("Tests will be performed on {GitLabUrl}", _settings.GitLabUrl);
        }

        [TearDown]
        public void Teardown()
        {
            TestData.ResetSettings(_settings);

            try
            {
                Assert.IsFalse(LsCommon.IsLanguageServerRunning(TimeSpan.FromSeconds(10), out var _),
                    "Expected no language servers to be running at end of test (Teardown)");
            }
            finally
            {
                _scope.Dispose();
                _scope = null;
            }
        }

        public async Task<ILsClient> StartLsClientAsync()
        {
            var lsClient = _scope.Resolve<ILsClient>(
                new TypedParameter(typeof(LsClientSolution),
                new LsClientSolution(TestData.SolutionName, TestData.SolutionPath)));
            var ret = await lsClient.ConnectAsync();
            Assert.IsTrue(ret, "Expected LsClient to connect");

            return lsClient;
        }

        public static string GetTempFilePathWithExtension(string extension)
        {
            var path = Path.GetTempPath();
            string fileName;
            string tmpFile;

            do
            {
                fileName = Path.ChangeExtension(Guid.NewGuid().ToString(), extension);
                tmpFile = Path.Combine(path, fileName);
            }
            while (File.Exists(tmpFile));

            return tmpFile;
        }

        public static string GetFileUrl(string fileName)
        {
            return $"file://{fileName.Replace('\\', '/')}";
        }

        [Test]
        public async Task ConnectTestAsync()
        {
            using (var lsClient = _scope.Resolve<ILsClient>(
                new TypedParameter(typeof(LsClientSolution),
                new LsClientSolution(TestData.SolutionName, TestData.SolutionPath))))
            {
                var ret = await lsClient.ConnectAsync();
                Assert.IsTrue(ret, "Expected LsClient to connect");
            }
        }

        [Test]
        public async Task DisposeWithOutConnectTestAsync()
        {
            var lsClient = _scope.Resolve<ILsClient>(
                new TypedParameter(typeof(LsClientSolution),
                new LsClientSolution(TestData.SolutionName, TestData.SolutionPath)));
            await lsClient.DisposeAsync();

            lsClient = _scope.Resolve<ILsClient>(
                new TypedParameter(typeof(LsClientSolution),
                new LsClientSolution(TestData.SolutionName, TestData.SolutionPath)));
            lsClient.Dispose();
        }

        [Test]
        public async Task ReconnectionTestAsync()
        {
            Log.Debug("*** ReconnectionTestAsync ***");

            using (var lsClient = await StartLsClientAsync())
            {
                Log.Debug("*** ReconnectionTestAsync: LsCommon.VerifyLsClientWorkingAsync #1");
                await LsCommon.VerifyLsClientWorkingAsync(lsClient);

                Log.Debug("*** ReconnectionTestAsync: LsCommon.KillLsClientAndWaitForReconnect");
                LsCommon.KillLsClientAndWaitForReconnect(lsClient);

                await Task.Delay(5000); // give a chance for any background work of the LSP to finish

                Log.Debug("*** ReconnectionTestAsync: LsCommon.KillLsClientAndWaitForReconnect #2");
                await LsCommon.VerifyLsClientWorkingAsync(lsClient);
            }
        }

        [Test]
        public async Task TextDocumentDidOpenAndDidCloseAsync()
        {
            using (var lsClient = await StartLsClientAsync())
            {
                Assert.IsTrue(await lsClient.SendTextDocumentDidOpenAsync(
                        "file:///foo.cs",
                        LsLanguageId.CSharp,
                        0,
                        "namespace Bar { public class Foo { } }"),
                    "Expected SendTextDocumentDidOpenAsync to return true");
                
                Assert.IsTrue(await lsClient.SendTextDocumentDidCloseAsync(
                        "file:///foo.cs"),
                    "Expected SendTextDocumentDidCloseAsync to return true");
            }
        }

        [Test]
        public async Task TextDocumentDidChangeAsync()
        {
            using (var lsClient = await StartLsClientAsync())
            {
                Assert.IsTrue(await lsClient.SendTextDocumentDidOpenAsync(
                        "file:///foo.cs",
                        LsLanguageId.CSharp,
                        0,
                        "namespace Bar { public class Foo { } }"),
                    "Expected SendTextDocumentDidOpenAsync to return true");

                var codeV1 = "namespace Bar {\n\tpublic class Foo {\n\t\tpublic Foo() {\n\t\t\t\n\t\t}\n\t}\n}";
                var changes = new TextDocumentContentChangeEvent[]
                {
                    new TextDocumentContentChangeEvent
                    {
                        Range = new Range
                        {
                            Start = new Position
                            {
                                Line = 0,
                                Character = 35,
                            },
                            End = new Position
                            {
                                Line = 0,
                                Character = 35,
                            }
                        },
                        Text = "public Foo() { } ",
                    }
                };

                //Assert.IsTrue(await lsClient.SendTextDocumentDidChangeAsync(
                //        "file:///foo.cs",
                //        1,
                //        changes),
                Assert.IsTrue(await lsClient.SendTextDocumentDidChangeAsync(
                        "file:///foo.cs",
                        1,
                        codeV1),
                    "Expected SendTextDocumentDidChangeAsync to return true");
            }
        }

        [Test]
        public async Task TextDocumentCompletionAsync()
        {
            Log.Debug("*** TextDocumentCompletionAsync ***");
            var tmpFile = GetTempFilePathWithExtension(".cs");
            var codeV0 = "namespace Bar {\n\tpublic class Foo {\n\t}\n}";
            var codeV1 = "namespace Bar {\n\tpublic class Foo {\n\t\tpublic Foo() {\n\t\t\t\n\t\t}\n\t}\n}";
            var codeV1Change = "\tpublic Foo() {\n\t\t\t\n\t\t}\n\t";
            uint codeV1Line = 2;
            uint codeV1Pos = 16;
            uint codeV1StartLine = 2;
            uint codeV1StartChar = 1;
            uint codeV1EndLine = 2;
            uint codeV1EndChar = 1;
            try
            {
                File.WriteAllText(tmpFile, "namespace Bar {\n\tpublic class Foo {\n\t\tpublic Foo() {\n\t\t}\n\t}\n}");

                using (var lsClient = await StartLsClientAsync())
                {
                    bool ret;
                    var tokenSource = new CancellationTokenSource();

                    ret = await lsClient.SendTextDocumentDidOpenAsync(
                        tmpFile,
                        LsLanguageId.CSharp,
                        0,
                        codeV0);
                    Assert.IsTrue(ret, "Expect SendTextDocumentDidOpenAsync to return true");
                    
                    ret = await lsClient.SendTextDocumentDidChangeAsync(
                        tmpFile, 1, codeV1);
                    Assert.IsTrue(ret, "Expect SendTextDocumentDidChangeAsync to return true");

                    for (var retry = 0; retry < 10; retry++)
                    {
                        // Make sure the didChange gets processed before asking for completion
                        await Task.Delay(500);

                        try
                        {
                            var result = await lsClient.SendTextDocumentCompletionAsync(
                                tmpFile,
                                codeV1Line,
                                codeV1Pos,
                                CompletionTriggerKind.Invoked,
                                tokenSource.Token);

                            if (!result.IsSuccess(out var completions)) continue;
                        
                            Assert.GreaterOrEqual(completions.Items.Count, 1, "Expected at least one completion.");
                            
                            var completion = new Completion(completions.Items[0]);

                            Assert.Greater(completion.InsertText?.Length ?? 0, 0, "Expected non-empty InsertText in completion");

                            return;
                        }
                        catch (Exception exception)
                        {
                            Log.Warning("Attempt #{retry} to get code suggestion failed due to exception: {exception}", retry + 1, exception);
                        }
                    }
                    
                    Assert.Fail("Did not get a code suggestion after all retries.");

                }
            }
            finally
            {
                if(File.Exists(tmpFile))
                    File.Delete(tmpFile);

                Log.Debug("^^^ TextDocumentCompletionAsync ^^^");
            }
        }

        private string _userAgent;

        private void UserAgentTestHandler(HttpListenerRequest request, HttpListenerResponse response)
        {
            _userAgent = request.Headers["user-agent"];

            response.StatusCode = 200;
            response.StatusDescription = "OK";
            response.ContentLength64 = 0;
        }

        [Test]
        public async Task UserAgentTestAsync()
        {
            _userAgent = null;

            using (var listener = new TestHttpListener(UserAgentTestHandler))
            {
                var tmpFile = GetTempFilePathWithExtension(".cs");
                var codeV0 = "namespace Bar {\n\tpublic class Foo {\n\t}\n}";
                var codeV1 = "namespace Bar {\n\tpublic class Foo {\n\t\tpublic Foo() {\n\t\t\t\n\t\t}\n\t}\n}";
                uint codeV1Line = 3;
                uint codeV1Pos = 3;

                try
                {
                    File.WriteAllText(tmpFile, "namespace Bar {\n\tpublic class Foo {\n\t\tpublic Foo() {\n\t\t}\n\t}\n}");

                    _settings.GitLabUrl = listener.Url;

                    using (var lsClient = await StartLsClientAsync())
                    {
                        bool ret;
                        var tokenSource = new CancellationTokenSource();

                        ret = await lsClient.SendTextDocumentDidOpenAsync(
                            tmpFile,
                            LsLanguageId.CSharp,
                            0,
                            codeV0);
                        Assert.IsTrue(ret, "Expect SendTextDocumentDidOpenAsync to return true");

                        ret = await lsClient.SendTextDocumentDidChangeAsync(
                            tmpFile,
                            1,
                            codeV1);
                        Assert.IsTrue(ret, "Expect SendTextDocumentDidChangeAsync to return true");

                        // Make sure the didChange gets processed before asking for completion
                        await Task.Delay(250);

                        var result = await lsClient.SendTextDocumentCompletionAsync(
                            tmpFile,
                            codeV1Line,
                            codeV1Pos,
                            CompletionTriggerKind.Invoked,
                            tokenSource.Token);

                        // Occationally the LS process will hang/not shutdown
                        // if the listener isn't closed first.
                        listener.Dispose();
                    }
                }
                catch(Exception ex)
                {
                    if (ex.Message.Contains("unexpected 200 OK response when acquiring token"))
                        return;

                    throw;
                }
                finally
                {
                    if (File.Exists(tmpFile))
                        File.Delete(tmpFile);
                }

                Assert.IsNotNull(_userAgent);
                Assert.True(_userAgent.Contains("gl-visual-studio-extension"));
            }
        }
    }
}
