﻿using GitLab.Extension.LanguageServer;
using Microsoft.VisualStudio.VCProjectEngine;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;
using System.Linq;
using GitLab.Extension.LanguageServer.Models;

namespace GitLab.Extension.Tests.LanguageServer
{
    [SetUpFixture]
    public class LsCommon
    {
        private static int[] _existingProcessIds = null;

        [OneTimeSetUp]
        public void OneTimeSetup()
        {
            var processIds = new List<int>(1);

            foreach (var lsProcessName in TestData.LanguageServerProcessNames)
            {
                var processes = Process.GetProcessesByName(lsProcessName);
                if (processes.Length == 0)
                    _existingProcessIds = new int[0];

                foreach (var p in processes)
                {
                    processIds.Add(p.Id);
                }
            }

            _existingProcessIds = processIds.ToArray();
        }

        public static Process[] FilterOutExistingProcesses(Process[] processes)
        {
            if (processes.Length == 0)
                return new Process[0];

            var ret = new List<Process>();
            foreach(var p in processes)
            {
                if (((IList<int>)_existingProcessIds).Contains(p.Id))
                    continue;

                ret.Add(p);
            }

            return ret.ToArray();
        }

        /// <summary>
        /// Get language server processes excluding those that existed
        /// before running our tests.
        /// </summary>
        /// <returns></returns>
        public static Process[] GetLanguageServerProcesses()
        {
            var lsProcesses = new List<Process>();

            foreach (var lsProcessName in TestData.LanguageServerProcessNames)
            {
                lsProcesses.AddRange(Process.GetProcessesByName(lsProcessName));
            }

            return FilterOutExistingProcesses(lsProcesses.ToArray());
        }

        /// <summary>
        /// Does a language server process exist?
        /// </summary>
        /// <returns>True if process exists, false otherwise.</returns>
        public static bool IsLanguageServerRunning(out int count)
        {
            var processes = GetLanguageServerProcesses();
            count = processes.Length;
            return processes.Length > 0;
        }

        /// <summary>
        /// Get the language server process id
        /// </summary>
        /// <returns>Process id or -1 if process not found or multiple processes found.</returns>
        public static int GetLanguageServerProcessId()
        {
            var processes = GetLanguageServerProcesses();
            if (processes == null || processes.Length != 1)
                return -1;

            return processes[0].Id;
        }

        /// <summary>
        /// Does a language server process exist?
        /// </summary>
        /// <param name="waitTime">Time to wait for process to stop if its found.</param>
        /// <returns>True if process exists, false otherwise.</returns>
        public static bool IsLanguageServerRunning(TimeSpan waitTime, out int count)
        {
            var start = DateTime.Now;

            do
            {
                if (!IsLanguageServerRunning(out count))
                    return false;
            }
            while (DateTime.Now - start < waitTime);

            return true;
        }

        static Dictionary<int, TcpListener> _holdPorts = new Dictionary<int, TcpListener>();

        public static void HoldPort(int port)
        {
            var _listener = new TcpListener(IPAddress.Loopback, port);
            _listener.Start();

            _holdPorts[port] = _listener;
        }

        public static void ReleasePort(int port)
        {
            var _listener = _holdPorts[port];
            _holdPorts.Remove(port);

            _listener.Stop();
            _listener.Server?.Dispose();
        }

        public static void KillLanguageServer()
        {
            var processes = GetLanguageServerProcesses();
            foreach (var process in processes)
            {
                try
                {
                if (process.HasExited)
                    continue;
                    process.Kill();
                    process.WaitForExit();
                }
                catch
                {
                    Debugger.Break();
                }
            }
        }

        public static void KillLsClientAndWaitForReconnect(ILsClient lsClient)
        {
            KillLanguageServer();
            var maxWait = TimeSpan.FromSeconds(15);
            var ret = false;

            for (var start = DateTime.Now; DateTime.Now - start < maxWait;)
            {
                ret = IsLanguageServerRunning(out var _);
                if (!ret)
                    continue;

                if (lsClient.IsConnected)
                    break;
            }

            Assert.IsTrue(ret, "Expected language server client to be connected.");
        }
        public static async Task VerifyLsClientWorkingAsync(ILsClient lsClient)
        {
            Assert.IsTrue(await lsClient.SendTextDocumentDidOpenAsync(
                "file:///foo.cs",
                LsLanguageId.CSharp,
                0,
                "namespace Bar { public class Foo { } }"),
                "Expected SendTextDocumentDidOpenAsync call to work");

            switch(lsClient.TextDocumentSyncKind)
            {
                case TextDocumentSyncKind.Incremental:
                    var changes = new TextDocumentContentChangeEvent[]
                    {
                        new TextDocumentContentChangeEvent
                        {
                            Range = new Range
                            {
                                Start = new Position
                                {
                                    Line = 0,
                                    Character = 35,
                                },
                                End = new Position
                                {
                                    Line = 0,
                                    Character = 35,
                                }
                            },
                            Text = "public Foo() { } ",
                        }
                    };

                    Assert.IsTrue(await lsClient.SendTextDocumentDidChangeAsync(
                            "file:///foo.cs",
                            1,
                            changes),
                        "Expected SendTextDocumentDidChangeAsync to return true");
                    break;
                case TextDocumentSyncKind.Full:
                    Assert.IsTrue(await lsClient.SendTextDocumentDidChangeAsync(
                            "file:///foo.cs",
                            1,
                            "namespace Bar { public class Foo { public Foo() { } } }"
                        ),
                        "Expected first SendTextDocumentDidChangeAsync to work");
                    break;
            }
        }

    }
}
