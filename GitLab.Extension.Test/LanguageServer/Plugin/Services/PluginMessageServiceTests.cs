using System;
using System.Threading.Tasks;
using GitLab.Extension.LanguageServer.Plugin.Exceptions;
using GitLab.Extension.LanguageServer.Plugin.Model;
using GitLab.Extension.LanguageServer.Plugin.Services;
using GitLab.Extension.LanguageServer.Plugin.Utilities;
using Moq;
using Newtonsoft.Json.Linq;
using NUnit.Framework;
using Serilog;

namespace GitLab.Extension.Tests.LanguageServer.Plugin.Services
{
    [TestFixture]
    public class PluginMessageServiceTests
    {
        private Mock<ILogger> _loggerMock;
        private PluginMessageService _service;

        [SetUp]
        public void Setup()
        {
            _loggerMock = new Mock<ILogger>();
            _loggerMock.Setup(l => l.Verbose(It.IsAny<string>(), It.IsAny<object[]>()));
            _loggerMock.Setup(l => l.Debug(It.IsAny<string>(), It.IsAny<object[]>()));
            _loggerMock.Setup(l => l.Warning(It.IsAny<string>(), It.IsAny<object[]>()));

            _service = new PluginMessageService(_loggerMock.Object);
        }
        
        [Test]
        public void RegisterMessageHandler_NewRoute_SuccessfullyRegisters()
        {
            var route = new PluginMessageRoute("testPlugin", PluginMessageType.Request, "testMethod");
            var handler = new PluginMessageHandlerDelegate(_ => Task.FromResult(new JObject() as JToken));

            Assert.DoesNotThrow(() => _service.RegisterMessageHandler(route, handler));
            _loggerMock.Verify(l => l.Verbose("Registered handler for route: {Route}", route), Times.Once);
        }
        
        [Test]
        public void RegisterMessageHandler_DuplicateRoute_ThrowsInvalidOperationException()
        {
            var route = new PluginMessageRoute("testPlugin", PluginMessageType.Request, "testMethod");
            var handler = new PluginMessageHandlerDelegate(_ => Task.FromResult(new JObject() as JToken));

            _service.RegisterMessageHandler(route, handler);

            Assert.Throws<InvalidOperationException>(() => _service.RegisterMessageHandler(route, handler));
        }
        
        [Test]
        public async Task DispatchAsync_RegisteredRoute_InvokesHandlerAsync()
        {
            var route = new PluginMessageRoute("testPlugin", PluginMessageType.Request, "testMethod");
            var expectedResult = new JObject { ["key"] = "value" };
            var handler = new PluginMessageHandlerDelegate(_ => Task.FromResult(expectedResult as JToken));

            _service.RegisterMessageHandler(route, handler);

            var result = await _service.DispatchAsync(route, new JObject());

            Assert.That(result, Is.EqualTo(expectedResult));
            _loggerMock.Verify(l => l.Debug("Dispatching message for route: {Route}", route), Times.Once);
        }
        
        [Test]
        public void DispatchAsync_UnregisteredRoute_ThrowsHandlerNotFoundException()
        {
            var route = new PluginMessageRoute("testPlugin", PluginMessageType.Request, "testMethod");

            Assert.ThrowsAsync<HandlerNotFoundException>(() => _service.DispatchAsync(route, new JObject()));
            _loggerMock.Verify(l => l.Warning("Handler not found for route: {Route}", route), Times.Once);
        }
        
        [Test]
        public void DispatchAsync_HandlerThrowsException_ThrowsPluginMessageHandlingException()
        {
            var route = new PluginMessageRoute("testPlugin", PluginMessageType.Request, "testMethod");
            var handler = new PluginMessageHandlerDelegate(_ => throw new Exception("Test exception"));

            _service.RegisterMessageHandler(route, handler);

            var exception = Assert.ThrowsAsync<PluginMessageHandlerException>(() => _service.DispatchAsync(route, new JObject()));
            Assert.That(exception!.Message, Is.EqualTo("An error occurred while handling the plugin message"));
            Assert.That(exception!.InnerException!.Message, Is.EqualTo("Test exception"));
            Assert.That(exception.Route, Is.EqualTo(route));
        }
    }
}
