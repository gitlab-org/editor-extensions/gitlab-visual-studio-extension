using System.Threading.Tasks;
using Autofac;
using GitLab.Extension.LanguageServer.Plugin.Attributes;
using GitLab.Extension.LanguageServer.Plugin.Utilities;
using Newtonsoft.Json.Linq;
using NUnit.Framework;

namespace GitLab.Extension.Tests.LanguageServer.Plugin.Utilities
{
    [TestFixture]
    public class PluginMessageHandlerDelegateUtilitiesTests
    {
        private IContainer _container;

        [SetUp]
        public void Setup()
        {
            var builder = new ContainerBuilder();
            builder.RegisterType<TestController>().AsSelf();
            builder.RegisterType<TestDependency>().AsSelf();
            _container = builder.Build();
        }
        
        [Test]
        public async Task CreateHandlerDelegate_VoidMethod_ReturnsNullAsync()
        {
            var methodInfo = typeof(TestController).GetMethod(nameof(TestController.VoidMethod));
            var handler = PluginMessageHandlerDelegateUtilities.CreateHandlerDelegate(methodInfo, _container.BeginLifetimeScope());

            var result = await handler(null);

            Assert.That(result.Type, Is.EqualTo(JTokenType.Null));
        }
        
        [Test]
        public async Task CreateHandlerDelegate_VoidMethod_WhenPassedNonNullPayload_ReturnsNullAsync()
        {
            var methodInfo = typeof(TestController).GetMethod(nameof(TestController.VoidMethod));
            var handler = PluginMessageHandlerDelegateUtilities.CreateHandlerDelegate(methodInfo, _container.BeginLifetimeScope());

            var result = await handler(JToken.FromObject(new { foo = "bar" }));

            Assert.That(result.Type, Is.EqualTo(JTokenType.Null));
        }
        
        [Test]
        public async Task CreateHandlerDelegate_AsyncMethod_ReturnsExpectedResultAsync()
        {
            var methodInfo = typeof(TestController).GetMethod(nameof(TestController.AsyncMethod));
            var handler = PluginMessageHandlerDelegateUtilities.CreateHandlerDelegate(methodInfo, _container.BeginLifetimeScope());

            var result = await handler(null);

            Assert.That(result.Value<string>(), Is.EqualTo("AsyncResult"));
        }
        
        [Test]
        public async Task CreateHandlerDelegate_MethodWithPayload_PassesPayloadCorrectlyAsync()
        {
            var methodInfo = typeof(TestController).GetMethod(nameof(TestController.MethodWithPayload));
            var handler = PluginMessageHandlerDelegateUtilities.CreateHandlerDelegate(methodInfo, _container.BeginLifetimeScope());

            var payload = JObject.FromObject(new { Value = "TestPayload" });
            var result = await handler(payload);

            Assert.That(result.Value<string>(), Is.EqualTo("TestPayload"));
        }
        
        [Test]
        public async Task CreateHandlerDelegate_MethodWithDependency_InjectsDependencyCorrectlyAsync()
        {
            var methodInfo = typeof(TestController).GetMethod(nameof(TestController.MethodWithDependency));
            var handler = PluginMessageHandlerDelegateUtilities.CreateHandlerDelegate(methodInfo, _container.BeginLifetimeScope());

            var result = await handler(null);

            Assert.That(result.Value<string>(), Is.EqualTo("DependencyMethod"));
        }
        
        public class TestController
        {
            public void VoidMethod() { }
            public string SyncMethod() => "SyncResult";
            public async Task<string> AsyncMethod() => await Task.FromResult("AsyncResult");
            public string MethodWithPayload([Payload] TestPayload payload) => payload.Value;
            public string MethodWithDependency(TestDependency dependency) => dependency.DependencyMethod();
        }

        public class TestDependency
        {
            public string DependencyMethod() => "DependencyMethod";
        }

        public class TestPayload
        {
            public string Value { get; set; }
        }
    }
}
