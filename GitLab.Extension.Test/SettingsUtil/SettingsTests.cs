using GitLab.Extension.SettingsUtil;
using Moq;
using NUnit.Framework;
using Serilog;
using Serilog.Events;
using System.Collections.Generic;

namespace GitLab.Extension.Tests.SettingsUtil
{
    [TestFixture]
    public class SettingsTests
    {
        private Mock<ISettingsStorage> _mockStorage;
        private Mock<ILogger> _mockLogger;
        private Settings _settings;

        [SetUp]
        public void Setup()
        {
            _mockStorage = new Mock<ISettingsStorage>();
            _mockLogger = new Mock<ILogger>();
            _settings = new Settings(_mockStorage.Object, _mockLogger.Object);
        }

        [Test]
        public void Constructor_LoadsSettingsFromStorage()
        {
            _mockStorage.Verify(s => s.Load(_settings), Times.Once);
        }

        [Test]
        public void GitLabAccessToken_SetValue_SavesSettingsAndRaisesEvent()
        {
            bool eventRaised = false;
            _settings.SettingsChangedEvent += (sender, args) => 
            {
                eventRaised = true;
                Assert.AreEqual(Settings.GitLabAccessTokenKey, ((Settings.SettingsEventArgs)args).ChangedSettingKey);
            };

            _settings.GitLabAccessToken = "test_token";

            Assert.AreEqual("test_token", _settings.GitLabAccessToken);
            _mockStorage.Verify(s => s.Save(_settings), Times.Once);
            Assert.IsTrue(eventRaised);
        }

        [Test]
        public void IsCodeSuggestionsEnabled_SetValue_SavesSettingsAndRaisesEvent()
        {
            bool eventRaised = false;
            _settings.SettingsChangedEvent += (sender, args) => 
            {
                eventRaised = true;
                Assert.AreEqual(Settings.IsCodeSuggestionsEnabledKey, ((Settings.SettingsEventArgs)args).ChangedSettingKey);
            };

            _settings.IsCodeSuggestionsEnabled = false;

            Assert.IsFalse(_settings.IsCodeSuggestionsEnabled);
            _mockStorage.Verify(s => s.Save(_settings), Times.Once);
            Assert.IsTrue(eventRaised);
        }

        [Test]
        public void IsTelemetryEnabled_SetValue_SavesSettingsAndRaisesEvent()
        {
            bool eventRaised = false;
            _settings.SettingsChangedEvent += (sender, args) => 
            {
                eventRaised = true;
                Assert.AreEqual(Settings.IsTelemetryEnabledKey, ((Settings.SettingsEventArgs)args).ChangedSettingKey);
            };

            _settings.IsTelemetryEnabled = false;

            Assert.IsFalse(_settings.IsTelemetryEnabled);
            _mockStorage.Verify(s => s.Save(_settings), Times.Once);
            Assert.IsTrue(eventRaised);
        }

        [Test]
        public void IsOpenTabsContextEnabled_SetValue_SavesSettingsAndRaisesEvent()
        {
            bool eventRaised = false;
            _settings.SettingsChangedEvent += (sender, args) => 
            {
                eventRaised = true;
                Assert.AreEqual(Settings.IsOpenTabsContextEnabledKey, ((Settings.SettingsEventArgs)args).ChangedSettingKey);
            };

            _settings.IsOpenTabsContextEnabled = false;

            Assert.IsFalse(_settings.IsOpenTabsContextEnabled);
            _mockStorage.Verify(s => s.Save(_settings), Times.Once);
            Assert.IsTrue(eventRaised);
        }

        [Test]
        public void IgnoreCertificateErrors_SetValue_SavesSettingsAndRaisesEvent()
        {
            bool eventRaised = false;
            _settings.SettingsChangedEvent += (sender, args) => 
            {
                eventRaised = true;
                Assert.AreEqual(Settings.IgnoreCertificateErrorsKey, ((Settings.SettingsEventArgs)args).ChangedSettingKey);
            };

            _settings.IgnoreCertificateErrors = false;

            Assert.IsFalse(_settings.IgnoreCertificateErrors);
            _mockStorage.Verify(s => s.Save(_settings), Times.Once);
            Assert.IsTrue(eventRaised);
        }

        [Test]
        public void IsDuoChatEnabled_SetValue_SavesSettingsAndRaisesEvent()
        {
            bool eventRaised = false;
            _settings.SettingsChangedEvent += (sender, args) => 
            {
                eventRaised = true;
                Assert.AreEqual(Settings.IsDuoChatEnabledKey, ((Settings.SettingsEventArgs)args).ChangedSettingKey);
            };

            _settings.IsDuoChatEnabled = false;

            Assert.IsFalse(_settings.IsDuoChatEnabled);
            _mockStorage.Verify(s => s.Save(_settings), Times.Once);
            Assert.IsTrue(eventRaised);
        }

        [Test]
        public void IsSuggestionStreamingEnabled_SetValue_SavesSettingsAndRaisesEvent()
        {
            bool eventRaised = false;
            _settings.SettingsChangedEvent += (sender, args) => 
            {
                eventRaised = true;
                Assert.AreEqual(Settings.IsSuggestionStreamingEnabledKey, ((Settings.SettingsEventArgs)args).ChangedSettingKey);
            };

            _settings.IsSuggestionStreamingEnabled = false;

            Assert.IsFalse(_settings.IsSuggestionStreamingEnabled);
            _mockStorage.Verify(s => s.Save(_settings), Times.Once);
            Assert.IsTrue(eventRaised);
        }

        [Test]
        public void GitLabUrl_SetValue_SavesSettingsAndRaisesEvent()
        {
            bool eventRaised = false;
            _settings.SettingsChangedEvent += (sender, args) => 
            {
                eventRaised = true;
                Assert.AreEqual(Settings.GitLabUrlKey, ((Settings.SettingsEventArgs)args).ChangedSettingKey);
            };

            _settings.GitLabUrl = "https://gitlab.example.com";

            Assert.AreEqual("https://gitlab.example.com", _settings.GitLabUrl);
            _mockStorage.Verify(s => s.Save(_settings), Times.Once);
            Assert.IsTrue(eventRaised);
        }

        [Test]
        public void BatchSettingsUpdate_DoesNotRaiseEventsOrSaveUntilStopped()
        {
            bool eventRaised = false;
            _settings.SettingsChangedEvent += (sender, args) => eventRaised = true;

            _settings.StartBatchSettingsUpdate();
            _settings.GitLabAccessToken = "test_token";
            _settings.IsCodeSuggestionsEnabled = false;

            Assert.IsFalse(eventRaised);
            _mockStorage.Verify(s => s.Save(_settings), Times.Never);

            _settings.StopBatchSettingsUpdate();

            Assert.IsTrue(eventRaised);
            _mockStorage.Verify(s => s.Save(_settings), Times.Once);
        }

        [Test]
        public void Configured_ReturnsTrue_WhenUrlAndTokenAreSet()
        {
            _settings.GitLabUrl = "https://gitlab.com";
            _settings.GitLabAccessToken = "test_token";

            Assert.IsTrue(_settings.Configured);
        }

        [Test]
        public void Configured_ReturnsFalse_WhenUrlIsEmpty()
        {
            _settings.GitLabUrl = "";
            _settings.GitLabAccessToken = "test_token";

            Assert.IsFalse(_settings.Configured);
        }

        [Test]
        public void Configured_ReturnsFalse_WhenTokenIsEmpty()
        {
            _settings.GitLabUrl = "https://gitlab.com";
            _settings.GitLabAccessToken = "";

            Assert.IsFalse(_settings.Configured);
        }

        [Test]
        public void SetAdditionalLanguages_SplitsAndFiltersLanguages()
        {
            _settings.SetAdditionalLanguages("csharp, java, python, invalid@language");

            CollectionAssert.AreEqual(new List<string> { "csharp", "java", "python" }, _settings.AdditionalLanguages);
            _mockLogger.Verify(l => l.Warning(It.IsAny<string>(), It.IsAny<string>()), Times.Once);
        }

        [Test]
        public void LogLevel_DefaultValue_IsWarning()
        {
            Assert.AreEqual(LogEventLevel.Warning, _settings.LogLevel);
        }

        [Test]
        public void LogLevel_SetValue_SavesSettingsAndRaisesEvent()
        {
            bool eventRaised = false;
            _settings.SettingsChangedEvent += (sender, args) => 
            {
                eventRaised = true;
                Assert.AreEqual(Settings.LogLevelKey, ((Settings.SettingsEventArgs)args).ChangedSettingKey);
            };

            _settings.LogLevel = LogEventLevel.Debug;

            Assert.AreEqual(LogEventLevel.Debug, _settings.LogLevel);
            _mockStorage.Verify(s => s.Save(_settings), Times.Once);
            Assert.IsTrue(eventRaised);
        }
    }
}
