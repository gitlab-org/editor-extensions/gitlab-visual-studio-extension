namespace GitLab.Extension.Tests.Status
{
    using NUnit.Framework;
    using System;
    using System.Collections.Generic;
    using GitLab.Extension.Status;
    using GitLab.Extension.Status.Models;

    [TestFixture]
    public class FeatureStateManagerTests
    {
        private FeatureStateManager _featureStateManager;

        [SetUp]
        public void Setup()
        {
            _featureStateManager = new FeatureStateManager();
        }

        [Test]
        public void Update_ShouldUpdateFeatureState()
        {
            // Arrange
            var feature = Feature.CODE_SUGGESTIONS;
            var initialState = _featureStateManager.GetCurrentState(feature);
            var newChecks = new List<FeatureStateCheck>
                { new FeatureStateCheck(StateCheckId.DISABLED_LANGUAGE, "Language disabled") };
            var newState = new FeatureState(feature, newChecks);

            // Act
            _featureStateManager.Update(newState);

            // Assert
            var updatedState = _featureStateManager.GetCurrentState(feature);
            Assert.AreEqual(newState, updatedState);
            Assert.AreNotEqual(initialState, updatedState);
        }

        [Test]
        public void GetObservable_ShouldReturnObservableForFeature()
        {
            // Arrange
            var feature = Feature.CHAT;

            // Act
            var observable = _featureStateManager.GetObservable(feature);

            // Assert
            Assert.IsNotNull(observable);
            Assert.IsInstanceOf<IObservable<FeatureState>>(observable);
        }

        [Test]
        public void GetCurrentState_ShouldReturnCurrentStateForFeature()
        {
            // Arrange
            var feature = Feature.CODE_SUGGESTIONS;

            // Act
            var state = _featureStateManager.GetCurrentState(feature);

            // Assert
            Assert.IsNotNull(state);
            Assert.AreEqual(feature, state.FeatureId);
        }

        [Test]
        public void IsAvailable_ShouldReturnTrueForAvailableFeature()
        {
            // Arrange
            var feature = Feature.CODE_SUGGESTIONS;

            // Act
            var isAvailable = _featureStateManager.IsAvailable(feature);

            // Assert
            Assert.IsTrue(isAvailable);
        }

        [Test]
        public void IsAvailable_ShouldReturnFalseForUnavailableFeature()
        {
            // Arrange
            var feature = Feature.CODE_SUGGESTIONS;
            var unavailableState = new FeatureState(feature,
                new List<FeatureStateCheck> { new FeatureStateCheck(StateCheckId.INVALID_TOKEN, "Invalid token") });
            _featureStateManager.Update(unavailableState);

            // Act
            var isAvailable = _featureStateManager.IsAvailable(feature);

            // Assert
            Assert.IsFalse(isAvailable);
        }

        [Test]
        public void Update_ShouldThrowExceptionForUnknownFeature()
        {
            // Arrange
            var unknownFeature = (Feature)999; // Assuming 999 is not a valid Feature enum value
            var invalidState = new FeatureState(unknownFeature, new List<FeatureStateCheck>());

            // Act & Assert
            Assert.Throws<ArgumentException>(() => _featureStateManager.Update(invalidState));
        }

        // continue writing tests
        [Test]
        public void GetObservable_ShouldThrowExceptionForUnknownFeature()
        {
            // Arrange
            var unknownFeature = (Feature)999; // Assuming 999 is not a valid Feature enum value

            // Act & Assert
            Assert.Throws<ArgumentException>(() => _featureStateManager.GetObservable(unknownFeature));
        }

        [Test]
        public void GetCurrentState_ShouldThrowExceptionForUnknownFeature()
        {
            // Arrange
            var unknownFeature = (Feature)999; // Assuming 999 is not a valid Feature enum value

            // Act & Assert
            Assert.Throws<ArgumentException>(() => _featureStateManager.GetCurrentState(unknownFeature));
        }

        [Test]
        public void Update_ShouldNotifyObservers()
        {
            // Arrange
            var feature = Feature.CHAT;
            var newState = new FeatureState(feature,
                new List<FeatureStateCheck> { new FeatureStateCheck(StateCheckId.CHAT_NO_LICENSE, "No license") });
            var notificationReceived = false;

            _featureStateManager.GetObservable(feature).Subscribe(state =>
            {
                if (state.Equals(newState))
                {
                    notificationReceived = true;
                }
            });

            // Act
            _featureStateManager.Update(newState);

            // Assert
            Assert.IsTrue(notificationReceived);
        }

        [Test]
        public void IsAvailable_ShouldReturnTrueForChatFeatureWithNoChecks()
        {
            // Arrange
            var feature = Feature.CHAT;

            // Act
            var isAvailable = _featureStateManager.IsAvailable(feature);

            // Assert
            Assert.IsTrue(isAvailable);
        }

        [Test]
        public void IsAvailable_ShouldReturnFalseForChatFeatureWithInvalidToken()
        {
            // Arrange
            var feature = Feature.CHAT;
            var unavailableState = new FeatureState(feature,
                new List<FeatureStateCheck> { new FeatureStateCheck(StateCheckId.INVALID_TOKEN, "Invalid token") });
            _featureStateManager.Update(unavailableState);

            // Act
            var isAvailable = _featureStateManager.IsAvailable(feature);

            // Assert
            Assert.IsFalse(isAvailable);
        }
    }
}
