// tests for NotificationService
using NUnit.Framework;
using Moq;
using System.Collections.Generic;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using System.Threading.Tasks;
using GitLab.Extension.InfoBar;
using GitLab.Extension.SettingsUtil;
using GitLab.Extension.Status;
using GitLab.Extension.Status.Models;
using Microsoft.VisualStudio;
using Microsoft.VisualStudio.Imaging.Interop;
using Microsoft.VisualStudio.Shell.Interop;

namespace GitLab.Extension.Tests
{
    [TestFixture]
    public class NotificationServiceTests
    {
        private Mock<ISettings> _mockSettings;
        private Mock<IInfoBarFactory> _mockInfoBarFactory;
        private Mock<IFeatureStateManager> _mockFeatureStateManager;
        private Mock<IInfoBar> _mockInfoBar;
        private NotificationService _notificationService;

        [SetUp]
        public void Setup()
        {
            _mockSettings = new Mock<ISettings>();
            _mockInfoBarFactory = new Mock<IInfoBarFactory>();
            _mockFeatureStateManager = new Mock<IFeatureStateManager>();
            _mockInfoBar = new Mock<IInfoBar>();

            _mockInfoBarFactory.Setup(f => f.AttachInfoBar(It.IsAny<IVsInfoBarHost>(), It.IsAny<string>(), It.IsAny<List<InfoBarAction>>(), It.IsAny<ImageMoniker>()))
                .Returns(_mockInfoBar.Object);

            _notificationService = new NotificationService(
                _mockSettings.Object,
                _mockInfoBarFactory.Object,
                _mockFeatureStateManager.Object);
        }
        
        [Test]
        public async Task NotificationService_ShouldShowInfoBarWhenAuthenticationRequired()
        {
            // Arrange
            MockShell();
            var authFeatureStates = new Subject<FeatureState>();
            _mockFeatureStateManager.Setup(m => m.GetObservable(Feature.AUTHENTICATION)).Returns(authFeatureStates.AsObservable());
            _notificationService.Initialize();
            
            // Act
            authFeatureStates.OnNext(new FeatureState(Feature.AUTHENTICATION, new List<FeatureStateCheck>
            {
                new FeatureStateCheck(StateCheckId.AUTHENTICATION_REQUIRED, "Authentication required")
            }));
            
            // account for possible race condition due to async events
            await Task.Delay(100);
            
            // Assert
            _mockInfoBarFactory.Verify(f => f.AttachInfoBar(
                It.IsAny<IVsInfoBarHost>(), It.Is<string>(s => s.Contains(NotificationService.AUTHENTICATION_REQUIRED_MESSAGE)), It.IsAny<List<InfoBarAction>>(), It.IsAny<ImageMoniker>()), Times.Once);
        }

        [Test]
        public async Task NotificationService_ShouldShowAndThenHideInfoBarWhenChecksDisappear()
        {
            // Arrange
                MockShell();
                var authFeatureStates = new Subject<FeatureState>();
                _mockFeatureStateManager.Setup(m => m.GetObservable(Feature.AUTHENTICATION)).Returns(authFeatureStates.AsObservable());
                _notificationService.Initialize();
                
                // Act
                authFeatureStates.OnNext(new FeatureState(Feature.AUTHENTICATION, new List<FeatureStateCheck>
                {
                    new FeatureStateCheck(StateCheckId.AUTHENTICATION_REQUIRED, "Authentication required")
                }));
                
                await Task.Delay(100);
                
                // Assert
                _mockInfoBarFactory.Verify(f => f.AttachInfoBar(
                    It.IsAny<IVsInfoBarHost>(), It.Is<string>(s => s.Contains(NotificationService.AUTHENTICATION_REQUIRED_MESSAGE)), It.IsAny<List<InfoBarAction>>(), It.IsAny<ImageMoniker>()), Times.Once);

                // Act again
                authFeatureStates.OnNext(new FeatureState(Feature.AUTHENTICATION, new List<FeatureStateCheck>()));
                
                await Task.Delay(100);
                
                // Assert again
                _mockInfoBar.Verify(i => i.Close(), Times.Once);
        }

        private void MockShell()
        {
            AssemblySetup.MockServiceProvider.Reset();

            var message = "Test Message";

            var mockHost = new Mock<IVsInfoBarHost>();
            var mockShell = new Mock<IVsShell>();

            object _host = mockHost.Object;
            mockShell.Setup(shell => shell.GetProperty((int)__VSSPROPID7.VSSPROPID_MainWindowInfoBarHost, out _host))
                .Returns(VSConstants.S_OK);
            AssemblySetup.MockServiceProvider.AddService(typeof(IVsShell), mockShell.Object);
        }
    }
}
