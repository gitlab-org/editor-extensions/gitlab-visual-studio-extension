using System.Collections.Generic;
using GitLab.Extension.Status.Models;
using NUnit.Framework;

namespace GitLab.Extension.Tests.Status
{
    [TestFixture]
    public class StatusBarStateTests
    {
        private StatusBarState _statusBarState;

        [SetUp]
        public void Setup()
        {
            _statusBarState = new StatusBarState();
        }

        [Test]
        public void IsConfigured_WhenSet_UpdatesRootState()
        {
            _statusBarState.IsConfigured = true;
            Assert.AreEqual(RootState.LOADING, _statusBarState.RootState);

            _statusBarState.IsConfigured = false;
            Assert.AreEqual(RootState.NOT_CONFIGURED, _statusBarState.RootState);
        }

        [Test]
        public void KeepsLoadingState_WhenNotAllFeaturesUpdated()
        {
            _statusBarState.IsConfigured = true;
            var featureState = new FeatureState(Feature.AUTHENTICATION, new List<FeatureStateCheck>());
            _statusBarState.UpdateFeatureState(featureState);

            Assert.AreEqual(RootState.LOADING, _statusBarState.RootState);
        }

        [Test]
        public void RootState_WhenAllFeaturesEnabled_ReturnsEnabled()
        {
            FinishLoadAndConfigure();

            Assert.AreEqual(RootState.ENABLED, _statusBarState.RootState);
        }

        [Test]
        public void RootState_WhenDuoDisabled_ReturnsDisabled()
        {
            FinishLoadAndConfigure();
            _statusBarState.UpdateFeatureState(new FeatureState(Feature.AUTHENTICATION, new List<FeatureStateCheck> 
            { 
                new FeatureStateCheck(StateCheckId.DUO_DISABLED_FOR_PROJECT, "Duo disabled") 
            }));

            Assert.AreEqual(RootState.DISABLED, _statusBarState.RootState);
        }

        [Test]
        public void RootState_WhenAuthenticationRequired_ReturnsError()
        {
            FinishLoadAndConfigure();
            _statusBarState.UpdateFeatureState(new FeatureState(Feature.AUTHENTICATION, new List<FeatureStateCheck> 
            { 
                new FeatureStateCheck(StateCheckId.AUTHENTICATION_REQUIRED, "Auth required") 
            }));

            Assert.AreEqual(RootState.ERROR, _statusBarState.RootState);
        }

        [Test]
        public void StatusMessage_WhenNotConfigured_ReturnsSetupRequired()
        {
            _statusBarState.IsConfigured = false;
            Assert.AreEqual("Setup required.", _statusBarState.StatusMessage);
        }

        [Test]
        public void StatusMessage_WhenError_ReturnsErrorDetails()
        {
            FinishLoadAndConfigure();
            _statusBarState.UpdateFeatureState(new FeatureState(Feature.AUTHENTICATION, new List<FeatureStateCheck> 
            { 
                new FeatureStateCheck(StateCheckId.INVALID_TOKEN, "Invalid token") 
            }));

            Assert.AreEqual("Invalid token", _statusBarState.StatusMessage);
        }

        [Test]
        public void ChatState_WhenNotConfigured_ReturnsDisabled()
        {
            _statusBarState.IsConfigured = false;
            Assert.AreEqual(ChatState.DISABLED, _statusBarState.ChatState);
        }

        [Test]
        public void ChatState_WhenNoLicense_ReturnsError()
        {
            FinishLoadAndConfigure();
            _statusBarState.UpdateFeatureState(new FeatureState(Feature.CHAT, new List<FeatureStateCheck> 
            { 
                new FeatureStateCheck(StateCheckId.CHAT_NO_LICENSE, "No license") 
            }));

            Assert.AreEqual(ChatState.ERROR, _statusBarState.ChatState);
        }

        [Test]
        public void CodeSuggestionsState_WhenNotConfigured_ReturnsDisabled()
        {
            _statusBarState.IsConfigured = false;
            Assert.AreEqual(CodeSuggestionsState.DISABLED, _statusBarState.CodeSuggestionsState);
        }

        [Test]
        public void CodeSuggestionsState_WhenLanguageNotSupported_ReturnsLanguageNotSupported()
        {
            FinishLoadAndConfigure();
            _statusBarState.UpdateFeatureState(new FeatureState(Feature.CODE_SUGGESTIONS, new List<FeatureStateCheck> 
            { 
                new FeatureStateCheck(StateCheckId.UNSUPPORTED_LANGUAGE, "Language not supported") 
            }));

            Assert.AreEqual(CodeSuggestionsState.LANGUAGE_NOT_SUPPORTED, _statusBarState.CodeSuggestionsState);
        }

        [Test]
        public void DarkMode_WhenSet_UpdatesCorrectly()
        {
            _statusBarState.IsStatusBarDarkMode = true;
            Assert.IsTrue(_statusBarState.IsStatusBarDarkMode);

            _statusBarState.IsMenuDarkMode = false;
            Assert.IsFalse(_statusBarState.IsMenuDarkMode);
        }

        // Set the state to configured and finish its load by setting all feature states
        private void FinishLoadAndConfigure()
        {
            _statusBarState.IsConfigured = true;
            _statusBarState.UpdateFeatureState(new FeatureState(Feature.AUTHENTICATION, new List<FeatureStateCheck>()));
            _statusBarState.UpdateFeatureState(new FeatureState(Feature.CODE_SUGGESTIONS, new List<FeatureStateCheck>()));
            _statusBarState.UpdateFeatureState(new FeatureState(Feature.CHAT, new List<FeatureStateCheck>()));
        }
    }
}
