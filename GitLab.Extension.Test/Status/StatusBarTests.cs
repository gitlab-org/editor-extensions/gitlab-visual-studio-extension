using System;
using System.Collections.Generic;
using System.Drawing;
using System.Reactive.Subjects;
using System.Windows;
using GitLab.Extension.SettingsUtil;
using GitLab.Extension.Status;
using GitLab.Extension.Status.Interfaces;
using GitLab.Extension.Status.Models;
using GitLab.Extension.Workspace;
using GitLab.Extension.Workspace.Model;
using Microsoft.VisualStudio.Shell;
using Moq;
using NUnit.Framework;
using Serilog;

namespace GitLab.Extension.Tests.Status
{
    [TestFixture]
    public class StatusBarTests
    {
        private Mock<ISettings> _settingsMock;
        private Mock<IStatusControlFactory> _statusControlFactoryMock;
        private Mock<IFeatureStateManager> _featureStateManagerMock;
        private Mock<IThemeManager> _themeManagerMock;
        private Mock<ILogger> _loggerMock;
        private Mock<IExtensionStatusControl> _statusControlMock;
        private Mock<IStatusBarState> _statusBarStateMock;

        private Subject<FeatureState> _authSubject;
        private Subject<FeatureState> _codeSuggestionsSubject;
        private Subject<FeatureState> _chatSubject;
        private Subject<ThemeUpdate> _themeSubject;

        private StatusBar _statusBar;

        [SetUp]
        [ExecuteOnMainThread]
        public void Setup()
        {
            _settingsMock = new Mock<ISettings>();
            _statusControlFactoryMock = new Mock<IStatusControlFactory>();
            _featureStateManagerMock = new Mock<IFeatureStateManager>();
            _themeManagerMock = new Mock<IThemeManager>();
            _loggerMock = new Mock<ILogger>();
            _statusControlMock = new Mock<IExtensionStatusControl>();
            _statusBarStateMock = new Mock<IStatusBarState>();

            _statusControlMock.SetupGet(x => x.StatusBarState).Returns(_statusBarStateMock.Object);

            _authSubject = new Subject<FeatureState>();
            _codeSuggestionsSubject = new Subject<FeatureState>();
            _chatSubject = new Subject<FeatureState>();
            _themeSubject = new Subject<ThemeUpdate>();

            _featureStateManagerMock.Setup(x => x.GetObservable(Feature.AUTHENTICATION)).Returns(_authSubject);
            _featureStateManagerMock.Setup(x => x.GetObservable(Feature.CODE_SUGGESTIONS)).Returns(_codeSuggestionsSubject);
            _featureStateManagerMock.Setup(x => x.GetObservable(Feature.CHAT)).Returns(_chatSubject);
            _themeManagerMock.SetupGet(x => x.ThemeUpdates).Returns(_themeSubject);
            _settingsMock.SetupGet(x => x.Configured).Returns(true);

            _statusControlFactoryMock.Setup(x => x.Create(It.IsAny<DuoMenuClickHandlers>())).Returns(_statusControlMock.Object);

            _statusBar = new StatusBar(
                _settingsMock.Object,
                _statusControlFactoryMock.Object,
                _featureStateManagerMock.Object,
                _themeManagerMock.Object,
                _loggerMock.Object
            );
        }

        [Test]
        [ExecuteOnMainThread]
        [TestCase(true)]
        [TestCase(false)]
        public void InitializeDisplay_CreatesStatusControlAndSetsIsConfigured(bool isConfigured)
        {
            var packageMock = new Mock<AsyncPackage>();
            _settingsMock.SetupGet(x => x.Configured).Returns(isConfigured);
            _statusControlMock.SetupGet(x => x.UIElement).Returns(new Mock<UIElement>().Object);

            _statusBar.InitializeDisplay(packageMock.Object);
            
            _statusControlFactoryMock.Verify(x => x.Create(It.IsAny<IDuoMenuClickHandlers>()), Times.Once);
            _statusBarStateMock.VerifySet(x => x.IsConfigured = isConfigured, Times.Once);
            _themeManagerMock.Verify(x => x.UpdateTheme(), Times.Once);
        }

        [Test]
        [ExecuteOnMainThread]
        public void OnFeatureStateUpdate_UpdatesStatusBarState()
        {
            var packageMock = new Mock<AsyncPackage>();
            _statusBar.InitializeDisplay(packageMock.Object);
            _statusControlMock.SetupGet(x => x.UIElement).Returns(new Mock<UIElement>().Object);
            
            var featureState = new FeatureState(Feature.AUTHENTICATION, new List<FeatureStateCheck>());

            _authSubject.OnNext(featureState);

            _statusBarStateMock.Verify(x => x.UpdateFeatureState(featureState), Times.Once);
        }
        
        [Test]
        [ExecuteOnMainThread]
        public void OnStatusBarInit_SetsTheClickHandler()
        {
            var packageMock = new Mock<AsyncPackage>();
            _statusBar.InitializeDisplay(packageMock.Object);

            _statusControlFactoryMock.Verify(x => x.Create(It.IsAny<DuoMenuClickHandlers>()), Times.Once);
        }

        [Test]
        [ExecuteOnMainThread]
        public void OnFeatureStateUpdate_LogsErrorOnException()
        {
            var packageMock = new Mock<AsyncPackage>();
            _statusBar.InitializeDisplay(packageMock.Object);
            _statusControlMock.SetupGet(x => x.UIElement).Returns(new Mock<UIElement>().Object);
            
            var featureState = new FeatureState(Feature.AUTHENTICATION, new List<FeatureStateCheck>());
            _statusBarStateMock.Setup(x => x.UpdateFeatureState(It.IsAny<FeatureState>())).Throws(new Exception("Test exception"));

            _authSubject.OnNext(featureState);

            _loggerMock.Verify(x => x.Error(It.IsAny<Exception>(), It.IsAny<string>()), Times.Once);
        }
        
        public static Color DarkColor => Color.DarkBlue;
        public static Color LightColor => Color.LightYellow;

        [Test]
        [ExecuteOnMainThread]
        [TestCase(nameof(DarkColor), nameof(DarkColor), true, true)]
        [TestCase(nameof(DarkColor), nameof(LightColor), true, false)]
        [TestCase(nameof(LightColor), nameof(DarkColor), false, true)]
        [TestCase(nameof(LightColor), nameof(LightColor), false, false)]
        public void OnThemeUpdate_RefreshesTheme(string backgroundColorName, string statusBarColorName, bool expectedMenuDarkMode, bool expectedStatusBarDarkMode)
        {
            var packageMock = new Mock<AsyncPackage>();
            _statusBar.InitializeDisplay(packageMock.Object);
            _statusControlMock.SetupGet(x => x.UIElement).Returns(new Mock<UIElement>().Object);

            var backgroundColor = (Color) typeof(StatusBarTests).GetProperty(backgroundColorName).GetValue(null);
            var statusBarColor = (Color) typeof(StatusBarTests).GetProperty(statusBarColorName).GetValue(null);
            var themeUpdate = new ThemeUpdate(backgroundColor, statusBarColor);

            _themeSubject.OnNext(themeUpdate);

            _statusBarStateMock.VerifySet(x => x.IsStatusBarDarkMode = expectedStatusBarDarkMode, Times.Once);
            _statusBarStateMock.VerifySet(x => x.IsMenuDarkMode = expectedMenuDarkMode, Times.Once);
        }
    }
}
