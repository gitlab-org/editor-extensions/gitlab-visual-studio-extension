using System;
using System.Collections.Generic;

namespace GitLab.Extension.Tests
{
    internal class TestObserver<T> : IObserver<T>
    {
        public List<T> Values { get; } = new List<T>();
        public List<Exception> Errors { get; } = new List<Exception>();
        public List<bool> Completed { get; } = new List<bool>();

        public void OnCompleted() => Completed.Add(true);
        public void OnError(Exception error) => Errors.Add(error);
        public void OnNext(T value) => Values.Add(value);
    }
}
