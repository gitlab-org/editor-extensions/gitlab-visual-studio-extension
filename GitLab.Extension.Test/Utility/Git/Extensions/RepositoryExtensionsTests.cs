using LibGit2Sharp;
using Moq;
using NUnit.Framework;
using RepositoryExtensions = GitLab.Extension.Utility.Git.RepositoryExtensions;

namespace GitLab.Extension.Tests.Utility.Git.Extensions
{
    [TestFixture]
    public class RepositoryExtensionsTests
    {
        [Test]
        public void GetRemoteInfo_RemoteNotFound_ReturnsError()
        {
            var mockRepository = new Mock<IRepository>();
            mockRepository.Setup(r => r.Network.Remotes["origin"]).Returns((Remote)null);

            var result = RepositoryExtensions.GetRemoteInfo(mockRepository.Object, "origin");

            Assert.That(result.IsError(out _), Is.True);
        }
        
        [Test]
        public void GetRemoteInfo_Success_ReturnsRemoteInfo()
        {
            // Arrange
            var mockRepository = new Mock<IRepository>();
            var mockRemote = new Mock<Remote>();
            mockRemote.SetupGet(r => r.Url).Returns("https://example.com/example-namespace/repo.git");
            mockRepository.Setup(r => r.Network.Remotes["origin"]).Returns(mockRemote.Object);

            // Act
            var result = RepositoryExtensions.GetRemoteInfo(mockRepository.Object, "origin");

            // Assert
            Assert.That(result.IsSuccess(out var remoteInfo), Is.True);
            Assert.That(remoteInfo, Is.Not.Null);
        }
    }
}