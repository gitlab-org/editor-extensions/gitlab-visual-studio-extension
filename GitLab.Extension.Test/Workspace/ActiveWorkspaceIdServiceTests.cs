using GitLab.Extension.Workspace;
using Moq;
using NUnit.Framework;
using Serilog;

namespace GitLab.Extension.Tests.Workspace
{
    [TestFixture]
    public class ActiveWorkspaceIdServiceTests
    {
        private ActiveWorkspaceIdService _service;
        private Mock<ILogger> _mockLogger;
        private readonly WorkspaceId _testWorkspaceId = new WorkspaceId("TestSolution", "path/to/solution");
        private readonly WorkspaceId _test2WorkspaceId = new WorkspaceId("TestSolution2", "path/to/solution2");
        
        [SetUp]
        public void SetUp()
        {
            _mockLogger = new Mock<ILogger>();
            _service = new ActiveWorkspaceIdService(_mockLogger.Object);
        }
        
        [Test]
        public void OnWorkspaceOpen_SetsNewActiveWorkspace()
        {
            // Arrange
            var observer = new TestObserver<WorkspaceId?>();

            // Act
            _service.OnWorkspaceOpen(_testWorkspaceId);
            using (_service.Subscribe(observer))
            {
                // Assert
                Assert.That(observer.Values[0], Is.EqualTo(_testWorkspaceId));
            }
        }
        
        [Test]
        public void OnWorkspaceOpen_SameAsCurrent_DoesNotUpdate()
        {
            // Arrange
            _service.OnWorkspaceOpen(_testWorkspaceId); // First open to set the current active workspace
            
            var observer = new TestObserver<WorkspaceId?>();
            using (_service.Subscribe(observer))
            {
                // Act
                _service.OnWorkspaceOpen(_testWorkspaceId); // Attempt to open the same workspace again
            }
            
            // Assert
            Assert.That(observer.Values.Count, Is.EqualTo(1));
        }
        
        [Test]
        public void OnWorkspaceClose_ActiveWorkspace_SetsToNull()
        {
            // Arrange
            _service.OnWorkspaceOpen(_testWorkspaceId);
            var observer = new TestObserver<WorkspaceId?>();
            
            // Act
            _service.OnWorkspaceClose(_testWorkspaceId);
            
            // Assert
            using (_service.Subscribe(observer))
            {
                Assert.That(observer.Values[0], Is.Null);
            }
        }
        
        [Test]
        public void OnWorkspaceClose_DifferentWorkspace_DoesNotUpdate()
        {
            // Arrange
            _service.OnWorkspaceOpen(_testWorkspaceId); // Open to set active workspace
            var observer = new TestObserver<WorkspaceId?>();
            
            // Act
            _service.OnWorkspaceClose(_test2WorkspaceId); // Attempt to close a different workspace
            
            // Assert
            using (_service.Subscribe(observer))
            {
                Assert.That(observer.Values[0], Is.Not.Null);
            }
        }
        
        [Test]
        public void Subscribe_ReceivesUpdatesOnWorkspaceChanges()
        {
            // Arrange
            var observer = new TestObserver<WorkspaceId?>();
            
            // Act
            _service.OnWorkspaceOpen(_testWorkspaceId);
            using (_service.Subscribe(observer))
            {
                _service.OnWorkspaceClose(_testWorkspaceId);
            }
            
            // Assert
            Assert.Multiple(() =>
            {
                Assert.That(observer.Values[0], Is.EqualTo(_testWorkspaceId));
                Assert.That(observer.Values[1], Is.Null);
            });
        }
    }
}
