using GitLab.Extension.Workspace;
using GitLab.Extension.Workspace.Model;
using Microsoft.VisualStudio.Editor;
using Microsoft.VisualStudio.Shell.Interop;
using Microsoft.VisualStudio.Text;
using Microsoft.VisualStudio.Text.Editor;
using Microsoft.VisualStudio.TextManager.Interop;
using Microsoft.VisualStudio.Utilities;
using Moq;
using NUnit.Framework;

namespace GitLab.Extension.Tests.Workspace
{
    [TestFixture]
    public class TextDocumentInfoExtensionsTests
    {
        private const string FilePath = @"C:\Path\To\TestFile.cs";
        private const string SolutionPath = @"C:\Path\To";
        private const string FileName = "TestFile.cs";
        private const string FileType = "CSharp";
        private Mock<IVsEditorAdaptersFactoryService> _adaptersFactoryMock;
        private Mock<ITextDocumentFactoryService> _textDocumentFactoryMock;
        private Mock<IWpfTextView> _wpfTextViewMock;
        private Mock<ITextDocument> _textDocumentMock;
        private Mock<ITextBuffer> _textBufferMock;
        private Mock<IContentType> _contentTypeMock;
        private WorkspaceId _workspaceId;

        [SetUp]
        public void SetUp()
        {
            _textDocumentFactoryMock = new Mock<ITextDocumentFactoryService>();
            _wpfTextViewMock = new Mock<IWpfTextView>();
            _textDocumentMock = new Mock<ITextDocument>();
            _textBufferMock = new Mock<ITextBuffer>();
            _contentTypeMock = new Mock<IContentType>();
            _workspaceId = new WorkspaceId("TestSolution", SolutionPath);
            
            var textDocument = _textDocumentMock.Object;
            _textDocumentFactoryMock.Setup(f => f.TryGetTextDocument(It.IsAny<ITextBuffer>(), out textDocument)).Returns(true);
            _wpfTextViewMock.Setup(v => v.TextBuffer).Returns(_textBufferMock.Object);
            _textDocumentMock.Setup(d => d.FilePath).Returns(FilePath);
            _textDocumentMock.Setup(d => d.TextBuffer).Returns(_textBufferMock.Object);
            _textBufferMock.Setup(b => b.ContentType).Returns(_contentTypeMock.Object);
            var persistFileFormat = new Mock<IPersistFileFormat>();
            var filePath = FilePath;
            uint something;
            persistFileFormat.Setup(f => f.GetCurFile(out filePath, out something))
                .Returns(0);
            var properties = new PropertyCollection();
            properties.AddProperty(typeof(IVsTextBuffer), persistFileFormat.Object);
            _textBufferMock.Setup(b => b.Properties).Returns(properties);
            _contentTypeMock.Setup(c => c.TypeName).Returns(FileType);
        }

        [Test]
        [ExecuteOnMainThread]
        public void GetTextDocumentInfo_FromIWpfTextView_ReturnsExpectedTextDocumentInfo()
        {
            // Act
            var result = _wpfTextViewMock.Object.GetTextViewInfo(_workspaceId);

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(FileName, result.RelativePath);
            Assert.AreEqual(FileType, result.TypeName);
            Assert.AreEqual(_workspaceId, result.Workspace);
        }

        [Test]
        [ExecuteOnMainThread]
        public void GetTextDocumentInfo_FromITextBuffer_ReturnsExpectedTextDocumentInfo()
        {
            // Arrange
            var textBuffer = _textBufferMock.Object;

            // Act
            var result = textBuffer.GetTextViewInfo(_textDocumentFactoryMock.Object, _workspaceId);

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(FileName, result.RelativePath);
            Assert.AreEqual(FileType, result.TypeName);
            Assert.AreEqual(_workspaceId, result.Workspace);
        }
    }
}
