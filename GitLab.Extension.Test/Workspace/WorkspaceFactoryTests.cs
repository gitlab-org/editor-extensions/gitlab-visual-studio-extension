using System.Threading.Tasks;
using GitLab.Extension.Workspace;
using NUnit.Framework;

namespace GitLab.Extension.Tests.Workspace
{
    [TestFixture]
    public class WorkspaceFactoryTests
    {
        [Test]
        public async Task CreateWorkspaceAsync_ReturnsSuccessResultWithCorrectWorkspaceIdAsync()
        {
            // Arrange
            var factory = new WorkspaceFactory();
            var workspaceId = new WorkspaceId("TestSolution", "path/to/solution");

            // Act
            var result = await factory.CreateWorkspaceAsync(workspaceId);

            // Assert
            
            Assert.IsTrue(result.IsSuccess(out var workspace));
            Assert.IsNotNull(workspace);
            Assert.AreEqual(workspaceId, workspace.Id);
        }
    }
}
