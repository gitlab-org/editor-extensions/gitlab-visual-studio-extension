using GitLab.Extension.Workspace;
using NUnit.Framework;

namespace GitLab.Extension.Tests.Workspace
{
    [TestFixture]
    public class WorkspaceIdTests
    {
        [Test]
        public void Equals_SameValues_ReturnsTrue()
        {
            var workspaceId1 = new WorkspaceId("SolutionA", "/path/to/solutionA");
            var workspaceId2 = new WorkspaceId("SolutionA", "/path/to/solutionA");

            Assert.True(workspaceId1.Equals(workspaceId2));
        }

        [Test]
        public void Equals_DifferentSolutionName_ReturnsFalse()
        {
            var workspaceId1 = new WorkspaceId("SolutionA", "/path/to/solutionA");
            var workspaceId2 = new WorkspaceId("SolutionB", "/path/to/solutionA");

            Assert.False(workspaceId1.Equals(workspaceId2));
        }
        
        [Test]
        public void Equals_DifferentSolutionPath_ReturnsFalse()
        {
            var workspaceId1 = new WorkspaceId("SolutionA", "/path/to/solutionA");
            var workspaceId2 = new WorkspaceId("SolutionA", "/path/to/solutionB");

            Assert.False(workspaceId1.Equals(workspaceId2));
        }
        
        [Test]
        public void OperatorEquals_WithEqualObjects_ReturnsTrue()
        {
            var workspaceId1 = new WorkspaceId("SolutionA", "/path/to/solutionA");
            var workspaceId2 = new WorkspaceId("SolutionA", "/path/to/solutionA");

            Assert.IsTrue(workspaceId1 == workspaceId2);
        }

        [Test]
        public void OperatorEquals_WithUnequalObjects_ReturnsFalse()
        {
            var workspaceId1 = new WorkspaceId("SolutionA", "/path/to/solutionA");
            var workspaceId2 = new WorkspaceId("SolutionB", "/path/to/solutionA");

            Assert.IsFalse(workspaceId1 == workspaceId2);
        }
        
        [Test]
        public void OperatorNotEquals_WithEqualObjects_ReturnsFalse()
        {
            var workspaceId1 = new WorkspaceId("SolutionA", "/path/to/solutionA");
            var workspaceId2 = new WorkspaceId("SolutionA", "/path/to/solutionA");

            Assert.IsFalse(workspaceId1 != workspaceId2);
        }

        [Test]
        public void OperatorNotEquals_WithUnequalObjects_ReturnsTrue()
        {
            var workspaceId1 = new WorkspaceId("SolutionA", "/path/to/solutionA");
            var workspaceId2 = new WorkspaceId("SolutionA", "/path/to/solutionB");

            Assert.IsTrue(workspaceId1 != workspaceId2);
        }
        
        [Test]
        public void GetHashCode_MultipleHashCodeGenerations_ReturnsSameValue()
        {
            var workspaceId = new WorkspaceId("SolutionA", "/path/to/solutionA");
            int hashCode1 = workspaceId.GetHashCode();
            int hashCode2 = workspaceId.GetHashCode();

            Assert.AreEqual(hashCode1, hashCode2);
        }

        [Test]
        public void GetHashCode_DifferentObjects_ReturnsDifferentValues()
        {
            var workspaceId1 = new WorkspaceId("SolutionA", "/path/to/solutionA");
            var workspaceId2 = new WorkspaceId("SolutionB", "/path/to/solutionB");

            Assert.AreNotEqual(workspaceId1.GetHashCode(), workspaceId2.GetHashCode());
        }

        [Test]
        public void GetHashCode_SameProperties_ReturnsSameHashCode()
        {
            var workspaceId1 = new WorkspaceId("SolutionA", "/path/to/solutionA");
            var workspaceId2 = new WorkspaceId("SolutionA", "/path/to/solutionA");

            Assert.AreEqual(workspaceId1.GetHashCode(), workspaceId2.GetHashCode());
        }
    }
}
