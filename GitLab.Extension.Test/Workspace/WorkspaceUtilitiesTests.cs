using System.Diagnostics.CodeAnalysis;
using System.Threading.Tasks;
using EnvDTE;
using EnvDTE80;
using GitLab.Extension.Workspace;
using Microsoft.VisualStudio.Shell.Interop;
using Moq;
using NUnit.Framework;

namespace GitLab.Extension.Tests.Workspace
{
    [TestFixture]
    [SuppressMessage("Usage", "VSTHRD010:Invoke single-threaded types on Main thread")]
    public class WorkspaceUtilitiesTests
    {
        private Mock<DTE2> _dte2Mock;
        private Mock<Solution> _solutionMock;
        
        [SetUp]
        public void SetUp()
        {
            _solutionMock = new Mock<Solution>();
            _dte2Mock = new Mock<DTE2>();
            _dte2Mock.Setup(d => d.Solution).Returns(_solutionMock.Object);
            
            AssemblySetup.MockServiceProvider.Reset();
            AssemblySetup.MockServiceProvider.AddService(typeof(SDTE), _dte2Mock.Object);
        }

        [Test]
        [ExecuteOnMainThread]
        public void GetCurrentWorkspaceId_ReturnsExpectedWorkspaceId()
        {
            // Arrange
            const string expectedFullName = @"C:\Path\To\MySolution.sln";
            const string expectedFileName = @"C:\Path\To\MySolution.sln";
            const string expectedSolutionName = "MySolution";
            const string expectedSolutionPath = @"C:\Path\To";
            
            _solutionMock.Setup(x => x.FullName).Returns(expectedFullName);
            _solutionMock.Setup(x => x.FileName).Returns(expectedFileName);

            var expectedWorkspaceId = new WorkspaceId(expectedSolutionName, expectedSolutionPath);
            
            // Act
            var actualWorkspaceId = WorkspaceUtilities.GetCurrentWorkspaceId();

            // Assert
            Assert.AreEqual(expectedWorkspaceId, actualWorkspaceId);
        }
        
        [Test]
        [ExecuteOnMainThread]
        public async Task GetCurrentWorkspaceIdAsync_ReturnsExpectedWorkspaceIdAsync()
        {
            // Arrange
            const string expectedFullName = @"C:\Path\To\MySolution.sln";
            const string expectedFileName = @"C:\Path\To\MySolution.sln";
            const string expectedSolutionName = "MySolution";
            const string expectedSolutionPath = @"C:\Path\To";
            
            _solutionMock.Setup(x => x.FullName).Returns(expectedFullName);
            _solutionMock.Setup(x => x.FileName).Returns(expectedFileName);

            var expectedWorkspaceId = new WorkspaceId(expectedSolutionName, expectedSolutionPath);
            
            // Act
            var actualWorkspaceId = await WorkspaceUtilities.GetCurrentWorkspaceIdAsync();

            // Assert
            Assert.AreEqual(expectedWorkspaceId, actualWorkspaceId);
        }
    }
}
