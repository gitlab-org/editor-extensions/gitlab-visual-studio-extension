using System.Threading.Tasks;
using GitLab.Extension.Workspace;

namespace GitLab.Extension.CodeSuggestions
{
    public interface IGitLabCodeSuggestionTelemetryController
    {
        /// <summary>
        /// Should be called when a GitLab code suggestion proposal is shown to the user.
        /// </summary>
        /// <param name="uniqueRequestId">Unique request id for the shown suggestion.</param>
        /// <param name="workspaceId">Workspace id where this suggestion originates from.</param>
        /// <returns>A task representing the asynchronous operation.</returns>
        Task OnShownAsync(string uniqueRequestId, WorkspaceId workspaceId);
        
        /// <summary>
        /// Should be called when a GitLab code suggestion proposal is was never provided to the user.
        /// </summary>
        /// <param name="uniqueRequestId">Unique request id for the shown suggestion.</param>
        /// <param name="workspaceId">Workspace id where this suggestion originates from.</param>
        /// <returns>A task representing the asynchronous operation.</returns>
        Task OnNotProvidedAsync(string uniqueRequestId, WorkspaceId workspaceId);

        /// <summary>
        /// Should be called when a GitLab code suggestion proposal is rejected by the user.
        /// </summary>
        /// <param name="uniqueRequestId">Unique request id for the rejected suggestion.</param>
        /// <param name="workspaceId">Workspace id where this suggestion originates from.</param>
        /// <returns>A task representing the asynchronous operation.</returns>
        Task OnRejectedAsync(string uniqueRequestId, WorkspaceId workspaceId);

        /// <summary>
        /// Should be called when a GitLab code suggestion proposal is accepted by the user.
        /// </summary>
        /// <param name="uniqueRequestId">Unique request id for the accepted suggestion.</param>
        /// <param name="workspaceId">Workspace id where this suggestion originates from.</param>
        /// <param name="optionId">Option id of the selected suggestion.</param>
        /// <returns>A task representing the asynchronous operation.</returns>
        Task OnAcceptedAsync(string uniqueRequestId, WorkspaceId workspaceId, int? optionId = null);
    }
}
