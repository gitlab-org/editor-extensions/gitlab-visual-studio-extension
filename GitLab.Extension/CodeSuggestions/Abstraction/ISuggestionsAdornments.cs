using System;
using GitLab.Extension.CodeSuggestions.Model;
using Microsoft.VisualStudio.Text.Editor;

namespace GitLab.Extension.CodeSuggestions
{
    public interface ISuggestionsAdornments : IDisposable
    {
        public void RenderAdornment(SuggestionDisplaySession suggestionDisplaySession);
        public IWpfTextViewMargin GetMargin();
    }
}