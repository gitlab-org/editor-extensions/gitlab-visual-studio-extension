using Microsoft.VisualStudio.Text.Editor;

namespace GitLab.Extension.CodeSuggestions
{
    public interface ISuggestionsAdornmentsFactory
    {
        public ISuggestionsAdornments Create(IWpfTextView textView, ISuggestionEventBroker suggestionEventBroker);
    }
}