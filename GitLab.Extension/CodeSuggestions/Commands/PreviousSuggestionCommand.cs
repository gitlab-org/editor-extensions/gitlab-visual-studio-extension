using System;
using System.ComponentModel.Design;
using System.Threading.Tasks;
using GitLab.Extension.CodeSuggestions.Model;
using GitLab.Extension.Command;
using Microsoft.VisualStudio.Shell;

namespace GitLab.Extension.CodeSuggestions.Commands
{
    public class PreviousSuggestionCommand : ICommand
    {
        private readonly ISuggestionEventBroker _eventBroker;
        
        public CommandID CommandID { get; } = new CommandID(
            PackageIds.GitLabPackageCmdSet,
            PackageIds.CmdPrevSuggestion);
        
        public PreviousSuggestionCommand(ISuggestionEventBroker eventBroker)
        {
            _eventBroker = eventBroker;
        }
        
        public async Task InitializeAsync(IServiceProvider serviceProvider)
        {
            await ThreadHelper.JoinableTaskFactory.SwitchToMainThreadAsync();

            var oleMenuItem = new OleMenuCommand(
                (obj, args) => ThreadHelper.JoinableTaskFactory.Run(ExecuteAsync),
                CommandID);

            var commandService = serviceProvider.GetService(typeof(IMenuCommandService)) as OleMenuCommandService;

            commandService?.AddCommand(oleMenuItem);
        }
        
        private Task ExecuteAsync()
        {
            _eventBroker.Publish(new SuggestionPreviousEvent(null));
            return Task.CompletedTask;
        }
    }
}
