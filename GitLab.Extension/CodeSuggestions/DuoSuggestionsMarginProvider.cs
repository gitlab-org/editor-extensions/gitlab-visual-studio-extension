using System.ComponentModel.Composition;
using Autofac;
using GitLab.Extension.CodeSuggestions.UI;
using Microsoft.VisualStudio.Text.Editor;
using Microsoft.VisualStudio.Utilities;

namespace GitLab.Extension.CodeSuggestions
{
    [Export(typeof(IWpfTextViewMarginProvider))]
    [Name(DuoSuggestionMargin.MarginName)]
    [Order(After = PredefinedMarginNames.Left)]
    [MarginContainer(PredefinedMarginNames.Left)]
    [ContentType("code")]
    [TextViewRole(PredefinedTextViewRoles.Document)]
    public class DuoSuggestionsMarginProvider : IWpfTextViewMarginProvider
    {
        public IWpfTextViewMargin CreateMargin(IWpfTextViewHost wpfTextViewHost, IWpfTextViewMargin marginContainer)
        {
            var managerProvider = DependencyInjection.Instance.Scope
                .Resolve<ISuggestionManagerProvider>();

            return managerProvider?.GetSuggestionManager(wpfTextViewHost.TextView).GetMargin();
        }
    }
}