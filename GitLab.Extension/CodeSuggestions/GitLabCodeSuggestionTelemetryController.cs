using System;
using System.Threading.Tasks;
using GitLab.Extension.Workspace;
using Serilog;

namespace GitLab.Extension.CodeSuggestions
{
    public class GitLabCodeSuggestionTelemetryController :
        IGitLabCodeSuggestionTelemetryController
    {
        private readonly IWorkspaceLsClientProvider _lsClientProvider;
        private readonly ILogger _logger;
        
        public GitLabCodeSuggestionTelemetryController(IWorkspaceLsClientProvider lsClientProvider,
            ILogger logger)
        {
            _lsClientProvider = lsClientProvider;
            _logger = logger;
        }

        public async Task OnShownAsync(string uniqueRequestId, WorkspaceId workspaceId)
        {
            try
            {
                var lsClient = await _lsClientProvider.GetClientAsync(workspaceId);
                await lsClient.SendGitlabTelemetryCodeSuggestionShownAsync(uniqueRequestId);
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Failed to log code suggestion shown for {FilePath} and request Id {RequestId}", 
                    uniqueRequestId);
            }
        }

        public async Task OnNotProvidedAsync(string uniqueRequestId, WorkspaceId workspaceId)
        {
            try
            {
                var lsClient = await _lsClientProvider.GetClientAsync(workspaceId);
                await lsClient.SendGitlabTelemetryCodeSuggestionNotProvidedAsync(uniqueRequestId);
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Failed to log code suggestion not provided for request Id {RequestId}", 
                    uniqueRequestId);
            }
        }

        public async Task OnRejectedAsync(string uniqueRequestId, WorkspaceId workspaceId)
        {
            try {
                var lsClient = await _lsClientProvider.GetClientAsync(workspaceId);
                await lsClient.SendGitlabTelemetryCodeSuggestionRejectedAsync(uniqueRequestId);
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Failed to log code suggestion not provided for {FilePath} and request Id {RequestId}", 
                    uniqueRequestId);
            }
        }

        public async Task OnAcceptedAsync(string uniqueRequestId, WorkspaceId workspaceId, int? optionId = null)
        {
            try {
                var lsClient = await _lsClientProvider.GetClientAsync(workspaceId);
                await lsClient.SendGitlabTelemetryCodeSuggestionAcceptedAsync(uniqueRequestId, optionId);
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Failed to log code suggestion accepted for request Id {RequestId}", uniqueRequestId);
            }
        }
    }
}
