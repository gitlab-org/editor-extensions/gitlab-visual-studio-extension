using System.Collections.Generic;
using GitLab.Extension.CodeSuggestions.Model;
using Microsoft.VisualStudio.Language.Proposals;
using Microsoft.VisualStudio.Text.Editor;

namespace GitLab.Extension.CodeSuggestions
{
    public static class GitlabProposalCreator
    {
        public static GitLabProposal CreateProposal(
            Completion completion,
            IWpfTextView textView,
            CompletionState completionState)
        {
            const ProposalFlags flags =
                ProposalFlags.FormatAfterCommit |
                ProposalFlags.MoveCaretToEnd |
                ProposalFlags.DisableInIntelliSense |
                ProposalFlags.SimulateBraceCompletion;
            
            var displaySuggestion = completion.InsertText;

            var bufferPosition = textView.Caret.Position.BufferPosition;
            var line = bufferPosition.GetContainingLine();
            var column = bufferPosition.Position - line.Start.Position;

            // If the caret is in virtual space and the column position is zero,
            // it will look to the user like the caret is indented even through
            // the column is zero. Our code suggestion be based on a zero column
            // position and include the needed indentation.
            //
            // To make the display of the suggestion look correct to the user
            // we need to trim any preceding indentation. But only for the display
            // value. We want to insert with the correct indentation.
            if (textView.Caret.InVirtualSpace && column == 0)
                displaySuggestion = displaySuggestion.TrimStart();

            var edits = new List<ProposedEdit>
            {
                new ProposedEdit(
                    textView.GetTextElementSpan(textView.Caret.Position.BufferPosition),
                    displaySuggestion)
            };

            var proposalId = "gitlab:" + completion.UniqueTrackingId;
            var proposalMetadata =
                new GitLabProposalMetadata(
                    completion.UniqueTrackingId,
                    completion.StreamId,
                    completion.OptionId);

            return GitLabProposal.TryCreateProposal(
                proposalMetadata,
                null,
                edits,
                textView.Caret.Position.VirtualBufferPosition,
                completionState,
                flags,
                proposalId: proposalId);
        }
    }
}
