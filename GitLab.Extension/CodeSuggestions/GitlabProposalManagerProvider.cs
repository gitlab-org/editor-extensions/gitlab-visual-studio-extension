﻿using Autofac;
using Autofac.Core;
using Microsoft.VisualStudio.Language.Proposals;
using Microsoft.VisualStudio.Text.Editor;
using Microsoft.VisualStudio.Text.Tagging;
using Microsoft.VisualStudio.Threading;
using Microsoft.VisualStudio.Utilities;
using System.ComponentModel.Composition;
using System.Threading;
using System.Threading.Tasks;
using Serilog;
using System;
using GitLab.Extension.Status;
using GitLab.Extension.Workspace.Model;

namespace GitLab.Extension.CodeSuggestions
{
    [Export(typeof(GitlabProposalManagerProvider))]
    [Export(typeof(ProposalManagerProviderBase))]
    [Name("GitlabProposalManager")]
    [ContentType("code")]
    [Order(Before = "IntelliCodeCSharpProposalManager")]
    [Order(Before = "Highest Priority")]
    public class GitlabProposalManagerProvider : ProposalManagerProviderBase
    {
        public readonly IViewTagAggregatorFactoryService ViewTagAggregatorFactoryService;
        public readonly JoinableTaskFactory JoinableTaskFactory;
        private readonly ILanguageManager _languageManager;

        [ImportingConstructor]
        public GitlabProposalManagerProvider(
          IViewTagAggregatorFactoryService viewTagAggregatorFactoryService,
          [Import("GitLab.Extension.CodeSuggestions.PackageJoinableTaskFactory")] JoinableTaskFactory joinableTaskFactory)
        {
            ViewTagAggregatorFactoryService = viewTagAggregatorFactoryService;
            JoinableTaskFactory = joinableTaskFactory;

            _languageManager = DependencyInjection.Instance.Scope.Resolve<ILanguageManager>();
        }

        public override async Task<ProposalManagerBase> GetProposalManagerAsync(ITextView view, CancellationToken cancel)
        {
            await JoinableTaskFactory.SwitchToMainThreadAsync(cancel);

            try
            {
                // This is a workaround to a race condition where the extension package did not start its
                // initialization yet before the listener was registered as the VS component.
                Logging.ConfigureLogging();

                var contentType = view.TextBuffer.ContentType.DisplayName.ToLower();
                var extension = _languageManager.GetExtensionFromFilename(view.GetFilePath());
                var language = _languageManager.GetLanguage(contentType, extension);

                if (language == null || !language.FeatureCodeSuggestions)
                {
                    Log.Debug($"{nameof(GetProposalManagerAsync)}: Code suggestions disabled. ContentType ({contentType}) or file extension ({extension}) not supported.");
                    return null;
                }

                Log.Debug($"{nameof(GetProposalManagerAsync)}: Code suggestions enabled. ContentType ({contentType}) or file extension ({extension}) is supported.");

                var ret = DependencyInjection.Instance.Scope.Resolve<GitlabProposalManager>(new Parameter[]
                {
                    new TypedParameter(typeof(ITextView), view),
                    new TypedParameter(typeof(GitlabProposalManagerProvider), this),
                    new TypedParameter(typeof(Language), language)
                });

                return ret;
            }
            catch(Exception ex)
            {
                Log.Error(ex, "Error Creating instance of GitlabProposalManager: " + ex.Message);
                return null;
            }
        }
    }
}
