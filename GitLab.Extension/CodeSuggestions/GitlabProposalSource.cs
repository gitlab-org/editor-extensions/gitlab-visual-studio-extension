﻿using Microsoft.VisualStudio.Language.Proposals;
using Microsoft.VisualStudio.Text;
using Microsoft.VisualStudio.Text.Editor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Serilog;
using Microsoft.VisualStudio.Shell;
using Microsoft.VisualStudio.Threading;

namespace GitLab.Extension.CodeSuggestions
{
    /// <summary>
    /// Generate inline proposals for a unique IWpfTextView instance.
    /// Each open file will have its own GitlabProposalSource instance.
    /// When the file is closed, the associated GitlabProposalSource instance will be disposed.
    /// </summary>
    public class GitlabProposalSource : ProposalSourceBase
    {
        public const string SourceName = nameof(GitlabProposalSource);
        
        private readonly IWpfTextView _textView;
        private readonly ILogger _logger;
        
        private readonly ISuggestionManagerProvider _suggestionManagerProvider;
        
        public GitlabProposalSource(
            IWpfTextView textView,
            ISuggestionManagerProvider suggestionManagerProvider,
            ILogger logger)
        {
            _logger = logger;
            
            _textView = textView;
            _suggestionManagerProvider = suggestionManagerProvider;
        }

        /// <summary>
        /// Called by Visual Studio to get a list of proposals from our extension. This
        /// triggers a call to get a code suggestion.
        /// </summary>
        /// <param name="caret"></param>
        /// <param name="completionState"></param>
        /// <param name="scenario"></param>
        /// <param name="triggeringCharacter"></param>
        /// <param name="token"></param>
        /// <returns>Returns a GitlabProposalCollection instance or null on error.</returns>
        public override async Task<ProposalCollectionBase> RequestProposalsAsync(
            VirtualSnapshotPoint caret, 
            CompletionState completionState, 
            ProposalScenario scenario, 
            char triggeringCharacter, 
            CancellationToken token)
        {
            // ignore any completion requests for e.g. variables or function names
            if (scenario == ProposalScenario.Completion)
            {
                return null;
            }
            
            try
            {
                // Wait 150 milliseconds in case the user is typing quickly.
                token.WaitHandle.WaitOne(150);
                
                await ThreadHelper.JoinableTaskFactory.SwitchToMainThreadAsync();

                var manager = _suggestionManagerProvider.GetSuggestionManager(_textView);
                
                // switch back to background thread
                await TaskScheduler.Default;

                var completions = await manager.GetFirstCompletionsAsync(token);
                
                if (completions.Count == 0)
                {
                    return null;
                }

                var proposal = GitlabProposalCreator.CreateProposal(completions.First(), _textView, completionState);

                return new ProposalCollection(SourceName, new List<ProposalBase> {proposal});
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Error occured requesting proposals.");
                return null;
            }
        }
    }
}
