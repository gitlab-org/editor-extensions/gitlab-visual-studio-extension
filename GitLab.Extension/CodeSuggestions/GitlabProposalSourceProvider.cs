﻿using Microsoft.VisualStudio.Language.Proposals;
using Microsoft.VisualStudio.Text.Editor;
using Microsoft.VisualStudio.Utilities;
using System;
using System.ComponentModel.Composition;
using System.Threading;
using System.Threading.Tasks;
using Serilog;
using Autofac;

namespace GitLab.Extension.CodeSuggestions
{
    /// <summary>
    /// This class is exported and discovered by visual studio.
    /// It can be considered the entrypoint for code suggestions.
    /// </summary>
    /// <remarks>
    /// For each open file a call to GetProposalSource is made returning
    /// a unique instance of GitlabProposalSource.
    /// </remarks>
    [Export(typeof(GitlabProposalSourceProvider)), 
        Export(typeof(ProposalSourceProviderBase)), 
        Name("GitlabProposalSourceProvider"),
        ContentType("code"),
        Order(Before = "InlineCSharpProposalSourceProvider"), 
        Order(Before = "Highest Priority")]
    public class GitlabProposalSourceProvider : ProposalSourceProviderBase, IDisposable
    {
        [Export(typeof(AdornmentLayerDefinition))]
        [Name(Constants.SuggestionsAdornmentLayerName)]
        [Order(After = PredefinedAdornmentLayers.Text)]
        public AdornmentLayerDefinition SuggestionsAdornmentLayer;
        
        /// <summary>
        /// Create an instance of GitlabProposalSource for the ITextView. 
        /// Each open file in the IDE will have its own GitlabProposalSource.
        /// </summary>
        /// <param name="view"></param>
        /// <param name="cancel"></param>
        /// <returns></returns>
        public override async Task<ProposalSourceBase> GetProposalSourceAsync(ITextView view, CancellationToken cancel)
        {
            try
            {
                Log.Debug("GitlabProposalSourceProvider.GetProposalSourceAsync");

                var proposalSource = DependencyInjection.Instance.Scope
                    .Resolve<GitlabProposalSource>(
                        new TypedParameter(typeof(IWpfTextView), view as IWpfTextView));

                return proposalSource;
            }
            catch(Exception ex)
            {
                Log.Debug(ex, $"GitlabProposalSourceProvider.GetProposalSourceAsync exception");
                return null;
            }
        }

        public void Dispose()
        {
        }
    }
}
