﻿using GitLab.Extension.LanguageServer;
using System.Collections.Generic;

namespace GitLab.Extension.CodeSuggestions
{
    /// <summary>
    /// A language supported by this extension.
    /// This allows:
    ///     - Enabling/disabling based on ContentType
    ///     - Tracking features supported by language as the extension grows.
    /// </summary>
    public class Language : ILsLanguage
    {
        public string Name;
        public string ContentType;
        public bool FeatureCodeSuggestions;
        public IReadOnlyList<string> Extensions;
        public IReadOnlyList<char> TriggerCharacters;

        public LsLanguageId LanguageId { get; }

        public Language(string name, string contentType, LsLanguageId languageId) :
            this(name, contentType, languageId, new string[0], new char[0])
        {
        }

        public Language(
            string name, 
            string contentType, 
            LsLanguageId languageId, 
            IReadOnlyList<string> extensions,
            IReadOnlyList<char> triggerCharacters)
        {
            Name = name;
            ContentType = contentType.ToLower();
            Extensions = extensions;
            FeatureCodeSuggestions = true;
            LanguageId = languageId;
            TriggerCharacters = triggerCharacters;
        }
    }
}
