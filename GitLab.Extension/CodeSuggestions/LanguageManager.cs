﻿using System;
using System.Collections.Generic;
using System.Linq;
using GitLab.Extension.LanguageServer;

namespace GitLab.Extension.CodeSuggestions
{
    /// <summary>
    /// Defines all the supported languages and provides
    /// helper methods for determining if a feature can
    /// be provided to a specific language.
    /// </summary>
    public class LanguageManager : ILanguageManager
    {
        private IReadOnlyList<Language> _languages;

        public LanguageManager()
        {
            _languages = new Language[]
            {
                new Language("ABAP", "abap", LsLanguageId.ABAP, new[] {"abap"}, new[] {'*'}),
                new Language("BibTeX", "bibtex", LsLanguageId.BibTeX, new[] {"bib"}, new[] {'%'}),
                new Language("C", "c", LsLanguageId.C, new[] {"c", "h"}, new[] {'/', '{'}),
                new Language("C++", "cpp", LsLanguageId.CPP, new[] {"cpp", "hpp"}, new[] {'/', '{'}),
                new Language("C++", "c/c++", LsLanguageId.CPP, new[] {"cpp", "hpp"}, new[] {'/', '{'}),
                new Language("C#", "csharp", LsLanguageId.CSharp, new[] {"cs"}, new[] {'/', '{'}),
                new Language("Clojure", "clojure", LsLanguageId.Clojure, new[] {"clj", "cljs", "cljc"}, new[] {';'}),
                new Language("CoffeeScript", "coffeescript", LsLanguageId.Coffeescript, new[] {"coffee"}, new[] {'#'}),
                new Language("CSS", "css", LsLanguageId.CSS, new[] {"css"}, new[] {'/'}),
                new Language("CUDA C++", "cuda-cpp", LsLanguageId.CudaCpp, new string[0], new[] {'/', '{'}),
                new Language("Dart", "dart", LsLanguageId.Dart, new[] {"dart"}, new[] {'/', '{'}),
                new Language("Diff", "diff", LsLanguageId.Diff, new[] {"diff", "patch"}, new char[0]),
                new Language("Dockerfile", "dockerfile", LsLanguageId.Dockerfile, new[] {"Dockerfile"}, new[] {'#'}),
                new Language("Elixir", "elixir", LsLanguageId.Elixir, new[] {"ex", "exs"}, new[] {'#'}),
                new Language("Erlang", "erlang", LsLanguageId.Erlang, new[] {"erl", "hrl"}, new[] {'%'}),
                new Language("F#", "fsharp", LsLanguageId.FSharp, new[] {"fs", "fsi", "fsx"}, new[] {'/', '{'}),
                new Language("Git Commit", "git-commit", LsLanguageId.GitCommit, new[] {"COMMIT_EDITMSG", "MERGE_MSG"}, new[] {'#'}),
                new Language("Git Rebase", "git-rebase", LsLanguageId.GitRebase, new[] {"git-rebase-todo"}, new[] {'#'}),
                new Language("Golang", "go", LsLanguageId.Go, new[] {"go"}, new[] {'/', '{'}),
                new Language("Groovy", "groovy", LsLanguageId.Groovy, new[] {"groovy", "gvy", "gy", "gsh"}, new[] {'/', '{'}),
                new Language("HAML", "haml", LsLanguageId.HAML, new[] {"haml"}, new[] {'-', '%'}),
                new Language("Handlebars", "handlebars", LsLanguageId.Handlebars, new[] {"hbs", "handlebars"}, new[] {'{'}),
                new Language("HTML", "html", LsLanguageId.HTML, new[] {"html"}, new[] {'<', '-'}),
                new Language("Ini", "ini", LsLanguageId.Ini, new[] {"ini"}, new[] {';'}),
                new Language("Java", "java", LsLanguageId.Java, new[] {"java"}, new[] {'/', '{'}),
                new Language("JavaScript", "javascript", LsLanguageId.JavaScript, new[] {"js"}, new[] {'/', '{'}),
                new Language("JavaScript React", "javascriptreact", LsLanguageId.JavaScriptReact, new[] {"jsx"}, new[] {'/', '{'}),
                new Language("JSON", "json", LsLanguageId.JSON, new[] {"json"}, new[] {'{', '['}),
                new Language("Kotlin", "kotlin", LsLanguageId.Kotlin, new[] {"kt"}, new[] {'/', '{'}),
                new Language("LaTeX", "latex", LsLanguageId.LaTeX, new[] {"tex"}, new[] {'%'}),
                new Language("Less", "less", LsLanguageId.Less, new[] {"less"}, new[] {'/'}),
                new Language("Lua", "lua", LsLanguageId.Lua, new[] {"lua"}, new[] {'-'}),
                new Language("Makefile", "makefile", LsLanguageId.Makefile, new[] {"Makefile", "makefile"}, new[] {'#'}),
                new Language("Markdown", "markdown", LsLanguageId.Markdown, new[] {"md"}, new[] {'<', '-'}),
                new Language("Objective-C", "objective-c", LsLanguageId.ObjectiveC, new[] {"m"}, new[] {'/', '{'}),
                new Language("Objective-C++", "objective-cpp", LsLanguageId.ObjectiveCpp, new[] {"mm"}, new[] {'/', '{'}),
                new Language("Perl", "perl", LsLanguageId.Perl, new[] {"pl", "pm"}, new[] {'#'}),
                new Language("Perl 6", "perl6", LsLanguageId.Perl6, new[] {"p6", "pl6", "pm6"}, new[] {'#'}),
                new Language("PHP", "php", LsLanguageId.Php, new[] {"php", "phtml", "php3", "php4", "php5", "phps"}, new[] {'/', '#'}),
                new Language("PowerShell", "powershell", LsLanguageId.Powershell, new[] {"ps1", "psm1", "psd1"}, new[] {'#'}),
                new Language("Pug", "jade", LsLanguageId.Pug, new[] {"pug", "jade"}, new[] {'/', '-'}),
                new Language("Python", "python", LsLanguageId.Python, new[] {"py", "pyw", "pyx"}, new[] {'#', ':'}),
                new Language("R", "r", LsLanguageId.R, new[] {"r", "R"}, new[] {'#'}),
                new Language("Razor", "razor", LsLanguageId.Razor, new[] {"cshtml"}, new[] {'@', '<'}),
                new Language("Ruby", "ruby", LsLanguageId.Ruby, new[] {"rb", "rbw"}, new[] {'#'}),
                new Language("Rust", "rust", LsLanguageId.Rust, new[] {"rs"}, new[] {'/', '{'}),
                new Language("SASS", "sass", LsLanguageId.SASS, new[] {"sass"}, new[] {'/'}),
                new Language("Scala", "scala", LsLanguageId.Scala, new[] {"scala", "sc"}, new[] {'/'}),
                new Language("SCSS", "scss", LsLanguageId.SCSS, new[] {"scss"}, new[] {'/'}),
                new Language("ShaderLab", "shaderlab", LsLanguageId.ShaderLab, new[] {"shader"}, new[] {'/'}),
                new Language("Shell Script", "shellscript", LsLanguageId.ShellScript, new[] {"sh"}, new[] {'#'}),
                new Language("SQL", "sql", LsLanguageId.SQL, new[] {"sql"}, new char[0]),
                new Language("Svelte", "svelte", LsLanguageId.Svelte, new[] {"svelte"}, new[] {'<', '{'}),
                new Language("Swift", "swift", LsLanguageId.Swift, new[] {"swift"}, new[] {'/'}),
                new Language("Terraform", "terraform", LsLanguageId.Terraform, new[] {"tf", "tfvars"}, new[] {'#'}),
                new Language("TeX", "tex", LsLanguageId.TeX, new[] {"tex"}, new[] {'%'}),
                new Language("TypeScript", "typescript", LsLanguageId.TypeScript, new[] {"ts"}, new[] {'/', '{'}),
                new Language("TypeScript React", "typescriptreact", LsLanguageId.TypeScriptReact, new[] {"tsx"}, new[] {'/', '{'}),
                new Language("Visual Basic", "vb", LsLanguageId.VisualBasic, new[] {"vb"}, new [] {'\''}),
                new Language("Vue", "vue", LsLanguageId.Vue, new[] {"vue"}, new[] {'<', '{'}),
                new Language("Windows Bat", "bat", LsLanguageId.WindowsBat, new[] {"bat", "cmd"}, new[] {'@', ':'}),
                new Language("XML", "xml", LsLanguageId.XML, new[] {"xml", "xsl", "xsd"}, new[] {'<', '?'}),
                new Language("XSL", "xsl", LsLanguageId.XSL, new[] {"xsl"}, new[] {'<', '?'}),
                new Language("YAML", "yaml", LsLanguageId.YAML, new[] {"yml", "yaml"}, new[] {'#'})
            };
        }

        public IReadOnlyList<Language> Languages
        {
            get { return _languages; }
            set { _languages = value; }
        }

        public string GetExtensionFromFilename(string filename)
        {
            if (string.IsNullOrWhiteSpace(filename))
                return "";

            var periodIndex = filename.LastIndexOf('.');
            if (periodIndex == -1 || periodIndex+1 >= filename.Length)
                return "";

            return filename.Substring(periodIndex + 1);
        }

        private string FixContentType(string contentType)
        {
            contentType = contentType.ToLower();

            if (contentType.StartsWith("code++."))
                contentType = contentType.Substring("code++.".Length);

            return contentType;
        }

        public bool CheckFeatureCodeSuggestion(string contentType, string extension)
        {
            contentType = FixContentType(contentType);

            var ret = _languages
                .Any(x => x.FeatureCodeSuggestions == true && 
                     (x.ContentType == contentType || x.Extensions.Contains(extension)));

            return ret;
        }

        public Language GetLanguage(string contentType, string extension)
        {
            contentType = FixContentType(contentType);

            var ret = _languages
                .FirstOrDefault(x => x.ContentType == contentType || x.Extensions.Contains(extension));

            return ret;
        }
    }
}
