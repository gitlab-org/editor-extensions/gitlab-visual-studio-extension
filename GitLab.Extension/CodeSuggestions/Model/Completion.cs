using GitLab.Extension.LanguageServer.Models;

namespace GitLab.Extension.CodeSuggestions.Model
{
    public class Completion
    {
        private const string SUGGESTION_ACCEPTED_COMMAND = "gitlab.ls.codeSuggestionAccepted";
        private const string START_STREAMING_COMMAND = "gitlab.ls.startStreaming";
        public string UniqueTrackingId { get; }
        public string InsertText { get; }
        public bool IsStreaming => StreamId != null;
        public string StreamId { get; }
        public int? OptionId { get; }
        
        public Completion(string uniqueTrackingId, string insertText, string streamId = null, int? optionId = null)
        {
            InsertText = insertText;
            StreamId = streamId;
            UniqueTrackingId = uniqueTrackingId;
            OptionId = optionId;
        }
        
        public Completion(string uniqueTrackingId, CompletionChunk completionChunk)
        {
            InsertText = completionChunk.InsertText;
            UniqueTrackingId = uniqueTrackingId;
            StreamId = completionChunk.StreamId;
            
            // streams do not support multiple options
            OptionId = null;
        }

        public Completion(CompletionItem completionItem)
        {
            InsertText = completionItem.InsertText;
            StreamId = completionItem.Command?.CommandName switch
            {
                START_STREAMING_COMMAND => TryGetArgumentAsString(completionItem.Command, 0),
                _ => null
            };
            UniqueTrackingId = completionItem.Command?.CommandName switch
            {
                SUGGESTION_ACCEPTED_COMMAND => TryGetArgumentAsString(completionItem.Command, 0),
                START_STREAMING_COMMAND => TryGetArgumentAsString(completionItem.Command, 1),
                _ => null
            };
            if (!IsStreaming)
            {
                OptionId = TryGetArgumentAsInt(completionItem.Command, 1);
            }
        }

        private static string TryGetArgumentAsString(LanguageServer.Models.Command command, int index)
        {
            if (command?.Arguments == null || index >= command.Arguments.Count)
            {
                return null;
            }

            return command.Arguments[index] as string;
        }

        private static int? TryGetArgumentAsInt(LanguageServer.Models.Command command, int index)
        {
            if (command?.Arguments == null || index >= command.Arguments.Count)
            {
                return null;
            }

            if (command.Arguments[index] is int)
            {
                return command.Arguments[index] as int?;
            }

            var text = command.Arguments[index] as string;

            if (text == null)
            {
                return null;
            }

            return int.Parse(text);
        }
    }
}