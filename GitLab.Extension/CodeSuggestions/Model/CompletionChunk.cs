using GitLab.Extension.LanguageServer.Models;

namespace GitLab.Extension.CodeSuggestions.Model
{
    public class CompletionChunk
    {
        public string InsertText { get; }
        public string StreamId { get; }
        public bool IsLast { get; }
        
        public CompletionChunk(string insertText, string streamId, bool isLast)
        {
            StreamId = streamId;
            InsertText = insertText;
            IsLast = isLast;
        }
        
        public CompletionChunk(StreamingSuggestionNotification streamingSuggestion)
        {
            StreamId = streamingSuggestion.Id;
            InsertText = streamingSuggestion.Completion;
            IsLast = streamingSuggestion.Done;
        }
    }
}
