using System;

namespace GitLab.Extension.CodeSuggestions.Model
{
    public class GitLabProposalMetadata
    {
        public GitLabProposalMetadata(
            string uniqueRequestId,
            string streamId = null,
            int? optionId = null)
        {
            UniqueRequestId = uniqueRequestId;
            StreamId = streamId;
            OptionId = optionId;
        }

        /// <summary>
        /// The Telemetry ID used for tracking the proposal's usage and behavior
        /// </summary>
        public string UniqueRequestId { get; }

        /// <summary>
        /// The StreamId used to track updates of the displayed proposal
        /// </summary>
        public string StreamId { get; }
        
        /// <summary>
        /// Option ID of the proposal item applicable to multiple suggestions provided in one response
        /// </summary>
        public int? OptionId { get; }
        
        public override bool Equals(object obj)
        {
            if (obj is GitLabProposalMetadata other)
            {
                return UniqueRequestId == other.UniqueRequestId &&
                       StreamId == other.StreamId &&
                       OptionId == other.OptionId;
            }
            return false;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(UniqueRequestId, StreamId, OptionId);
        }

        public static bool operator ==(GitLabProposalMetadata left, GitLabProposalMetadata right)
        {
            return left?.Equals(right) ?? right is null;
        }

        public static bool operator !=(GitLabProposalMetadata left, GitLabProposalMetadata right)
        {
            return !(left == right);
        }
    }
}
