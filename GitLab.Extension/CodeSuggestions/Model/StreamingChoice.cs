namespace GitLab.Extension.CodeSuggestions.Model
{
    public class StreamingChoice : ISuggestionChoice
    {
        public string InsertText { get; }
        public string UniqueTrackingId { get; }
        public bool Done { get; }

        public StreamingChoice(string insertText, string uniqueTrackingId, bool done)
        {
            InsertText = insertText;
            UniqueTrackingId = uniqueTrackingId;
            Done = done;
        }
    }
}