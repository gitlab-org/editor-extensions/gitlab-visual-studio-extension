using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace GitLab.Extension.CodeSuggestions.Model
{
    /// <summary>
    /// Represents a thread safe logical state of the currently displayed suggestion to the user.
    /// </summary>
    public class SuggestionDisplaySession : IDisposable
    {
        private readonly List<Completion> _completions = new List<Completion>();
        private readonly SortedSet<int> _shownCompletions = new SortedSet<int>();
        
        // All operations on the session are very fast given that we have only a couple of suggestions at a time
        // but "if race condition is possible it WILL happen", so we should ensure we always return read-only copies
        // and any read operation is blocked while we are writing anything
        private readonly ReaderWriterLockSlim _lock = new ReaderWriterLockSlim();
        
        private int _currentIndex = 0;
        private SuggestionState _state = SuggestionState.None;
        private bool _loadAdditional = false;
        private bool _streaming = false;
        private CancellationTokenSource _cancellationTokenSource = null;

        public SuggestionState State
        {
            get
            {
                _lock.EnterReadLock();
                try
                {
                    return _state;
                }
                finally
                {
                    _lock.ExitReadLock();
                }
            }
        }

        public bool Streaming
        {
            get
            {
                _lock.EnterReadLock();
                try
                {
                    return _streaming;
                }
                finally
                {
                    _lock.ExitReadLock();
                }
            }
        }

        public bool LoadAdditional
        {
            get
            {
                _lock.EnterReadLock();
                try
                {
                    return _loadAdditional;
                }
                finally
                {
                    _lock.ExitReadLock();
                }
            }
        }

        public IReadOnlyList<Completion> Completions {
            get
            {
                _lock.EnterReadLock();
                try
                {
                    return _completions.ToList().AsReadOnly();
                }
                finally
                {
                    _lock.ExitReadLock();
                }
            }
        }

        public int CurrentIndex
        {
            get
            {
                _lock.EnterReadLock();
                try
                {
                    return _currentIndex;
                }
                finally
                {
                    _lock.ExitReadLock();
                }
            }
        }

        public Completion CurrentCompletion
        {
            get
            {
                _lock.EnterReadLock();
                try
                {
                    return _completions[_currentIndex];
                }
                finally
                {
                    _lock.ExitReadLock();
                }
            }
        }
        
        public CancellationToken CancellationToken
        {
            get {
                _lock.EnterReadLock();
                try
                {
                    return _cancellationTokenSource.Token;
                }
                finally
                {
                    _lock.ExitReadLock();
                }
            }
        }

        public void New()
        {
            _lock.EnterWriteLock();
            try
            {
                _currentIndex = 0;
                _completions.Clear();
                _shownCompletions.Clear();
                _state = SuggestionState.Loading;
                DisposeCancellation();
            }
            finally
            {
                _lock.ExitWriteLock();
            }
        }

        /// <summary>
        /// Resets the state of the session with given completions to represent a new session.
        /// </summary>
        /// <param name="completions">A list of initial completions</param>
        /// <param name="loadAdditional">Do we expect more suggestions to be loaded</param>
        public void Start(List<Completion> completions, bool loadAdditional = false)
        {
            _lock.EnterWriteLock();
            try
            {
                _state = SuggestionState.Loading;
                _loadAdditional = loadAdditional;
                _streaming = !loadAdditional;
                _cancellationTokenSource = new CancellationTokenSource();

                _completions.AddRange(completions);

                if (completions.Count > 0)
                {
                    _shownCompletions.Add(0);
                }
            }
            finally
            {
                _lock.ExitWriteLock();
            }
        }
        
        /// <summary>
        /// Moves the session to an error state where some error occured.
        /// </summary>
        public void Error()
        {
            _lock.EnterWriteLock();
            try
            {
                _state = SuggestionState.Error;
                DisposeCancellation();
            }
            finally
            {
                _lock.ExitWriteLock();
            }
        }
        
        /// <summary>
        /// Moves the session to a shown state, where user has a suggestion displayed.
        /// </summary>
        public void Shown()
        {
            _lock.EnterWriteLock();
            try
            {
                _state = SuggestionState.Shown;
            }
            finally
            {
                _lock.ExitWriteLock();
            }
        }
        
        /// <summary>
        /// Loads completions to the end of the list ensuring there are no duplicated completions
        /// with the same InsertText with all trailing whitespace removed.
        /// </summary>
        /// <param name="completions"></param>
        /// <returns>True if at least one completion was added, false if all were duplicates.</returns>
        public bool AdditionalLoaded(List<Completion> completions = null)
        {
            _lock.EnterWriteLock();
            try
            {
                _loadAdditional = false;
                _state = SuggestionState.Shown;
                if (completions == null)
                {
                    return false;
                }
                var added = false;
                completions.ForEach(c =>
                {
                    var trimmedInsertText = c.InsertText.Trim();
                    if (_completions.All(existing => existing.InsertText.Trim() != trimmedInsertText))
                    {
                        _completions.Add(c);
                        added = true;
                    }
                });
                return added;
            }
            finally
            {
                _lock.ExitWriteLock();
            }
        }

        /// <summary>
        /// Updates relevant completion with the given completion chunk.
        /// </summary>
        /// <param name="chunk"></param>
        /// <returns>True if managed to update any of completions.</returns>
        public bool ChunkLoaded(CompletionChunk chunk)
        {
            _lock.EnterWriteLock();
            try
            {
                var existingCompletionIndex = _completions.FindIndex(c => c.StreamId == chunk.StreamId);

                if (existingCompletionIndex < 0)
                {
                    return false;
                }
                
                if (_state == SuggestionState.Loading && chunk.IsLast)
                {
                    _state = SuggestionState.Shown;
                    return true;
                }
                
                _completions[existingCompletionIndex] = new Completion(_completions[existingCompletionIndex].UniqueTrackingId, chunk);
                return true;
            }
            finally
            {
                _lock.ExitWriteLock();
            }
        }
        
        /// <summary>
        /// Attempts to move to the next completion.
        /// </summary>
        public void Next()
        {
            _lock.EnterWriteLock();
            try
            {
                if (_currentIndex < _completions.Count - 1)
                {
                    _currentIndex++;
                }
                else
                {
                    _currentIndex = 0;
                }
                _shownCompletions.Add(_currentIndex);
            }
            finally
            {
                _lock.ExitWriteLock();
            }
        }
        
        /// <summary>
        /// Attempts to move to the previous completion.
        /// </summary>
        public void Previous()
        {
            _lock.EnterWriteLock();
            try
            {
                if (_currentIndex > 0)
                {
                    _currentIndex--;
                }
                else
                {
                    _currentIndex = _completions.Count - 1;
                }
                _shownCompletions.Add(_currentIndex);
            }
            finally
            {
                _lock.ExitWriteLock();
            }
        }
        
        /// <summary>
        /// Moves the session to a complete state where user either accepted or dismissed a suggestion.
        /// </summary>
        public void Complete()
        {
            _lock.EnterWriteLock();
            try
            {
                _state = SuggestionState.Complete;
                DisposeCancellation();
            }
            finally
            {
                _lock.ExitWriteLock();
            }
        }
        
        /// <summary>
        /// Gets a list of completions that were shown to user.
        /// </summary>
        public List<Completion> GetShownCompletions()
        {
            _lock.EnterReadLock();
            try
            {
                return _completions
                    .Where((_, index) => _shownCompletions.Contains(index))
                    .ToList();
            }
            finally
            {
                _lock.ExitReadLock();
            }
        }
        
        /// <summary>
        /// Gets a list of completions that were never shown to user.
        /// </summary>
        public List<Completion> GetNotShownCompletions()
        {
            _lock.EnterReadLock();
            try
            {
                return _completions
                    .Except(_shownCompletions.Select((_, index) => _completions[index]))
                    .ToList();
            }
            finally
            {
                _lock.ExitReadLock();
            }
        }
        
        public void Dispose()
        {
            DisposeCancellation();
        }

        private void DisposeCancellation()
        {
            if (_cancellationTokenSource == null)
            {
                return;
            }
            try
            {
                _cancellationTokenSource.Cancel();
                _cancellationTokenSource.Dispose();
            }
            catch (ObjectDisposedException)
            {
                // ignored
            }
            _cancellationTokenSource = null;
        }
    }
}
