using Microsoft.VisualStudio.Text.Editor;

namespace GitLab.Extension.CodeSuggestions.Model
{
    public interface SuggestionEvent {}

    public abstract class TextViewSuggestionEvent : SuggestionEvent
    {
        public IWpfTextView TextView { get; }
        
        protected TextViewSuggestionEvent(IWpfTextView textView)
        {
            TextView = textView;
        }
    }

    #region Events coming from the SuggestionServiceListener

    public abstract class SuggestionStateEvent : TextViewSuggestionEvent
    {
        public string UniqueTrackingId { get; }
        public int? OptionId { get; }
        public string StreamId { get; }
        
        public SuggestionStateEvent(
            IWpfTextView textView,
            string uniqueTrackingId,
            int? optionId,
            string streamId)
            : base(textView)
        {
            UniqueTrackingId = uniqueTrackingId;
            OptionId = optionId;
            StreamId = streamId;
        }
    }

    public class SuggestionStateShownEvent : SuggestionStateEvent
    {
        public SuggestionStateShownEvent(
            IWpfTextView textView,
            string uniqueTrackingId,
            int? optionId,
            string streamId)
            : base(textView, uniqueTrackingId, optionId, streamId) {}
    }

    public class SuggestionStateAcceptedEvent : SuggestionStateEvent
    {
        public SuggestionStateAcceptedEvent(
            IWpfTextView textView,
            string uniqueTrackingId,
            int? optionId,
            string streamId)
            : base(textView, uniqueTrackingId, optionId, streamId) {}
    }

    public class SuggestionStateRejectedEvent : SuggestionStateEvent
    {
        public SuggestionStateRejectedEvent(
            IWpfTextView textView,
            string uniqueTrackingId,
            int? optionId,
            string streamId)
            : base(textView, uniqueTrackingId, optionId, streamId) {}
    }

    public class SuggestionStateDismissedEvent : SuggestionStateEvent
    {
        public SuggestionStateDismissedEvent(
            IWpfTextView textView,
            string uniqueTrackingId,
            int? optionId,
            string streamId)
            : base(textView, uniqueTrackingId, optionId, streamId) {}
    }

    #endregion

    #region Events coming from the Language Server

    public class SuggestionUpdatedEvent : SuggestionEvent
    {
        public CompletionChunk Chunk { get; }
        public SuggestionUpdatedEvent(CompletionChunk completionChunk)
        {
            Chunk = completionChunk;
        }
    }

    #endregion

    #region Events coming from the TextViewListener

    public class TextViewClosedEvent : TextViewSuggestionEvent
    {
        public TextViewClosedEvent(IWpfTextView textView) : base(textView) {}
    }

    public class TextViewLostFocusEvent : TextViewSuggestionEvent
    {
        public TextViewLostFocusEvent(IWpfTextView textView) : base(textView) {}
    }

    public class TextViewLayoutChangedEvent : TextViewSuggestionEvent
    {
        public TextViewLayoutChangedEvent(IWpfTextView textView) : base(textView) {}
    }

    #endregion
    
    #region Events coming from the commands and actions

    public class SuggestionNextEvent : TextViewSuggestionEvent
    {
        public SuggestionNextEvent(IWpfTextView textView) : base(textView) {}
    }

    public class SuggestionPreviousEvent : TextViewSuggestionEvent
    {
        public SuggestionPreviousEvent(IWpfTextView textView) : base(textView) {}
    }
    
    #endregion
}
