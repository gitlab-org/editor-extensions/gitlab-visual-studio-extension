using System;

namespace GitLab.Extension.CodeSuggestions.Model
{ 
    public enum SuggestionState
    {
        None,
        Loading,
        Error,
        Shown,
        Complete,
    }
}