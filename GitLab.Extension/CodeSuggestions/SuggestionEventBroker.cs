using System;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using GitLab.Extension.CodeSuggestions.Model;

namespace GitLab.Extension.CodeSuggestions
{
    public interface ISuggestionEventBroker
    {
        IObservable<T> GetEvent<T>() where T : SuggestionEvent;
        void Publish<T>(T suggestionEvent) where T : SuggestionEvent;
    }
    public class SuggestionEventBroker : ISuggestionEventBroker
    {
        private readonly Subject<SuggestionEvent> _eventSubject = new Subject<SuggestionEvent>();

        public IObservable<T> GetEvent<T>() where T : SuggestionEvent
        {
            return _eventSubject.OfType<T>();
        }

        public void Publish<T>(T suggestionEvent) where T : SuggestionEvent
        {
            _eventSubject.OnNext(suggestionEvent);
        }
    }
}
