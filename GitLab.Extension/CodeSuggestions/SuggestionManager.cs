using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Reactive.Threading.Tasks;
using System.Threading;
using System.Threading.Tasks;
using GitLab.Extension.CodeSuggestions.Model;
using GitLab.Extension.LanguageServer.Models;
using GitLab.Extension.Status;
using GitLab.Extension.Status.Models;
using GitLab.Extension.Utility.Results;
using GitLab.Extension.Workspace;
using GitLab.Extension.Workspace.Model;
using Microsoft.VisualStudio.Shell;
using Microsoft.VisualStudio.Text.Editor;
using Serilog;

namespace GitLab.Extension.CodeSuggestions
{
    public interface ISuggestionManager
    {
        /// <summary>
        /// Allows to get first chunk or the whole suggestion to immediately display to user.
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns>First completion to display to user or null if no suggestion is available.</returns>
        Task<List<Completion>> GetFirstCompletionsAsync(CancellationToken cancellationToken);

        IWpfTextViewMargin GetMargin();
    }
    public class SuggestionManager : ISuggestionManager, IDisposable
    {
        private readonly TextDocumentInfo _textViewInfo;
        private readonly IWorkspaceLsClientProvider _lsClientProvider;
        private readonly IGitLabCodeSuggestionTelemetryController _telemetryController;
        private readonly ISuggestionEventBroker _eventBroker;
        private readonly IFeatureStateManager _featureStateManager;
        private readonly ILogger _logger;
        
        private readonly SuggestionProviderWrapper _suggestionProviderWrapper;
        private readonly List<IDisposable> _subscriptions = new List<IDisposable>();
        private readonly ISuggestionsAdornments _suggestionsAdornments;

        private readonly SuggestionDisplaySession _suggestionDisplaySession;

        public SuggestionManager(
            TextDocumentInfo textDocumentInfo,
            IWorkspaceLsClientProvider lsClientProvider,
            IGitLabCodeSuggestionTelemetryController telemetryController,
            ISuggestionEventBroker eventBroker,
            IFeatureStateManager featureStateManager,
            ISuggestionsAdornmentsFactory suggestionMarginFactory,
            ILogger logger)
        {
            _textViewInfo = textDocumentInfo;
            _lsClientProvider = lsClientProvider ?? throw new ArgumentNullException(nameof(lsClientProvider));
            _telemetryController = telemetryController ?? throw new ArgumentNullException(nameof(telemetryController));
            _eventBroker = eventBroker ?? throw new ArgumentNullException(nameof(eventBroker));
            _featureStateManager = featureStateManager ?? throw new ArgumentNullException(nameof(featureStateManager));
            _logger = logger.ForContext<SuggestionManager>();
            
            _suggestionProviderWrapper = new SuggestionProviderWrapper(_textViewInfo.TextView);
            _suggestionsAdornments = suggestionMarginFactory.Create(_textViewInfo.TextView, _eventBroker);
            _suggestionDisplaySession = new SuggestionDisplaySession();
            
            _subscriptions.AddRange(
                new[] {
                    _eventBroker.GetEvent<SuggestionUpdatedEvent>().Subscribe(async e => await HandleStreamingAsync(e)),
                    _eventBroker.GetEvent<TextViewLayoutChangedEvent>().Subscribe(async e => await HandleLayoutChangedAsync(e)),
                    _eventBroker.GetEvent<SuggestionStateRejectedEvent>().Subscribe(async e => await HandleSuggestionSessionEndAsync(e)),
                    _eventBroker.GetEvent<SuggestionStateDismissedEvent>().Subscribe(async e => await HandleSuggestionSessionEndAsync(e)),
                    _eventBroker.GetEvent<SuggestionStateAcceptedEvent>().Subscribe(async e => await HandleSuggestionSessionEndAsync(e)),
                    _eventBroker.GetEvent<SuggestionStateShownEvent>().Subscribe(async e => await HandleSuggestionShownEventAsync(e)),
                    _eventBroker.GetEvent<SuggestionNextEvent>().Subscribe(async e => await HandleNextSuggestion(e)),
                    _eventBroker.GetEvent<SuggestionPreviousEvent>().Subscribe(async e => await HandlePreviousSuggestion(e)),
                }
            );
        }

        public async Task<List<Completion>> GetFirstCompletionsAsync(CancellationToken cancellationToken)
        {
            if (!_featureStateManager.IsAvailable(Feature.CODE_SUGGESTIONS))
            {
                return new List<Completion>();
            }
            
            try
            {
                _suggestionDisplaySession.New();

                var ret = await GetCodeSuggestionAsync(CompletionTriggerKind.Automatic, cancellationToken);

                if (ret.IsError(out var error))
                {
                    _suggestionDisplaySession.Error();
                    _logger.Error("Error retrieving code suggestions for {FilePath}: {Error}",
                        _textViewInfo.RelativePath,
                        error.Message);
                    return new List<Completion>();
                }

                ret.IsSuccess(out List<Completion> completions);

                if (completions.Count == 0)
                {
                    return completions;
                }

                if (!completions.First().IsStreaming)
                {
                    _suggestionDisplaySession.Start(completions, true);
                    await ThreadHelper.JoinableTaskFactory.SwitchToMainThreadAsync();
                    _suggestionsAdornments.RenderAdornment(_suggestionDisplaySession);
                    return completions;
                }
                
                // additional suggestion loading is not supported for streams (yet)
                _suggestionDisplaySession.Start(new List<Completion> { completions.First() }, false);

                var streamUpdateEvent = await _eventBroker.GetEvent<SuggestionUpdatedEvent>()
                    .Where(e => e.Chunk.StreamId == completions.First().StreamId)
                    .FirstAsync()
                    .ToTask(cancellationToken);

                if (streamUpdateEvent.Chunk.IsLast)
                {
                    _suggestionDisplaySession.Complete();
                    return new List<Completion>();
                }
                
                // combine stream chunk and initial LSP response to provide first item to display to user
                return new List<Completion> { new Completion(completions.First().UniqueTrackingId, streamUpdateEvent.Chunk) };
            }
            catch (OperationCanceledException)
            {
                _suggestionDisplaySession.Complete();
            }
            catch (Exception ex)
            {
                _suggestionDisplaySession.Error();
                _logger.Error(ex,
                    "Error occurred while retrieving code suggestion for {FilePath}: {Error}",
                    _textViewInfo.RelativePath,
                    ex.Message);
            }
            return new List<Completion>();
        }
        
        private async Task<Result<List<Completion>>> GetCodeSuggestionAsync(CompletionTriggerKind triggerKind, CancellationToken cancellationToken)
        {
            var bufferPosition = _textViewInfo.TextView.Caret.Position.BufferPosition;
            
            _logger.Debug("Requesting code suggestion for {FilePath} at line {Line} and column {Column}.", 
                _textViewInfo.RelativePath, 
                bufferPosition.GetContainingLine().LineNumber, 
                bufferPosition.Position - bufferPosition.GetContainingLine().Start.Position);

            
            var line = bufferPosition.GetContainingLine();
            var lineNumber = line.LineNumber;
            var column = bufferPosition.Position - line.Start.Position;

            var lsClient = await _lsClientProvider.GetClientAsync(_textViewInfo.Workspace);

            var result = await lsClient.SendTextDocumentCompletionAsync(
                _textViewInfo.RelativePath,
                (uint)lineNumber,
                (uint)column,
                triggerKind,
                cancellationToken);

            if (result.IsError(out var error))
            {
                return Result.Error<List<Completion>>(error);
            }

            result.IsSuccess(out var list);

            if (list.Items.Count == 0)
            {
                _logger.Debug("No code suggestions available for {FilePath} at the requested position.",
                    _textViewInfo.RelativePath);
                return new List<Completion>();
            }
            
            _logger.Debug("Received code suggestion for {FilePath} at line {Line} and column {Column}.", 
                _textViewInfo.RelativePath, lineNumber, column);
            return list.Items.Select(ci => new Completion(ci)).ToList();
        }
        
        private async Task HandleStreamingAsync(SuggestionUpdatedEvent suggestionUpdatedEvent)
        {
            if (!_suggestionDisplaySession.ChunkLoaded(suggestionUpdatedEvent.Chunk))
            {
                return;
            }
            
            if ((_suggestionDisplaySession.State & SuggestionState.Complete) > 0)
            {
                // this session handled the streamed chunk, but it was already complete by the user
                await SendStreamCancelEventAsync(suggestionUpdatedEvent.Chunk.StreamId);
                _logger.Debug("Received code suggestion chunk from stream {StreamId}, but the corresponding session is already complete.", suggestionUpdatedEvent.Chunk.StreamId);
                return;
            }

            await ThreadHelper.JoinableTaskFactory.SwitchToMainThreadAsync();

            await RenderSuggestionAsync();
        }

        private async Task HandleLayoutChangedAsync(TextViewLayoutChangedEvent textViewLayoutChangedEvent)
        {
            try
            {
                if (textViewLayoutChangedEvent.TextView != _textViewInfo.TextView)
                {
                    return;
                }
                
                if (_suggestionDisplaySession.State == SuggestionState.Complete || _suggestionDisplaySession.State == SuggestionState.None)
                {
                    return;
                }

                await ThreadHelper.JoinableTaskFactory.SwitchToMainThreadAsync();

                _suggestionsAdornments.RenderAdornment(_suggestionDisplaySession);
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Error while handling event {EventType}.", typeof(TextViewLayoutChangedEvent));
            }
        }
        
        private async Task HandleSuggestionSessionEndAsync(SuggestionStateEvent stateEvent)
        {
            try
            {
                if (stateEvent.TextView != _textViewInfo.TextView)
                {
                    return;
                }

                var rejectedIds = new List<string>();

                if (stateEvent is SuggestionStateAcceptedEvent)
                {
                    Task.Run(async () =>
                    {
                        await _telemetryController.OnAcceptedAsync(
                            _suggestionDisplaySession.CurrentCompletion.UniqueTrackingId, _textViewInfo.Workspace);
                    });
                    
                    rejectedIds = _suggestionDisplaySession.GetShownCompletions()
                        .Where(c => c.UniqueTrackingId != _suggestionDisplaySession.CurrentCompletion.UniqueTrackingId)
                        .Select(c => c.UniqueTrackingId)
                        .Distinct()
                        .ToList();
                }
                else if (stateEvent is SuggestionStateDismissedEvent || stateEvent is SuggestionStateRejectedEvent)
                {
                    rejectedIds = _suggestionDisplaySession.GetShownCompletions().Select(c => c.UniqueTrackingId).Distinct().ToList();
                }
                
                var notProvidedIds = _suggestionDisplaySession.GetNotShownCompletions().Select(c => c.UniqueTrackingId).Distinct().Except(rejectedIds).ToList();
            
                Task.Run(async () =>
                {
                    await Task.WhenAll(rejectedIds.Select(id => _telemetryController.OnRejectedAsync(id, _textViewInfo.Workspace)).ToArray());
                    await Task.WhenAll(notProvidedIds.Select(id => _telemetryController.OnNotProvidedAsync(id, _textViewInfo.Workspace)).ToArray());
                });
                _suggestionDisplaySession.Complete();
            
                await ThreadHelper.JoinableTaskFactory.SwitchToMainThreadAsync();
                
                _suggestionsAdornments.RenderAdornment(_suggestionDisplaySession);
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Error while handling event {EventType}.", stateEvent.GetType());
            }
        }
        
        private async Task HandleSuggestionShownEventAsync(SuggestionStateShownEvent shownEvent)
        {
            try
            {
                if (shownEvent.TextView != _textViewInfo.TextView)
                {
                    return;
                }
                
                if (_suggestionDisplaySession.State == SuggestionState.Complete)
                {
                    return;
                }

                if (!_suggestionDisplaySession.Streaming)
                {
                    _suggestionDisplaySession.Shown();
                }
            
                if (!_suggestionDisplaySession.LoadAdditional)
                {
                    return;
                }
            
                Task.Run(async () =>
                {
                    await _telemetryController.OnShownAsync(_suggestionDisplaySession.CurrentCompletion.UniqueTrackingId, _textViewInfo.Workspace);
                });
            
                RequestAdditionalSuggestions(_suggestionDisplaySession.CancellationToken);
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Error while handling event {EventType}.", typeof(SuggestionStateShownEvent));
            }
        }

        private async Task HandleNextSuggestion(SuggestionNextEvent suggestionPreviousEvent)
        {
            try
            {
                if (suggestionPreviousEvent.TextView != null && suggestionPreviousEvent.TextView != _textViewInfo.TextView)
                {
                    return;
                }
                
                var shownIds = _suggestionDisplaySession.GetShownCompletions().Select(c => c.UniqueTrackingId).Distinct().ToList();
                
                _suggestionDisplaySession.Next();
            
                var shownIdsNew = _suggestionDisplaySession.GetShownCompletions().Select(c => c.UniqueTrackingId).Except(shownIds).ToList();
            
                if (shownIdsNew.Count > 0)
                {
                    Task.Run(async () =>
                    {
                        await Task.WhenAll(shownIdsNew.Select(id => _telemetryController.OnShownAsync(id, _textViewInfo.Workspace)).ToArray());
                    });
                }
                
                await ThreadHelper.JoinableTaskFactory.SwitchToMainThreadAsync();
                await RenderSuggestionAsync();
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Error while handling event {EventType}.", typeof(SuggestionNextEvent));
            }
        }
        
        private async Task HandlePreviousSuggestion(SuggestionPreviousEvent suggestionPreviousEvent)
        {
            try
            {
                if (suggestionPreviousEvent.TextView != null && suggestionPreviousEvent.TextView != _textViewInfo.TextView)
                {
                    return;
                }
                
                var shownIds = _suggestionDisplaySession.GetShownCompletions().Select(c => c.UniqueTrackingId).Distinct().ToList();
                
                _suggestionDisplaySession.Previous();
                
                var shownIdsNew = _suggestionDisplaySession.GetShownCompletions().Select(c => c.UniqueTrackingId).Except(shownIds).ToList();

                if (shownIdsNew.Count > 0)
                {
                    Task.Run(async () =>
                    {
                        await Task.WhenAll(shownIdsNew.Select(id => _telemetryController.OnShownAsync(id, _textViewInfo.Workspace)).ToArray());
                    });
                }
                
                await RenderSuggestionAsync();
                await ThreadHelper.JoinableTaskFactory.SwitchToMainThreadAsync();
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Error while handling event {EventType}.", typeof(SuggestionPreviousEvent));
            }
        }

        private async Task RenderSuggestionAsync()
        {
            if (_suggestionDisplaySession.State == SuggestionState.Complete || _suggestionDisplaySession.State == SuggestionState.None)
            {
                return;
            }
            
            var currentCompletion = _suggestionDisplaySession.CurrentCompletion;
            
            var session = _suggestionProviderWrapper.Session;
            
            if (session == null)
            {
                _logger.Debug("No active session to update for {FilePath}.", _textViewInfo.RelativePath);
                return;
            }
            
            var proposal = GitlabProposalCreator.CreateProposal(
                currentCompletion,
                _textViewInfo.TextView,
                null);
            
            await session.DisplayProposalAsync(proposal, CancellationToken.None);
            _suggestionsAdornments.RenderAdornment(_suggestionDisplaySession);
        }
        
        private async Task SendStreamCancelEventAsync(string streamId)
        {
            _logger.Debug("Sending cancel stream event for {StreamId}.", streamId);
            
            var lsClient = await _lsClientProvider.GetClientAsync(_textViewInfo.Workspace);
            await lsClient.SendCancelStream(streamId);
        }

        private void RequestAdditionalSuggestions(CancellationToken cancellationToken)
        {
            Task.Run(async () =>
            {
                var proposals = await GetCodeSuggestionAsync(CompletionTriggerKind.Invoked, cancellationToken);
            
                if (proposals.IsError(out var error))
                {
                    _logger.Error("Error requesting more code suggestions: {Message}", error.Message);
                    return;
                }
            
                if (!proposals.IsSuccess(out var list))
                {
                    return;
                }

                await ThreadHelper.JoinableTaskFactory.SwitchToMainThreadAsync(cancellationToken);
                if (_suggestionDisplaySession.AdditionalLoaded(list))
                {
                    _suggestionsAdornments.RenderAdornment(_suggestionDisplaySession);
                }
            });
        }

        public void Dispose()
        {
            foreach (var subscription in _subscriptions)
            {
                subscription.Dispose();
            }

            _subscriptions.Clear();
            Task.Run(async () =>
            {
                await Task.WhenAll(_suggestionDisplaySession.Completions
                    .Where(c => c.IsStreaming)
                    .Select(c => SendStreamCancelEventAsync(c.StreamId))
                    .ToArray());
            });
            
            _suggestionsAdornments.Dispose();
            _suggestionDisplaySession.Dispose();

            _logger.Debug("SuggestionManager disposed.");
        }

        public IWpfTextViewMargin GetMargin()
        {
            return _suggestionsAdornments.GetMargin();
        }
    }
}
