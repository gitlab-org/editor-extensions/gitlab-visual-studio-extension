using System;
using System.Collections.Concurrent;
using GitLab.Extension.CodeSuggestions.Model;
using GitLab.Extension.Status;
using GitLab.Extension.Workspace;
using GitLab.Extension.Workspace.Model;
using Microsoft.VisualStudio.Shell;
using Microsoft.VisualStudio.Text.Editor;
using Serilog;

namespace GitLab.Extension.CodeSuggestions
{
    public interface ISuggestionManagerProvider
    {
        ISuggestionManager GetSuggestionManager(IWpfTextView textView);
    }

    public class SuggestionManagerProvider : ISuggestionManagerProvider, IDisposable
    {
        private readonly IWorkspaceLsClientProvider _lsClientProvider;
        private readonly ISuggestionEventBroker _eventBroker;
        private readonly IGitLabCodeSuggestionTelemetryController _telemetryController;
        private readonly IFeatureStateManager _featureStateManager;
        private readonly ISuggestionsAdornmentsFactory _suggestionsAdornmentsFactory;
        private readonly ILogger _logger;

        private readonly IDisposable _subscription;

        private readonly ConcurrentDictionary<IWpfTextView, ISuggestionManager> _textViewToManagerDictionary =
            new ConcurrentDictionary<IWpfTextView, ISuggestionManager>();

        public SuggestionManagerProvider(
            IWorkspaceLsClientProvider lsClientProvider,
            ISuggestionEventBroker eventBroker,
            IGitLabCodeSuggestionTelemetryController telemetryController,
            IFeatureStateManager featureStateManager,
            ISuggestionsAdornmentsFactory suggestionsAdornmentsFactory,
            ILogger logger)
        {
            _lsClientProvider = lsClientProvider;
            _eventBroker = eventBroker;
            _telemetryController = telemetryController;
            _featureStateManager = featureStateManager;
            _suggestionsAdornmentsFactory = suggestionsAdornmentsFactory;
            _logger = logger;

            _subscription = _eventBroker.GetEvent<TextViewClosedEvent>().Subscribe(e => OnTextViewClosed(e.TextView));
        }

        public ISuggestionManager GetSuggestionManager(IWpfTextView textView)
        {
            ThreadHelper.ThrowIfNotOnUIThread();
            
            return _textViewToManagerDictionary.GetOrAdd(textView, tv =>
            {
                var textDocumentInfo = tv.GetTextViewInfo(WorkspaceUtilities.GetCurrentWorkspaceId());
                var manager = new SuggestionManager(
                    textDocumentInfo,
                    _lsClientProvider,
                    _telemetryController,
                    _eventBroker,
                    _featureStateManager,
                    _suggestionsAdornmentsFactory,
                    _logger);
                _logger.Debug("Created new SuggestionManager for TextView.");
                return manager;
            });
        }

        private void OnTextViewClosed(IWpfTextView textView)
        {
            if (_textViewToManagerDictionary.TryRemove(textView, out var manager))
            {
                if (manager is IDisposable disposableManager)
                {
                    disposableManager.Dispose();
                }

                _logger.Debug("Removed and disposed SuggestionManager for closed TextView.");
            }
            else
            {
                _logger.Warning(
                    "Failed to remove SuggestionManager for closed TextView. It may not have been registered.");
            }
        }

        public void Dispose()
        {
            foreach (var kvp in _textViewToManagerDictionary)
            {
                if (kvp.Value is IDisposable disposableManager)
                {
                    disposableManager.Dispose();
                }
            }

            _textViewToManagerDictionary.Clear();
            _subscription.Dispose();
            _logger.Debug("Disposed SuggestionManagerProvider and cleared all SuggestionManagers.");
        }
    }
}
