using System;
using System.Reflection;
using Microsoft.VisualStudio.Language.Suggestions;
using Microsoft.VisualStudio.Text.Editor;
using Serilog;

namespace GitLab.Extension.CodeSuggestions
{
    public class SuggestionProviderWrapper
    {
        private readonly SuggestionProviderBase _provider;

        private readonly FieldInfo _sessionField;

        public SuggestionProviderWrapper(ITextView view)
        {
            try
            {
                var assembly = Assembly.Load("Microsoft.VisualStudio.IntelliCode");
                var providerType = assembly.GetType(
                    "Microsoft.VisualStudio.IntelliCode.SuggestionService.InlineCompletion.InlineCompletionsInstance",
                    false,
                    true);
                view.Properties.TryGetProperty(providerType, out _provider);
                _sessionField = providerType.GetField("Session", BindingFlags.Instance | BindingFlags.NonPublic);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Failed to initialize suggestion provider wrapper. Is IntelliCode missing?");
            }
        }

        public SuggestionSessionBase Session => _sessionField?.GetValue(_provider) as SuggestionSessionBase;
    }
}