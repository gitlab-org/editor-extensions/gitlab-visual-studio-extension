using System;
using GitLab.Extension.CodeSuggestions.Model;
using Microsoft.VisualStudio.Language.Suggestions;
using Microsoft.VisualStudio.Text.Editor;
using Serilog;

namespace GitLab.Extension.CodeSuggestions
{
    public class SuggestionServiceListener : IDisposable
    {
        private SuggestionServiceBase _suggestionServiceBase;
        private readonly ISuggestionEventBroker _eventBroker;
        private readonly ILogger _logger;
        
        public SuggestionServiceListener(ISuggestionEventBroker eventBroker, ILogger logger)
        {
            _eventBroker = eventBroker ?? throw new ArgumentNullException(nameof(eventBroker));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public void Initialize(SuggestionServiceBase suggestionServiceBase)
        {
            _suggestionServiceBase = suggestionServiceBase ??
                                     throw new ArgumentNullException(nameof(suggestionServiceBase));
            _suggestionServiceBase.ProposalDisplayed += HandleProposalDisplayed;
            _suggestionServiceBase.ProposalRejected += HandleProposalRejected;
            _suggestionServiceBase.SuggestionAccepted += HandleSuggestionAccepted;
            _suggestionServiceBase.SuggestionDismissed += HandleSuggestionDismissed;

            _logger.Information("Initialized suggestion service telemetry adapter.");
        }
        
        private void HandleProposalDisplayed(
            object sender,
            ProposalDisplayedEventArgs args)
        {
            if (!(args.OriginalProposal is GitLabProposal gitLabProposal))
            {
                return;
            }
            _eventBroker.Publish(new SuggestionStateShownEvent(
                args.View as IWpfTextView,
                gitLabProposal.GitLabProposalMetadata.UniqueRequestId,
                gitLabProposal.GitLabProposalMetadata.OptionId,
                gitLabProposal.GitLabProposalMetadata.StreamId));
        }

        private void HandleProposalRejected(
            object sender,
            ProposalRejectedEventArgs args)
        {
            if (!(args.OriginalProposal is GitLabProposal))
            {
                return;
            }
            _eventBroker.Publish(new SuggestionStateRejectedEvent(args.View as IWpfTextView, null, null, null));
        }

        private void HandleSuggestionAccepted(
            object sender,
            SuggestionAcceptedEventArgs args)
        {
            if (!(args.OriginalProposal is GitLabProposal))
            {
                return;
            }
            _eventBroker.Publish(new SuggestionStateAcceptedEvent(args.View as IWpfTextView, null, null, null));
        }

        private void HandleSuggestionDismissed(object sender, SuggestionDismissedEventArgs args)
        {
            if (args.OriginalProposal != null && !(args.OriginalProposal is GitLabProposal))
            {
                return;
            }
            _eventBroker.Publish(new SuggestionStateDismissedEvent(args.View as IWpfTextView, null, null, null));
        }

        public void Dispose()
        {
            if (_suggestionServiceBase == null)
            {
                return;
            }

            _suggestionServiceBase.ProposalDisplayed -= HandleProposalDisplayed;
            _suggestionServiceBase.ProposalRejected -= HandleProposalRejected;
            _suggestionServiceBase.SuggestionAccepted -= HandleSuggestionAccepted;
            _suggestionServiceBase.SuggestionDismissed -= HandleSuggestionDismissed;
        }
    }
}
