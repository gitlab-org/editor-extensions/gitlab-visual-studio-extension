using System.Windows;
using System.Windows.Controls;
using GitLab.Extension.CodeSuggestions.Model;
using Microsoft.VisualStudio.PlatformUI;
using Microsoft.VisualStudio.Text.Editor;

namespace GitLab.Extension.CodeSuggestions.UI
{
    public class DuoSuggestionMargin : Canvas, IWpfTextViewMargin
    {
        public const string MarginName = "DuoSuggestionMargin";
        public const double MarginSizeConst = 16;
        
        private readonly MarginImageControl _imageControl;
        
        public DuoSuggestionMargin()
        {
            _imageControl = new MarginImageControl();
        }

        public void Clear()
        {
            Children.Clear();
        }

        public void Update(SuggestionState state, double top)
        {
            SetLeft(_imageControl, 0);
            SetTop(_imageControl, top);
            
            _imageControl.SuggestionState = state;
            _imageControl.IsDarkMode = IsDarkMode();
            
            Children.Clear();
            Children.Add(_imageControl);
        }

        public void Dispose()
        {
            
        }

        public ITextViewMargin GetTextViewMargin(string marginName)
        {
            return this;
        }

        public double MarginSize => MarginSizeConst;
        public bool Enabled => true;
        public FrameworkElement VisualElement => this;

        private bool IsDarkMode()
        {
            return VSColorTheme.GetThemedColor(EnvironmentColors.SideBarBackgroundColorKey).GetBrightness() < 0.5;
        }
    }
}