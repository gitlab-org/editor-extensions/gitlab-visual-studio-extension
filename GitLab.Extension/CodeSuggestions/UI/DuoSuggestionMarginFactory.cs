using Microsoft.VisualStudio.Text.Editor;

namespace GitLab.Extension.CodeSuggestions.UI
{
    public class SuggestionsAdornmentsFactory : ISuggestionsAdornmentsFactory
    {
        public ISuggestionsAdornments Create(IWpfTextView textView, ISuggestionEventBroker suggestionEventBroker)
        {
            return new SuggestionsAdornments(textView, suggestionEventBroker);
        }
    }
}