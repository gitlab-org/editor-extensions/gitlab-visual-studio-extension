using System.ComponentModel;
using System.Runtime.CompilerServices;
using GitLab.Extension.CodeSuggestions.Model;

namespace GitLab.Extension.CodeSuggestions.UI
{
    public partial class MarginImageControl : INotifyPropertyChanged
    {
        private SuggestionState _suggestionState = SuggestionState.None;
        private bool _isDarkMode;

        public event PropertyChangedEventHandler PropertyChanged;

        public SuggestionState SuggestionState
        {
            get => _suggestionState;
            set
            {
                if (_suggestionState != value)
                {
                    _suggestionState = value;
                    OnPropertyChanged();
                }
            }
        }

        public bool IsDarkMode
        {
            get => _isDarkMode;
            set
            {
                if (_isDarkMode != value)
                {
                    _isDarkMode = value;
                    OnPropertyChanged();
                }
            }
        }

        public MarginImageControl()
        {
            InitializeComponent();
        }

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}