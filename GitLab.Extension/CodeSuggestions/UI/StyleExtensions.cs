using System;
using Microsoft.VisualStudio.PlatformUI;
using Microsoft.VisualStudio.Shell;

namespace GitLab.Extension.CodeSuggestions.UI
{
    public static class StyleExtensions
    {
        public static System.Windows.Media.Color AsColor(this ThemeResourceKey themeResourceKey)
        {
            try
            {
                var vsColor = VSColorTheme.GetThemedColor(themeResourceKey);

                return System.Windows.Media.Color.FromArgb(
                    vsColor.A,
                    vsColor.R,
                    vsColor.G,
                    vsColor.B);
            }
            catch (Exception ex)
            {
                // ignored
            }
            return System.Windows.Media.Colors.White;
        }
    }
}
