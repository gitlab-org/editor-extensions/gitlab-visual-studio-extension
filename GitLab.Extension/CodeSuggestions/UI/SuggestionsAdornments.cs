using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using GitLab.Extension.CodeSuggestions.Model;
using Microsoft.VisualStudio.PlatformUI;
using Microsoft.VisualStudio.Text.Editor;
using Serilog;

namespace GitLab.Extension.CodeSuggestions.UI
{
    public class SuggestionsAdornments : ISuggestionsAdornments
    {
        private const int BORDER_THICKNESS = 2;
        private const int PADDING = 5;
        
        private readonly IWpfTextView _textView;
        private readonly IAdornmentLayer _adornmentLayer;
        private readonly ISuggestionEventBroker _suggestionEventBroker;

        private readonly Button _previousButton;
        private readonly Button _nextButton;
        
        private readonly DuoSuggestionMargin _suggestionsMargin;
        
        public SuggestionsAdornments(
            IWpfTextView textView,
            ISuggestionEventBroker suggestionEventBroker)
        {
            _textView = textView;
            _adornmentLayer = _textView.GetAdornmentLayer(Constants.SuggestionsAdornmentLayerName);
            
            _previousButton = PreviousButton();
            _nextButton = NextButton();
            _suggestionEventBroker = suggestionEventBroker;
            _suggestionsMargin = new DuoSuggestionMargin();
        }
        
        public IWpfTextViewMargin GetMargin() => _suggestionsMargin;
        
        public void RenderAdornment(SuggestionDisplaySession suggestionDisplaySession)
        {
            try
            {
                _adornmentLayer.RemoveAllAdornments();

                if (_textView.Caret.ContainingTextViewLine.Top < _textView.ViewportTop ||
                    _textView.Caret.ContainingTextViewLine.Top > _textView.ViewportBottom)
                {
                    // caret is outside the current viewport, render nothing
                    _suggestionsMargin.Clear();
                    return;
                }

                _suggestionsMargin.Update(suggestionDisplaySession.State,
                    _textView.Caret.ContainingTextViewLine.Top - _textView.ViewportTop);

                var caretPosition = _textView.Caret.Position.BufferPosition;
                var line = _textView.GetTextViewLineContainingBufferPosition(caretPosition);
                var caretBounds = line.GetCharacterBounds(caretPosition);

                if (suggestionDisplaySession.State == SuggestionState.None ||
                    suggestionDisplaySession.State == SuggestionState.Complete)
                {
                    return;
                }

                if (suggestionDisplaySession.Completions.All(c => string.IsNullOrWhiteSpace(c.InsertText)))
                {
                    return;
                }

                var fontSize = _textView.FormattedLineSource.DefaultTextProperties.FontRenderingEmSize;

                var stackPanel = new StackPanel
                {
                    Orientation = Orientation.Horizontal,
                };

                stackPanel.Children.Add(GetTitle(fontSize));

                if (suggestionDisplaySession.Completions.Count > 1 || suggestionDisplaySession.LoadAdditional)
                {
                    try
                    {
                        (_previousButton.Parent as Panel)?.Children.Clear();
                        (_nextButton.Parent as Panel)?.Children.Clear();
                    }
                    catch (Exception)
                    {
                        // ignored
                    }

                    stackPanel.Children.Add(_previousButton);
                    stackPanel.Children.Add(GetCyclingHint(
                        fontSize,
                        suggestionDisplaySession.CurrentIndex + 1,
                        suggestionDisplaySession.LoadAdditional
                            ? (int?)null
                            : suggestionDisplaySession.Completions.Count));
                    stackPanel.Children.Add(_nextButton);
                }

                var container = new Border
                {
                    Background = new SolidColorBrush(EnvironmentColors.ToolWindowBackgroundColorKey.AsColor()),
                    BorderBrush = new SolidColorBrush(EnvironmentColors.ToolWindowBorderColorKey.AsColor()),
                    BorderThickness = new Thickness(BORDER_THICKNESS),
                    Child = stackPanel
                };

                Canvas.SetLeft(container, caretBounds.Left);
                Canvas.SetTop(container,
                    caretBounds.Top - container.ActualHeight - fontSize - (BORDER_THICKNESS + PADDING) * 2);

                _adornmentLayer.AddAdornment(
                    AdornmentPositioningBehavior.ViewportRelative,
                    null,
                    null,
                    container,
                    null);
            }
            catch (Exception ex)
            {
                // visuals are not critical for code suggestions, log the error and keep on going
                Log.Error(ex, "Error rendering suggestions adornment");
            }
        }

        private TextBlock GetTitle(double fontSize)
        {
            return new TextBlock
            {
                Text = "GitLab Duo Code Suggestions",
                TextWrapping = TextWrapping.Wrap,
                Foreground = new SolidColorBrush(EnvironmentColors.SystemGrayTextColorKey.AsColor()),
                FontFamily = new FontFamily("Consolas"),
                Padding = new Thickness(PADDING),
                FontSize = fontSize
            };
        }

        private TextBlock GetCyclingHint(double fontSize, int currentIndex, int? totalCount)
        {
            var totalCountText = totalCount == null ? "?" : totalCount.ToString();
            return new TextBlock
            {
                Text = $"{currentIndex}/{totalCountText}",
                TextWrapping = TextWrapping.Wrap,
                Foreground = new SolidColorBrush(EnvironmentColors.SystemGrayTextColorKey.AsColor()),
                FontFamily = new FontFamily("Consolas"),
                Padding = new Thickness(PADDING),
                FontSize = fontSize
            };
        }

        private Button NextButton()
        {
            var button = new Button
            {
                Content = ">",
                Background = Brushes.Transparent,
                BorderThickness = new Thickness(0),
                Padding = new Thickness(PADDING),
                Foreground = new SolidColorBrush(EnvironmentColors.SystemGrayTextColorKey.AsColor()),
            };
            button.Click += OnNextClicked;
            return button;
        }

        private Button PreviousButton()
        {
            var button = new Button
            {
                Content = "<",
                Background = Brushes.Transparent,
                BorderThickness = new Thickness(0),
                Padding = new Thickness(PADDING),
                Foreground = new SolidColorBrush(EnvironmentColors.SystemGrayTextColorKey.AsColor()),
            };
            button.Click += OnPreviousClicked;
            return button;
        }
        
        private void OnNextClicked(object sender, RoutedEventArgs e)
        {
            _suggestionEventBroker.Publish(new SuggestionNextEvent(_textView));
        }
        
        private void OnPreviousClicked(object sender, RoutedEventArgs e)
        {
            _suggestionEventBroker.Publish(new SuggestionPreviousEvent(_textView));
        }

        public void Dispose()
        {
            _suggestionsMargin.Dispose();
            _adornmentLayer.RemoveAllAdornments();
            _previousButton.Click -= OnPreviousClicked;
            _nextButton.Click -= OnNextClicked;
        }
    }
}
