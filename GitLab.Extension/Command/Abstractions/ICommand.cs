﻿using System;
using System.ComponentModel.Design;
using System.Threading.Tasks;

namespace GitLab.Extension.Command
{
    public interface ICommand
    {
        CommandID CommandID { get; }
        Task InitializeAsync(
            IServiceProvider serviceProvider);
    }
}
