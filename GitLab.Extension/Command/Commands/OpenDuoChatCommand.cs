﻿using System;
using System.ComponentModel.Design;
using System.Threading.Tasks;
using GitLab.Extension.Webviews;
using GitLab.Extension.Webviews.DuoChat;
using Microsoft.VisualStudio.Shell;

namespace GitLab.Extension.Command
{
    public class OpenDuoChatCommand : ICommand
    {
        public CommandID CommandID =>
            new CommandID(
                PackageIds.GitLabPackageCmdSet, 
                PackageIds.CmdOpenDuoChat);
        
        private readonly IWebviewController<DuoChatToolWindow> _duoChatController;
        
        public OpenDuoChatCommand(IWebviewController<DuoChatToolWindow> duoChatController)
        {
            _duoChatController = duoChatController;
        }

        public async Task InitializeAsync(
            IServiceProvider serviceProvider)
        {
            await ThreadHelper.JoinableTaskFactory.SwitchToMainThreadAsync();

            var oleMenuItem = 
                new OleMenuCommand(
                    OpenDuoChatWindow, 
                    CommandID);
            oleMenuItem.BeforeQueryStatus += BeforeQueryStatus;

            var commandService = 
                serviceProvider.GetService(typeof(IMenuCommandService)) as IMenuCommandService;
            
            commandService?.AddCommand(oleMenuItem);
        }

        private void OpenDuoChatWindow(
            object obj,
            EventArgs args)
        {
            _duoChatController.ShowAsync();
        }

        private void BeforeQueryStatus(
            object obj,
            EventArgs args)
        {
            var command = obj as OleMenuCommand;
            
            if (command == null)
            {
                return;
            }
            
            command.Visible = true;
            command.Enabled = _duoChatController.IsEnabled;
        }
    }
}
