﻿using System;
using System.ComponentModel.Design;
using System.Threading.Tasks;
using GitLab.Extension.SettingsUtil;
using Microsoft.VisualStudio.Shell;

namespace GitLab.Extension.Command
{
    public class ToggleCodeSuggestionsCommand : ICommand
    {
        public CommandID CommandID =>
            new CommandID(
                PackageIds.GitLabPackageCmdSet, 
                PackageIds.CmdToggleCodeSuggestions);

        private readonly ISettings _settings;

        public ToggleCodeSuggestionsCommand(
            ISettings settings)
        {
            _settings = settings;
        }

        public async Task InitializeAsync(
            IServiceProvider serviceProvider)
        {
            await ThreadHelper.JoinableTaskFactory.SwitchToMainThreadAsync();

            var oleMenuItem = 
                new OleMenuCommand(
                    ToggleCodeSuggestions, 
                    CommandID);

            var commandService = 
                serviceProvider.GetService(typeof(IMenuCommandService)) as IMenuCommandService;
            
            commandService?.AddCommand(oleMenuItem);
        }

        private void ToggleCodeSuggestions(
            object obj,
            EventArgs args)
        {
            _settings.IsCodeSuggestionsEnabled = !_settings.IsCodeSuggestionsEnabled;
        }
    }
}
