﻿using System;
using Autofac;
using GitLab.Extension.CodeSuggestions.Commands;
using GitLab.Extension.Webviews.DuoChat.Commands;

namespace GitLab.Extension.Command
{
    public static class DependencyInjection
    {
        public static Type[] CommandTypes =
        {
            typeof(ToggleCodeSuggestionsCommand),
            typeof(OpenDuoChatCommand),
            typeof(ExplainSelectedCodeCommand),
            typeof(FixSelectedCodeCommand),
            typeof(RefactorSelectedCodeCommand),
            typeof(TestSelectedCodeCommand),
            typeof(NextSuggestionCommand),
            typeof(PreviousSuggestionCommand)
        };

        public static ContainerBuilder RegisterCommands(
            this ContainerBuilder builder)
        {
            // Register Initializer
            builder.RegisterType<CommandInitializer>()
                .As<ICommandInitializer>();

            // Register Commands
            foreach (var type in CommandTypes)
            {
                builder.RegisterType(type)
                    .As<ICommand>();
            }

            return builder;
        }
    }
}
