﻿using System;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using AutofacSerilogIntegration;
using GitLab.Extension.CodeSuggestions;
using GitLab.Extension.CodeSuggestions.UI;
using GitLab.Extension.Command;
using GitLab.Extension.GitLabApi.DependencyInjection;
using GitLab.Extension.InfoBar;
using GitLab.Extension.LanguageServer;
using GitLab.Extension.LanguageServer.Plugin.DependencyInjection;
using GitLab.Extension.SettingsUtil;
using GitLab.Extension.SettingsUtil.Observable;
using GitLab.Extension.Status;
using GitLab.Extension.Status.Interfaces;
using GitLab.Extension.Utility.Git.DependencyInjection;
using GitLab.Extension.Workspace;
using GitLab.Extension.Workspace.DependencyInjection;
using Microsoft.Extensions.DependencyInjection;

namespace GitLab.Extension
{
    public class DependencyInjection
    {
        private static DependencyInjection _instance;
        private static ISettings _settings;

        private DependencyInjection()
        {
            var container = RegisterComponents();
            Scope = container.BeginLifetimeScope();

            _settings = Scope.Resolve<ISettings>();
        }

        public static DependencyInjection Instance => _instance ?? (_instance = new DependencyInjection());

        public ILifetimeScope Scope { get; }

        private IContainer RegisterComponents()
        {
            var builder = new ContainerBuilder();

            builder.Populate(
                new ServiceCollection().AddGitLabHttpClient());

            builder.AddGitClient();

            // Commands
            builder.RegisterCommands();

            // CodeSuggestions

            builder.RegisterType<GitlabProposalSource>();
            builder.RegisterType<GitlabProposalManager>();
            builder
                .RegisterType<LanguageManager>()
                .As<ILanguageManager>()
                .SingleInstance();
            
            builder
                .RegisterType<SuggestionServiceListener>()
                .As<SuggestionServiceListener>()
                .SingleInstance()
                .AutoActivate();
            
            builder
                .RegisterType<SuggestionEventBroker>()
                .As<ISuggestionEventBroker>()
                .SingleInstance();
            
            builder.RegisterType<SuggestionManagerProvider>()
                .As<ISuggestionManagerProvider>()
                .SingleInstance();
            
            builder.RegisterType<SuggestionsAdornmentsFactory>()
                .As<ISuggestionsAdornmentsFactory>();

            // CodeSuggestion - Telemetry

            builder
                .RegisterType<GitLabCodeSuggestionTelemetryController>()
                .As<IGitLabCodeSuggestionTelemetryController>();

            // Workspace
            builder.RegisterModule<WorkspaceModule>();
            
            builder
                .RegisterType<LsClientProvider>()
                .As<IWorkspaceLsClientProvider>()
                .SingleInstance();
            
            builder
                .RegisterType<ThemeManager>()
                .As<IThemeManager>()
                .SingleInstance();

            // LanguageServer

            builder
                .RegisterType<LsClientManager>()
                .As<ILsClientManager>()
                .SingleInstance();
            builder
                .RegisterType<LsClientTs>()
                .As<ILsClient>();
            builder
                .RegisterType<LsProcessManagerTs>()
                .As<ILsProcessManager>()
                .SingleInstance();
            builder.RegisterType<LsClientRpc>();
            
            // LanguageServer - Plugin

            builder.RegisterModule(new LsPluginCommunicationModule());
            
            // LanguageServer - FeatureStates
            
            builder.RegisterType<FeatureStateManager>()
                .As<IFeatureStateManager>()
                .SingleInstance();

            // Settings

            builder
                .RegisterType<Settings>()
                .As<ISettings>()
                .SingleInstance();
            builder
                .RegisterType<RegistryStorage>()
                .As<ISettingsStorage>()
                .SingleInstance();
            builder
                .RegisterType<ProtectImpl>()
                .As<ISettingsProtect>()
                .SingleInstance();
            builder
                .RegisterType<SettingsObservable>()
                .As<IObservable<ISettings>>()
                .SingleInstance();

            // Status

            builder
                .RegisterType<StatusBar>()
                .SingleInstance();
            builder
                .RegisterType<NotificationService>()
                .As<INotificationService>()
                .SingleInstance();
            builder.RegisterType<StatusBarControlFactory>()
                .As<IStatusControlFactory>();

            // Logging

            builder.RegisterLogger();

            // InfoBar
            builder
                .RegisterType<InfoBarFactory>()
                .As<IInfoBarFactory>()
                .SingleInstance();

            return builder.Build();
        }
    }
}