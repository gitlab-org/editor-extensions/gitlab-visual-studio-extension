﻿using System.Net;
using GitLab.Extension.Utility.Results;
using GitLab.Extension.Utility.Results.Errors;

namespace GitLab.Extension.GitLabApi.Errors
{
    public class GitLabApiError : Error
    {
        public GitLabApiError(
            HttpStatusCode? statusCode = null,
            string message = null) : base(message)
        {
            StatusCode = statusCode;
        }

        public HttpStatusCode? StatusCode { get; }
    }

    public class UnauthorizedError : GitLabApiError
    {
        public UnauthorizedError()
            : base(HttpStatusCode.Unauthorized, "Unauthorized")
        {
        }
    }
}