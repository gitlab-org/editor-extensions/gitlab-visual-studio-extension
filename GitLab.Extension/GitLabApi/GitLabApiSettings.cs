﻿using System;
using System.Collections.Generic;
using GitLab.Extension.GitLabApi.Errors;
using GitLab.Extension.Utility.Results;

namespace GitLab.Extension.GitLabApi
{
    public class GitLabApiSettings
    {
        private GitLabApiSettings(
            Uri baseUri,
            string accessToken)
        {
            BaseUri = baseUri;
            AccessToken = accessToken;
        }

        public Uri BaseUri { get; }

        public string AccessToken { get; }

        public static Result<GitLabApiSettings> Create(
            string baseUrl,
            string accessToken)
        {
            var validationErrors = ValidateCreateParams(baseUrl, accessToken);
            if (validationErrors.Count > 0)
            {
                return new GitLabApiConfigurationError(validationErrors);
            }

            return new GitLabApiSettings(
                new Uri(baseUrl),
                accessToken);
        }
        
        private static List<IError> ValidateCreateParams(
            string baseUrl, 
            string accessToken)
        {
            var errors = new List<IError>();

            if (string.IsNullOrEmpty(baseUrl) ||
                !Uri.TryCreate(baseUrl, UriKind.Absolute, out var baseUri) ||
                (baseUri.Scheme != Uri.UriSchemeHttp && baseUri.Scheme != Uri.UriSchemeHttps))
            {
                errors.Add(new InvalidBaseUrl("The base URL must be a valid HTTP or HTTPS URL."));
            }

            if (string.IsNullOrEmpty(accessToken))
            {
                errors.Add(new InvalidAccessToken("The access token cannot be null or empty."));
            }

            return errors;
        }
    }
}