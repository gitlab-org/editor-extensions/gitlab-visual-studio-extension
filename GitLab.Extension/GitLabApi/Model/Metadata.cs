﻿namespace GitLab.Extension.GitLabApi.Model
{
    public class Metadata
    {
        public GitLabVersion Version { get; set; }
        public string Revision { get; set; }
        public bool Enterprise { get; set; }
        public KubernetesAgentServer Kas { get; set; }

        public class KubernetesAgentServer
        {
            public bool Enabled { get; set; }
            public string ExternalUrl { get; set; }
            public string Version { get; set; }
        }
    }
}