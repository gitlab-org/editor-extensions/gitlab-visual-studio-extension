﻿using System;
using System.Text.Json;
using System.Text.Json.Serialization;
using GitLab.Extension.GitLabApi.Model;

namespace GitLab.Extension.GitLabApi.Serialization
{
    public class GitLabVersionJsonConverter : JsonConverter<GitLabVersion>
    {
        public override GitLabVersion Read(
            ref Utf8JsonReader reader, 
            Type typeToConvert, 
            JsonSerializerOptions options)
        {
            if (reader.TokenType == JsonTokenType.Null)
            {
                return null;
            }

            try
            {
                var versionString = reader.GetString();
                return GitLabVersion.Parse(versionString);
            }
            catch (Exception ex)
            {
                throw new JsonException($"Error parsing version string '{{versionString}}' at position {{reader.TokenStartIndex}}.", ex);
            }
        }

        public override void Write(
            Utf8JsonWriter writer, 
            GitLabVersion value, 
            JsonSerializerOptions options)
        {
            writer.WriteStringValue(value.ToString());
        }
    }
}