﻿using GitLab.Extension.VS;
using Microsoft.VisualStudio.Imaging.Interop;
using Microsoft.VisualStudio.Shell;
using Microsoft.VisualStudio.Shell.Interop;
using System.Collections.Generic;

namespace GitLab.Extension.InfoBar
{
    public static class InfoBarFactoryExtensions
    {
        public static IInfoBar AttachInfoBarToMainWindow(
            this IInfoBarFactory manager,
            string message,
            IEnumerable<InfoBarAction> action,
            ImageMoniker imageMoniker = default)
        {
            ThreadHelper.ThrowIfNotOnUIThread();

            if (!TryGetMainWindowInfoBarHost(out var host))
            {
                return null;
            }

            return manager.AttachInfoBar(host, message, action, imageMoniker);
        }

        private static bool TryGetMainWindowInfoBarHost(
            out IVsInfoBarHost infoBarHost)
        {
            ThreadHelper.ThrowIfNotOnUIThread();

            var shell = GlobalServiceProvider.GetService<IVsShell>();
            shell.GetProperty((int)__VSSPROPID7.VSSPROPID_MainWindowInfoBarHost, out object value);

            infoBarHost = value as IVsInfoBarHost;
            return infoBarHost != null;
        }
    }
}
