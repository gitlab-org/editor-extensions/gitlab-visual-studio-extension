﻿using GitLab.Extension.LanguageServer.Models;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using GitLab.Extension.Utility.Results;

namespace GitLab.Extension.LanguageServer
{
    public interface ILsClient : IDisposable, IAsyncDisposable
    {
        /// <summary>
        /// Are we connected to a language server process?
        /// </summary>
        bool IsConnected { get; }

        /// <summary>
        /// How are text documents sync'd. Full or Incremental.
        /// Based on this value, call the correct implementation
        /// of SendTextDocumentDidChangeAsync.
        /// </summary>
        TextDocumentSyncKind TextDocumentSyncKind { get; }

        /// <summary>
        /// Connect to a language server. If a language server doesn't
        /// exist one will be created.
        /// </summary>
        /// <returns>True on success, false on failure</returns>
        Task<bool> ConnectAsync();

        /// <summary>
        /// Send the 'textDocument/didOpen' notification message
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="languageId"></param>
        /// <param name="version"></param>
        /// <param name="text"></param>
        /// <returns>True if sent, false if not sent</returns>
        Task<bool> SendTextDocumentDidOpenAsync(string filePath, LsLanguageId languageId, int version, string text);

        /// <summary>
        /// Send the LSP 'textDocument/didChange' notification message when
        /// TextDocumentSyncKind is Full.
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="version"></param>
        /// <param name="fileText"></param>
        /// <returns>True if sent, false if not sent</returns>
        Task<bool> SendTextDocumentDidChangeAsync(string filePath, int version, string fileText);

        /// <summary>
        /// Send the LSP 'textDocument/didChange' notification message when
        /// TextDocumentSyncKind is Incremental.
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="version"></param>
        /// <param name="changes"></param>
        /// <returns></returns>
        Task<bool> SendTextDocumentDidChangeAsync(
            string filePath, int version,
            TextDocumentContentChangeEvent[] changes);

        /// <summary>
        /// Send the 'textDocument/didClose' notification message
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns>True if sent, false if not sent</returns>
        Task<bool> SendTextDocumentDidCloseAsync(string filePath);

        /// <summary>
        /// Send the 'textDocument/inlineCompletion' request message.
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="line"></param>
        /// <param name="character"></param>
        /// <param name="triggerKind"></param>
        /// <param name="token"></param>
        /// <returns>Result of either CompletionItemList or List of CompletionItem.</returns>
        Task<Result<CompletionItemList>> SendTextDocumentCompletionAsync(
            string filePath, uint line, uint character, CompletionTriggerKind triggerKind, CancellationToken token);

        /// <summary>
        /// Send the '$/gitlab/telemetry/suggestion_accepted' notification
        /// </summary>
        /// <param name="trackingId"></param>
        /// <param name="optionId"></param>
        /// <returns></returns>
        Task<bool> SendGitlabTelemetryCodeSuggestionAcceptedAsync(string trackingId, int? optionId = null);
        
        /// <summary>
        /// Send the '$/gitlab/telemetry/suggestion_rejected' notification
        /// </summary>
        /// <param name="trackingId"></param>
        /// <returns></returns>
        Task<bool> SendGitlabTelemetryCodeSuggestionRejectedAsync(string trackingId);

        /// <summary>
        /// Send the '$/gitlab/telemetry/suggestion_cancelled' notification
        /// </summary>
        /// <param name="trackingId"></param>
        /// <returns></returns>
        Task<bool> SendGitlabTelemetryCodeSuggestionCancelledAsync(string trackingId);

        /// <summary>
        /// Send the '$/gitlab/telemetry' suggestion_shown notification
        /// </summary>
        /// <param name="trackingId"></param>
        /// <returns></returns>
        Task<bool> SendGitlabTelemetryCodeSuggestionShownAsync(string trackingId);

        /// <summary>
        /// Send the '$/gitlab/telemetry' suggestion_not_provided notification
        /// </summary>
        /// <param name="trackingId"></param>
        /// <returns></returns>
        Task<bool> SendGitlabTelemetryCodeSuggestionNotProvidedAsync(string trackingId);

        Task<WebviewInfo[]> GetWebviewMetadataAsync();

        Task<bool> SendDidThemeChangeAsync(DidChangeTheme didChangeTheme);

        /// <summary>
        /// Send the '$/gitlab/didChangeDocumentInActiveEditor' notification
        /// </summary>
        /// <param name="filePath">Path of the new active document</param>
        /// <returns></returns>
        Task<bool> SendDidChangeDocumentInActiveEditor(string filePath);

        /// <summary>
        /// Send the 'cancelStreaming' notification to cancel streaming of an in-progress completion
        /// </summary>
        /// <param name="streamId"></param>
        /// <returns></returns>
        Task<bool> SendCancelStream(string streamId);

        Task SendPluginNotification(WebviewMessage message);
    }
}
