﻿using GitLab.Extension.LanguageServer.Models;
using GitLab.Extension.SettingsUtil;
using StreamJsonRpc;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using GitLab.Extension.Utility.Results;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Serilog;

namespace GitLab.Extension.LanguageServer
{
    public abstract class LsClientBase
    {
        protected readonly ExponentialBackoffHelper _backoffHelper;
        protected readonly string _solutionName;
        protected readonly string _solutionPath;
        protected readonly ISettings _settings;
        protected readonly ILogger _logger;

        /// <summary>
        /// Langauge server process manager instance.
        /// None-null indicates we have started a language server
        /// process.
        /// </summary>
        protected readonly ILsProcessManager _lsProcessManager;

        /// <summary>
        /// JsonRPC client connected to language server
        /// </summary>
        protected JsonRpc _rpc;
        /// <summary>
        /// Language server input stream
        /// </summary>
        protected bool _rpcConnected = false;
        /// <summary>
        /// Set after we have cleaned up all instance resources
        /// </summary>
        protected bool _disposed = false;
        /// <summary>
        /// Set when we are trying freeing resources
        /// </summary>
        protected bool _disposing = false;

        /// <summary>
        /// Are we connected to a language server process?
        /// </summary>
        public bool IsConnected { get { return _rpcConnected; } }

        /// <summary>
        /// How text documented are synced (full vs. partial)
        /// </summary>
        public TextDocumentSyncKind TextDocumentSyncKind { get; protected set; }

        /// <summary>
        /// DO NOT INSTANTIATE DIRECTLY, USE LsClientManager!!!
        /// </summary>
        /// <param name="solutionName"></param>
        /// <param name="solutionPath"></param>
        public LsClientBase(ISettings settings, ILsProcessManager lsProcessManager,
            LsClientSolution solution, ILogger logger)
        {
            _backoffHelper = new ExponentialBackoffHelper();
            _solutionName = solution.Name;
            _solutionPath = solution.Path;
            _lsProcessManager = lsProcessManager;
            _settings = settings;
            _logger = logger;
        }

        /// <summary>
        /// Start language server
        /// </summary>
        /// <param name="solutionPath"></param>
        /// <returns>Returns language server port, or -1 on error.</returns>
        protected abstract Task<int> StartLanguageServerAsync(string solutionPath);

        /// <summary>
        /// Called when we lose our connection to the license server
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <exception cref="NotImplementedException"></exception>
        protected void rpc_Disconnected(object sender, JsonRpcDisconnectedEventArgs e)
        {
            // Don't try reconnecting is we have not completed our initial connection
            if (!_rpcConnected)
                return;

            _rpcConnected = false;

            if (_disposed || _disposing)
            {
                _logger.Debug($"{nameof(rpc_Disconnected)} _disposing = true, returning");
                return;
            }

            _logger.Debug($"{nameof(rpc_Disconnected)} Restarting");

            // No need to block waiting for this to happen
            _ = ConnectAsync(true);
        }

        /// <summary>
        /// Check if we are connected to language server
        /// </summary>
        /// <returns></returns>
        protected bool IsRpcConnected()
        {
            return _rpcConnected && !_disposed && !_disposing;
        }

        /// <summary>
        /// Create a "file:///" url from a path
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public static string FilePathToUri(string filePath)
        {
            return $"file://{filePath.Replace(System.IO.Path.DirectorySeparatorChar, '/')}";
        }

        /// <summary>
        /// Connect to a language server. If a language server doesn't
        /// exist one will be created.
        /// </summary>
        /// <returns>True on success, false on failure</returns>
        public async Task<bool> ConnectAsync()
        {
            return await ConnectAsync(false);
        }

        /// <summary>
        /// Connect to a language server. If a language server doesn't
        /// exist one will be created.
        /// </summary>
        /// <param name="reconnect">Reconnect to the language server.</param>
        /// <returns>True on success, false on failure</returns>
        protected abstract Task<bool> ConnectAsync(bool reconnect = false);

        /// <summary>
        /// Send the LSP 'initialize' message
        /// </summary>
        /// <param name="solutionName"></param>
        /// <param name="solutionPath"></param>
        /// <returns></returns>
        protected abstract Task<object> SendInitializeAsync(string solutionName, string solutionPath);

        /// <summary>
        /// Send the 'initialized' notification message
        /// </summary>
        /// <returns>True if sent, false if not sent</returns>
        public virtual async Task<bool> SendInitializedAsync()
        {
            try
            {
                // Don't check if IsRpcConnected. It will always be false
                // until after this notification has been sent.

                _logger.Debug($"{nameof(SendInitializedAsync)}()");

                await _rpc?.NotifyWithParameterObjectAsync("initialized", new { });
                return true;
            }
            catch (Exception e)
            {
                _logger.Warning(e, $"{nameof(SendInitializedAsync)} exception, returning false");
                return false;
            }
        }

        /// <summary>
        /// Send the 'textDocument/didOpen' notification message
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="languageId"></param>
        /// <param name="version"></param>
        /// <param name="text"></param>
        /// <returns>True if sent, false if not sent</returns>
        public virtual async Task<bool> SendTextDocumentDidOpenAsync(string filePath, LsLanguageId languageId, int version, string text)
        {
            try
            {
                if (!IsRpcConnected())
                    return false;

                _logger.Debug($"{nameof(SendTextDocumentDidOpenAsync)}({{FilePath}}, {{LanguageId}}, {{Version}}, text.Length:{{TextLength}})",
                    filePath, languageId, version, text.Length);

                var didOpenParams = new {
                    textDocument = new {
                        uri = FilePathToUri(filePath),
                        languageId = languageId.ToString(),
                        version,
                        text,
                    },
                };

                await _rpc?.NotifyWithParameterObjectAsync("textDocument/didOpen", didOpenParams);
                return true;
            }
            catch (Exception e)
            {
                _logger.Warning(e, $"{nameof(SendTextDocumentDidOpenAsync)} exception, returning false");
                return false;
            }
        }

        /// <summary>
        /// Send the 'textDocument/didClose' notification message
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns>True if sent, false if not sent</returns>
        public virtual async Task<bool> SendTextDocumentDidCloseAsync(string filePath)
        {
            try
            {
                if (!IsRpcConnected())
                    return false;

                _logger.Debug($"{nameof(SendTextDocumentDidCloseAsync)}({{FilePath}})",
                    filePath);

                var didCloseParams = new {
                    textDocument = new {
                        uri = FilePathToUri(filePath),
                    },
                };

                await _rpc?.NotifyWithParameterObjectAsync("textDocument/didClose", didCloseParams);
                return true;
            }
            catch (Exception e)
            {
                _logger.Warning(e, $"{nameof(SendTextDocumentDidCloseAsync)} exception, returning false");
                return false;
            }
        }

        /// <summary>
        /// Send the LSP 'textDocument/didChange' notification message.
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="version"></param>
        /// <param name="fileText"></param>
        /// <returns>True if sent, false if not sent</returns>
        public virtual async Task<bool> SendTextDocumentDidChangeAsync(
            string filePath, int version,
            TextDocumentContentChangeEvent[] changes)
        {
            try
            {
                if (!IsRpcConnected())
                    return false;

                if (TextDocumentSyncKind != TextDocumentSyncKind.Incremental)
                {
                    _logger.Error($"Wrong implementation of SendTextDocumentDidChangeAsync was called (incremental).");
                    throw new Exception($"Wrong implementation of SendTextDocumentDidChangeAsync was called (incremental).");
                }

                _logger.Debug($"{nameof(SendTextDocumentDidChangeAsync)}({{FilePath}}, {{Version}}, {{ChangesCount}})",
                    filePath, version, changes.Length);

                var didChangeParams = new {
                    textDocument = new {
                        uri = FilePathToUri(filePath),
                        version = version,
                    },
                    contentChanges = changes,
                };

                await _rpc?.NotifyWithParameterObjectAsync("textDocument/didChange", didChangeParams);
                return true;
            }
            catch (Exception e)
            {
                _logger.Warning(e, $"{nameof(SendTextDocumentDidChangeAsync)} Exception, returning false");
                return false;
            }
        }

        /// <summary>
        /// Send the LSP 'textDocument/didChange' notification message.
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="version"></param>
        /// <param name="fileText"></param>
        /// <returns>True if sent, false if not sent</returns>
        public virtual async Task<bool> SendTextDocumentDidChangeAsync(string filePath, int version, string fileText)
        {
            try
            {
                if (!IsRpcConnected())
                    return false;

                if (TextDocumentSyncKind != TextDocumentSyncKind.Full)
                {
                    _logger.Error($"Wrong implementation of SendTextDocumentDidChangeAsync was called (full).");
                    throw new Exception($"Wrong implementation of SendTextDocumentDidChangeAsync was called (full).");
                }

                _logger.Debug($"{nameof(SendTextDocumentDidChangeAsync)}({{FilePath}}, {{Version}}, fileText.Length:{{FileTextLength}})",
                    filePath, version, fileText.Length);

                var didChangeParams = new {
                    textDocument = new {
                        uri = FilePathToUri(filePath),
                        version = version,
                    },
                    contentChanges = new [] {
                        new {
                            text = fileText,
                        }
                    }
                };

                await _rpc?.NotifyWithParameterObjectAsync("textDocument/didChange", didChangeParams);
                return true;
            }
            catch (Exception e)
            {
                _logger.Warning(e, $"{nameof(SendTextDocumentDidChangeAsync)} Exception, returning false");
                return false;
            }
        }

        /// <summary>
        /// Send the 'textDocument/inlineCompletion' request message.
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="line"></param>
        /// <param name="character"></param>
        /// <param name="triggerKind"></param>
        /// <param name="token"></param>
        /// <returns>CompletionItemList</returns>
        public virtual async Task<Result<CompletionItemList>> SendTextDocumentCompletionAsync(
            string filePath, uint line, uint character, CompletionTriggerKind triggerKind, CancellationToken token)
        {
            try
            {
                if (!IsRpcConnected() || _rpc == null)
                    return Result.Error<CompletionItemList>("Failed to connect to LSP");

                _logger.Debug($"{nameof(SendTextDocumentCompletionAsync)}({{FilePath}}, {{Line}}, {{Character}})",
                    filePath, line, character);

                var completionParams = new InlineCompletionParams
                {
                    TextDocument = new TextDocumentIdentifier
                    {
                        Uri = FilePathToUri(filePath),
                    },
                    Position = new Position
                    {
                        Line = line,
                        Character = character,
                    },
                    Context = new InlineCompletionContext
                    {
                        TriggerKind = triggerKind
                    }
                };

                var response = await _rpc.InvokeWithParameterObjectAsync<JToken>(
                    "textDocument/inlineCompletion", completionParams, token);
                
                // LSP tends to return empty list of suggestions when token is cancelled
                token.ThrowIfCancellationRequested();

                if (response == null)
                {
                    _logger.Debug($"{nameof(SendTextDocumentCompletionAsync)}: Null response");
                    return CompletionItemList.Empty();
                }

                var deserialized =
                    Serializer.Default.Deserialize<Either<CompletionItemList, List<CompletionItem>>>(
                        response.CreateReader());

                if (deserialized.Value is CompletionItemList)
                {
                    return deserialized.Value as CompletionItemList;
                }

                return new CompletionItemList()
                {
                    IsIncomplete = false,
                    Items = deserialized.Value as List<CompletionItem>
                };
            }
            catch (OperationCanceledException)
            {
                throw;
            }
            catch (Exception e)
            {
                _logger.Debug(e, $"{nameof(SendTextDocumentCompletionAsync)} Exception sending textDocument/inlineCompletion)");
                return Result.Error<CompletionItemList>(e.Message);
            }
        }

        /// <summary>
        /// Send the '$/gitlab/telemetry/suggestion_accepted' notification
        /// </summary>
        /// <param name="trackingId"></param>
        /// <returns></returns>
        public async Task<bool> SendGitlabTelemetryCodeSuggestionAcceptedAsync(string trackingId, int? optionId = null)
        {
            try
            {
                if (!IsRpcConnected())
                    return false;

                _logger.Debug($"{nameof(SendGitlabTelemetryCodeSuggestionAcceptedAsync)}({{TrackingId}})",
                    trackingId);

                var telemetryParams = new {
                    category = "code_suggestions",
                    action = "suggestion_accepted",
                    context = new {
                        trackingId = trackingId,
                        optionId = optionId
                    },
                };

                await _rpc?.NotifyWithParameterObjectAsync("$/gitlab/telemetry", telemetryParams);
                return true;
            }
            catch (Exception e)
            {
                _logger.Warning(e, $"{nameof(SendGitlabTelemetryCodeSuggestionAcceptedAsync)} exception, returning false");
                return false;
            }
        }

        /// <summary>
        /// Send the '$/gitlab/telemetry/suggestion_rejected' notification
        /// </summary>
        /// <param name="trackingId"></param>
        /// <returns></returns>
        public async Task<bool> SendGitlabTelemetryCodeSuggestionRejectedAsync(string trackingId)
        {
            try
            {
                if (!IsRpcConnected())
                    return false;

                _logger.Debug($"{nameof(SendGitlabTelemetryCodeSuggestionRejectedAsync)}({{TrackingId}})",
                    trackingId);

                var telemetryParams = new
                {
                    category = "code_suggestions",
                    action = "suggestion_rejected",
                    context = new
                    {
                        trackingId = trackingId
                    },
                };

                await _rpc?.NotifyWithParameterObjectAsync("$/gitlab/telemetry", telemetryParams);
                return true;
            }
            catch (Exception e)
            {
                _logger.Warning(e, $"{nameof(SendGitlabTelemetryCodeSuggestionRejectedAsync)} exception, returning false");
                return false;
            }
        }

        /// <summary>
        /// Send the '$/gitlab/telemetry/suggestion_cancelled' notification
        /// </summary>
        /// <param name="trackingId"></param>
        /// <returns></returns>
        public async Task<bool> SendGitlabTelemetryCodeSuggestionCancelledAsync(string trackingId)
        {
            try
            {
                if (!IsRpcConnected())
                    return false;

                _logger.Debug($"{nameof(SendGitlabTelemetryCodeSuggestionCancelledAsync)}({{TrackingId}})",
                    trackingId);

                var telemetryParams = new
                {
                    category = "code_suggestions",
                    action = "suggestion_cancelled",
                    context = new
                    {
                        trackingId
                    },
                };

                await _rpc?.NotifyWithParameterObjectAsync("$/gitlab/telemetry", telemetryParams);
                return true;
            }
            catch (Exception e)
            {
                _logger.Warning(e, $"{nameof(SendGitlabTelemetryCodeSuggestionCancelledAsync)} exception, returning false");
                return false;
            }
        }

        /// <summary>
        /// Send the '$/gitlab/telemetry' suggestion_shown notification
        /// </summary>
        /// <param name="trackingId"></param>
        /// <returns></returns>
        public async Task<bool> SendGitlabTelemetryCodeSuggestionShownAsync(string trackingId)
        {
            try
            {
                if (!IsRpcConnected())
                    return false;

                _logger.Debug($"{nameof(SendGitlabTelemetryCodeSuggestionShownAsync)}({{TrackingId}})",
                    trackingId);

                var telemetryParams = new
                {
                    category = "code_suggestions",
                    action = "suggestion_shown",
                    context = new
                    {
                        trackingId = trackingId
                    },
                };

                await _rpc?.NotifyWithParameterObjectAsync("$/gitlab/telemetry", telemetryParams);
                return true;
            }
            catch (Exception e)
            {
                _logger.Warning(e, $"{nameof(SendGitlabTelemetryCodeSuggestionShownAsync)} exception, returning false");
                return false;
            }
        }

        /// <summary>
        /// Send the '$/gitlab/telemetry' suggestion_shown notification
        /// </summary>
        /// <param name="trackingId"></param>
        /// <returns></returns>
        public async Task<bool> SendGitlabTelemetryCodeSuggestionNotProvidedAsync(string trackingId)
        {
            try
            {
                if (!IsRpcConnected())
                    return false;

                _logger.Debug($"{nameof(SendGitlabTelemetryCodeSuggestionNotProvidedAsync)}({{TrackingId}})",
                    trackingId);

                var telemetryParams = new
                {
                    category = "code_suggestions",
                    action = "suggestion_not_provided",
                    context = new
                    {
                        trackingId = trackingId
                    },
                };

                await _rpc?.NotifyWithParameterObjectAsync("$/gitlab/telemetry", telemetryParams);
                return true;
            }
            catch (Exception e)
            {
                _logger.Warning(e, $"{nameof(SendGitlabTelemetryCodeSuggestionNotProvidedAsync)} exception, returning false");
                return false;
            }
        }
        
        public async Task<bool> SendDidChangeDocumentInActiveEditor(string filePath)
        {
            try
            {
                if (!IsRpcConnected())
                    return false;

                _logger.Debug($"{nameof(SendDidChangeDocumentInActiveEditor)} ({{filePath}})", filePath);

                await _rpc?.NotifyAsync("$/gitlab/didChangeDocumentInActiveEditor", FilePathToUri(filePath));
                return true;
            }
            catch (Exception e)
            {
                _logger.Warning(e, $"{nameof(SendDidChangeDocumentInActiveEditor)} exception, returning false");
                return false;
            }
        }
        
        public async Task<bool> SendCancelStream(string streamId)
        {
            try
            {
                if (!IsRpcConnected())
                    return false;
                
                _logger.Debug($"{nameof(SendCancelStream)} ({{streamId}})", streamId);

                await _rpc?.NotifyWithParameterObjectAsync("cancelStreaming", new {id = streamId});
                return true;
            }
            catch (Exception e)
            {
                _logger.Warning(e, $"{nameof(SendCancelStream)} exception, returning false");
                return false;
            }
        }
        
        protected DidChangeConfig SettingsToDidChangeConfig()
        {
            return new DidChangeConfig
            {
                Settings = new DidChangeConfig.SettingsConfig
                {
                    Token = _settings.GitLabAccessToken,
                    BaseUrl = _settings.GitLabUrl,
                    Telemetry = new DidChangeConfig.SettingsConfig.TelemetryConfig
                    {
                        Enabled = _settings.IsTelemetryEnabled,
                    },
                    OpenTabsContext = _settings.IsOpenTabsContextEnabled,
                    IgnoreCertificateErrors = _settings.IgnoreCertificateErrors,
                    FeatureFlags = new DidChangeConfig.SettingsConfig.FeatureFlagsConfig
                    {
                        StreamCodeGenerations = _settings.IsSuggestionStreamingEnabled
                    },
                    CodeCompletion = new DidChangeConfig.SettingsConfig.CodeCompletionConfig
                    {
                        Enabled = _settings.IsCodeSuggestionsEnabled,
                        AdditionalLanguages = _settings.AdditionalLanguages
                    },
                    DuoChat = new DidChangeConfig.SettingsConfig.DuoChatConfig
                    {
                        Enabled = _settings.IsDuoChatEnabled
                    }
                }
            };
        }

    }
}
