﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;
using StreamJsonRpc;
using System.Diagnostics;
using GitLab.Extension.LanguageServer.Models;
using System.Reflection;
using GitLab.Extension.SettingsUtil;
using System.Runtime.InteropServices;
using Serilog;

namespace GitLab.Extension.LanguageServer
{
    /// <summary>
    /// Code suggestions language server client.
    /// </summary>
    /// <remarks>
    /// Once the client is connected to a language server,
    /// it will automatically reconnect using an exponential backoff
    /// if the connection is lost.
    /// 
    /// Reconnecting means stopping/starting the language server
    /// in addition to our rpc connection.
    /// </remarks>
    public class LsClientGolang : LsClientBase, ILsClient
    {
        /// <summary>
        /// TCP connection to the language server
        /// </summary>
        private TcpClient _tcpClient;
        /// <summary>
        /// The lanaguage server port number
        /// </summary>
        private int _lsPort = -1;

        private readonly LsClientRpc _lsClientRpc;

        /// <summary>
        /// DO NOT INSTANTIATE DIRECTLY, USE LsClientManager!!!
        /// </summary>
        public LsClientGolang(ISettings settings, ILsProcessManager lsProcessManager,
            LsClientSolution solution, ILogger logger, LsClientRpc lsClientRpc)
            : base(settings, lsProcessManager, solution, logger)
        {
            _logger.Debug($"{nameof(LsClientGolang)}({{SolutionName}}, {{SolutionPath}}",
                solution.Name, solution.Path);

            _lsClientRpc = lsClientRpc;
        }

        /// <summary>
        /// Start language server
        /// </summary>
        /// <param name="solutionPath"></param>
        /// <returns>Returns language server port, or -1 on error.</returns>
        protected override async Task<int> StartLanguageServerAsync(string solutionPath)
        {
            _logger.Debug($"{nameof(StartLanguageServerAsync)}({{SolutionPath}})",
                solutionPath);

            if (_lsPort != -1)
            {
                _logger.Debug($"{nameof(StartLanguageServerAsync)} _lsPort set, returning {{LsPort}}",
                    _lsPort);

                return _lsPort;
            }

            _lsPort = -1;

            await Task.Run(() =>
            {
                try
                {
                    _logger.Debug($"{nameof(StartLanguageServerAsync)} Setting and starting _lsProcessManager");
                    _lsProcessManager.StartLanguageServer(
                        solutionPath,
                        _settings.GitLabUrl,
                        _settings.GitLabAccessToken,
                        out _lsPort);
                }
                catch(Exception ex)
                {
                    _logger.Debug(ex, $"{nameof(StartLanguageServerAsync)} exception");
                    if(ex.InnerException != null)
                        _logger.Debug(ex.InnerException, $"LsClient.{nameof(StartLanguageServerAsync)} innerException");
                }
            });

            _settings.SettingsChangedEvent += SettingsChangedEvent;

            return _lsPort;
        }

        /// <summary>
        /// Called when the settings change to restart
        /// our language server with latest settings.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SettingsChangedEvent(object sender, EventArgs e)
        {
            if (_disposed)
                return;

            _logger.Debug($"{nameof(SettingsChangedEvent)}");
            // Only restart when access token has changed
            var settingsChangedEventArgs = e as Settings.SettingsEventArgs;
            if (settingsChangedEventArgs.ChangedSettingKey != Settings.GitLabAccessTokenKey
                && settingsChangedEventArgs.ChangedSettingKey != Settings.ApplicationName
                && settingsChangedEventArgs.ChangedSettingKey != Settings.GitLabUrlKey)
            {
                _logger.Debug($"{nameof(SettingsChangedEvent)} access token wasn't modified. Not restarting.");
                return;
            }

            // No need to block waiting for this to happen
            _ = ConnectAsync(true);
        }

        /// <summary>
        /// Cleanup resources before restarting
        /// </summary>
        private async Task RestartCleanupAsync()
        {
            _lsPort = -1;

            if (_rpc != null)
                _rpc.Disconnected -= rpc_Disconnected;

            _rpc = null;
            _tcpClient?.Dispose();

            await _lsProcessManager.StopLanguageServerAsync(_solutionPath);
        }

        /// <summary>
        /// Connect to a language server. If a language server doesn't
        /// exist one will be created.
        /// </summary>
        /// <param name="reconnect">Reconnect to the language server.</param>
        /// <returns>True on success, false on failure</returns>
        protected override async Task<bool> ConnectAsync(bool reconnect = false)
        {
            try
            {
                if (_disposed)
                    throw new ObjectDisposedException(nameof(LsClientGolang));

                if (_disposing)
                    return false;

                _logger.Debug($"{nameof(ConnectAsync)}({{Reconnect}}): Name: {{SolutionName}}, Path: {{SolutionPath}}",
                    reconnect, _solutionName, _solutionPath);

                // Make sure we have a valid configuration
                if (!_settings.Configured)
                {
                    _logger.Debug($"{nameof(ConnectAsync)} Settings.Instance.Configured == false, returning false");
                    return false;
                }

                if (!reconnect && _tcpClient != null)
                {
                    _logger.Debug($"{nameof(ConnectAsync)} !reconnect && _tcpClient != null, returning false");
                    return false;
                }

                // Use our backoff helper to make sure we don't
                // keep restarting in a tight loop.
                // There is no delay on first call
                if (!_backoffHelper.ShouldRetry)
                {
                    _logger.Debug($"{nameof(ConnectAsync)} !_backoffHelper.ShouldRetry, returning false");
                    return false;
                }
                await _backoffHelper.DelayAsync();

                // If we are reconnecting cleanup existing resources
                if (reconnect)
                {
                    _logger.Debug($"{nameof(ConnectAsync)} await RestartCleanupAsync");
                    await RestartCleanupAsync();
                }

                // Start a license server instance if we haven't already
                await StartLanguageServerAsync(_solutionPath);
                if (_lsPort == -1)
                {
                    _logger.Debug($"{nameof(ConnectAsync)} _lsPort == -1, returning false");
                    return false;
                }

                _logger.Debug($"{nameof(ConnectAsync)} Connecting via TcpClient, _lsPort={{LsPort}}",
                    _lsPort);
                // Connect to our license server
                _tcpClient = new TcpClient();
                await _tcpClient.ConnectAsync(IPAddress.Loopback, _lsPort);
                var stream = _tcpClient.GetStream();

                // Attach to the network stream for RPC
                _logger.Debug($"{nameof(ConnectAsync)} JsonRpc.Attach");
                _rpc = JsonRpc.Attach(stream, _lsClientRpc);
                _rpc.Disconnected += rpc_Disconnected;

                // Send the LSP 'initialize' message
                _logger.Debug($"{nameof(ConnectAsync)} SendInitializeAsync");
                var initializationResult = await SendInitializeAsync(_solutionName, _solutionPath);

                _rpcConnected = true;
                return true;
            }
            catch(Exception ex)
            {
                // TODO - Log errors/display to user somehow

                _logger.Warning(ex, $"{nameof(ConnectAsync)} exception");
                if (ex.InnerException != null)
                    _logger.Warning(ex.InnerException, $"{nameof(ConnectAsync)} inner exception");

                return false;
            }
        }

        /// <summary>
        /// Send the LSP 'initialize' message
        /// </summary>
        /// <param name="solutionName"></param>
        /// <param name="solutionPath"></param>
        /// <returns></returns>
        protected override async Task<object> SendInitializeAsync(string solutionName, string solutionPath)
        {
            try
            {
                _logger.Debug($"{nameof(SendInitializeAsync)}({{SolutionName}}, {{SolutionPath}})",
                    solutionName, solutionPath);

                var clientCapabilities = new ClientCapabilities()
                {
                    textDocument = new ClientCapabilities.TextDocumentClientCapabilities()
                    {
                        completion = new ClientCapabilities.TextDocumentClientCapabilities.CompletionClientCapabilities()
                        {
                            competionItem = new ClientCapabilities.TextDocumentClientCapabilities.CompletionClientCapabilities.CompletionItemClientCapabilities()
                            {
                                documentationFormat = MarkupKind.PlainText,
                                insertReplaceSupport = false,
                            },
                            completionItemKind = new ClientCapabilities.TextDocumentClientCapabilities.CompletionClientCapabilities.CompletionItemKindClientCapabilities()
                            {
                                valueSet = new CompletionItemKind[] { CompletionItemKind.Text }
                            },
                            contextSupport = false,
                            insertTextMode = InsertTextMode.AdjustIndentation,
                        },
                        codeAction = new ClientCapabilities.TextDocumentClientCapabilities.CodeActionClientCapabilities()
                        {
                        },
                        synchronization = new ClientCapabilities.TextDocumentClientCapabilities.TextDocumentSyncClientCapabilities()
                        {
                        },
                    },
                };

                var initializeParams = new InitializeParams
                {
                    processId = Process.GetCurrentProcess().Id,
                    capabilities = clientCapabilities,
                    clientInfo = new InitializeParams.ClientInfo()
                    {
                        name = "gl-visual-studio-extension",
                        version = $"{Assembly.GetExecutingAssembly().GetName().Version}; arch:{RuntimeInformation.OSArchitecture}; vs:{VSVersion.FullVersion}; os:{VSVersion.OSVersion}",
                    },
                    workspaceFolders = new WorkspaceFolder[]
                    {
                        new WorkspaceFolder()
                        {
                            name = solutionName,
                            // Uri will automatically convert a file path (c:\...) to
                            // a uri (file:///c:/...)
                            uri = FilePathToUri(solutionPath),
                        },
                    },
                };

                var initializeResult = await _rpc?.InvokeWithParameterObjectAsync<InitializeResult>(
                    "initialize", initializeParams);

                return initializeResult;
            }
            catch(Exception ex)
            {
                _logger.Warning(ex, $"LsClient.{nameof(SendInitializeAsync)} exception");
                throw;
            }
        }

        /// <summary>
        /// Common dispose items that are not async. Called
        /// by both DisposeAsync and Dispose.
        /// </summary>
        private void CommonDispose()
        {
            _settings.SettingsChangedEvent -= SettingsChangedEvent;
        }

        public async ValueTask DisposeAsync()
        {
            if (_disposed)
                return;
            
            _disposing = true;

            _logger.Debug($"{nameof(DisposeAsync)}");
            CommonDispose();
            await RestartCleanupAsync();

            _disposed = true;

            GC.SuppressFinalize(this);
        }

        public void Dispose()
        {
            if (_disposed)
                return;

            _disposing = true;

            _logger.Debug($"{nameof(Dispose)}");

            CommonDispose();
#pragma warning disable VSTHRD002 // Avoid problematic synchronous waits
            RestartCleanupAsync().GetAwaiter().GetResult();
#pragma warning restore VSTHRD002 // Avoid problematic synchronous waits

            _disposed = true;

            GC.SuppressFinalize(this);
        }

        public Task<WebviewInfo[]> GetWebviewMetadataAsync()
        {
            throw new NotImplementedException();
        }

        public Task<bool> SendDidThemeChangeAsync(DidChangeTheme didChangeTheme)
        {
            throw new NotImplementedException();
        }

        public Task SendPluginNotification(WebviewMessage message)
        {
            throw new NotImplementedException();
        }
    }
}
