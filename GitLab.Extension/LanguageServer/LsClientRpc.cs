﻿using GitLab.Extension.LanguageServer.Models;
using Serilog;
using StreamJsonRpc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GitLab.Extension.CodeSuggestions;
using GitLab.Extension.CodeSuggestions.Model;
using GitLab.Extension.LanguageServer.Plugin;
using GitLab.Extension.LanguageServer.Plugin.Model;
using GitLab.Extension.Status;
using GitLab.Extension.Status.Models;
using Newtonsoft.Json.Linq;

namespace GitLab.Extension.LanguageServer
{
    /// <summary>
    /// Handlers for LSP messages sent from the language server to the client.
    /// </summary>
    public class LsClientRpc
    {
        private readonly ISuggestionEventBroker _suggestionEventBroker;
        private readonly IPluginMessageDispatcher _pluginMessageDispatcher;
        private readonly IFeatureStateManager _featureStateManager;

        public LsClientRpc(
            ISuggestionEventBroker suggestionEventBroker,
            IPluginMessageDispatcher pluginMessageDispatcher,
            IFeatureStateManager featureStateManager)
        {
            _suggestionEventBroker = suggestionEventBroker;
            _pluginMessageDispatcher = pluginMessageDispatcher;
            _featureStateManager = featureStateManager;
        }

        [JsonRpcMethod("textDocument/publishDiagnostics")]
        public void textDocument_publishDiagnostics(dynamic pdParams)
        {
            // The language server reports errors this way.
            // Right now we don't have a way to handle errors
            // outside of logging it to the debug view.
            Log.Debug("textDocument/publishDiagnostics: {Diagnostic}",
                pdParams.Diagnostic);
        }

        [JsonRpcMethod("$/gitlab/plugin/notification", UseSingleObjectParameterDeserialization = true)]
        public Task HandleWebviewNotificationAsync(
            PluginMessage notificationParams)
        {
            return _pluginMessageDispatcher.DispatchAsync(
                new PluginMessageRoute(notificationParams.PluginId, PluginMessageType.Notification,
                    notificationParams.Type), notificationParams.Payload);
        }
        
        [JsonRpcMethod("$/gitlab/plugin/request", UseSingleObjectParameterDeserialization = true)]
        public async Task<JToken> HandleWebviewRequestAsync(
            PluginMessage requestParams)
        {
            var result =  await _pluginMessageDispatcher.DispatchAsync(
                new PluginMessageRoute(requestParams.PluginId, PluginMessageType.Request,
                    requestParams.Type), requestParams.Payload);

            return result;
        }

        [JsonRpcMethod("streamingCompletionResponse", UseSingleObjectParameterDeserialization = true)]
        public void StreamingCompletionResponse(StreamingSuggestionNotification streamingSuggestion)
        {
            _suggestionEventBroker.Publish(new SuggestionUpdatedEvent(new CompletionChunk(streamingSuggestion)));
        }
        
        [JsonRpcMethod("$/gitlab/featureStateChange", UseSingleObjectParameterDeserialization = true)]
        public void FeatureStateResponse(List<FeatureStateNotification> featureStates)
        {
            featureStates
                .Select(s => s.Map())
                .Where(s => s != null)
                .ToList()
                .ForEach(s => _featureStateManager.Update(s));
        }
    }
}
