﻿namespace GitLab.Extension.LanguageServer
{
    public class LsLanguageId
    {
        public static LsLanguageId ABAP { get; } = new LsLanguageId("abap");
        public static LsLanguageId BibTeX { get; } = new LsLanguageId("bibtex");
        public static LsLanguageId C { get; } = new LsLanguageId("c");
        public static LsLanguageId Clojure { get; } = new LsLanguageId("clojure");
        public static LsLanguageId Coffeescript { get; } = new LsLanguageId("coffeescript");
        public static LsLanguageId CPP { get; } = new LsLanguageId("cpp");
        public static LsLanguageId CSharp { get; } = new LsLanguageId("csharp");
        public static LsLanguageId CSS { get; } = new LsLanguageId("css");
        public static LsLanguageId CudaCpp { get; } = new LsLanguageId("cuda-cpp");
        public static LsLanguageId Dart { get; } = new LsLanguageId("dart");
        public static LsLanguageId Diff { get; } = new LsLanguageId("diff");
        public static LsLanguageId Dockerfile { get; } = new LsLanguageId("dockerfile");
        public static LsLanguageId Elixir { get; } = new LsLanguageId("elixir");
        public static LsLanguageId Erlang { get; } = new LsLanguageId("erlang");
        public static LsLanguageId FSharp { get; } = new LsLanguageId("fsharp");
        public static LsLanguageId GitCommit { get; } = new LsLanguageId("git-commit");
        public static LsLanguageId GitRebase { get; } = new LsLanguageId("git-rebase");
        public static LsLanguageId Go { get; } = new LsLanguageId("go");
        public static LsLanguageId Groovy { get; } = new LsLanguageId("groovy");
        public static LsLanguageId HAML { get; } = new LsLanguageId("haml");
        public static LsLanguageId Handlebars { get; } = new LsLanguageId("handlebars");
        public static LsLanguageId HTML { get; } = new LsLanguageId("html");
        public static LsLanguageId Ini { get; } = new LsLanguageId("ini");
        public static LsLanguageId Java { get; } = new LsLanguageId("java");
        public static LsLanguageId JavaScript { get; } = new LsLanguageId("javascript");
        public static LsLanguageId JavaScriptReact { get; } = new LsLanguageId("javascriptreact");
        public static LsLanguageId JSON { get; } = new LsLanguageId("json");
        public static LsLanguageId Kotlin { get; } = new LsLanguageId("kotlin");
        public static LsLanguageId LaTeX { get; } = new LsLanguageId("latex");
        public static LsLanguageId Less { get; } = new LsLanguageId("less");
        public static LsLanguageId Lua { get; } = new LsLanguageId("lua");
        public static LsLanguageId Makefile { get; } = new LsLanguageId("makefile");
        public static LsLanguageId Markdown { get; } = new LsLanguageId("markdown");
        public static LsLanguageId ObjectiveC { get; } = new LsLanguageId("objective-c");
        public static LsLanguageId ObjectiveCpp { get; } = new LsLanguageId("objective-cpp");
        public static LsLanguageId Perl { get; } = new LsLanguageId("perl");
        public static LsLanguageId Perl6 { get; } = new LsLanguageId("perl6");
        public static LsLanguageId Php { get; } = new LsLanguageId("php");
        public static LsLanguageId Powershell { get; } = new LsLanguageId("powershell");
        public static LsLanguageId Pug { get; } = new LsLanguageId("jade");
        public static LsLanguageId Python { get; } = new LsLanguageId("python");
        public static LsLanguageId R { get; } = new LsLanguageId("r");
        public static LsLanguageId Razor { get; } = new LsLanguageId("razor");
        public static LsLanguageId Ruby { get; } = new LsLanguageId("ruby");
        public static LsLanguageId Rust { get; } = new LsLanguageId("rust");
        public static LsLanguageId SASS { get; } = new LsLanguageId("sass");
        public static LsLanguageId Scala { get; } = new LsLanguageId("scala");
        public static LsLanguageId SCSS { get; } = new LsLanguageId("scss");
        public static LsLanguageId ShaderLab { get; } = new LsLanguageId("shaderlab");
        public static LsLanguageId ShellScript { get; } = new LsLanguageId("shellscript");
        public static LsLanguageId SQL { get; } = new LsLanguageId("sql");
        public static LsLanguageId Svelte { get; } = new LsLanguageId("svelte");
        public static LsLanguageId Swift { get; } = new LsLanguageId("swift");
        public static LsLanguageId Terraform { get; } = new LsLanguageId("terraform");
        public static LsLanguageId TeX { get; } = new LsLanguageId("tex");
        public static LsLanguageId TypeScript { get; } = new LsLanguageId("typescript");
        public static LsLanguageId TypeScriptReact { get; } = new LsLanguageId("typescriptreact");
        public static LsLanguageId VisualBasic { get; } = new LsLanguageId("vb");
        public static LsLanguageId Vue { get; } = new LsLanguageId("vue");
        public static LsLanguageId WindowsBat { get; } = new LsLanguageId("bat");
        public static LsLanguageId XML { get; } = new LsLanguageId("xml");
        public static LsLanguageId XSL { get; } = new LsLanguageId("xsl");
        public static LsLanguageId YAML { get; } = new LsLanguageId("yaml");

        public string Value { get; private set; }

        private LsLanguageId(string value)
        {
            Value = value;
        }

        public override string ToString()
        {
            return Value;
        }
    }
}
