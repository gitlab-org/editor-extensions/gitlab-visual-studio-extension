﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.IO;
using System.Reflection;
using System.Net;
using System.Net.Sockets;
using System.Diagnostics;
using System.Threading;
using System.Collections.Concurrent;
using Serilog;
using System.ComponentModel;

namespace GitLab.Extension.LanguageServer
{
    /// <summary>
    /// Manage language server processes. 
    /// Singleton pattern with single instance as LsProcessManager.Instance.
    /// </summary>
    /// <remarks>
    /// LsClient handles restarting the process.
    /// </remarks>
    public class LsProcessManagerTs : ILsProcessManager
    {
        private readonly ILogger _logger;

        /// <summary>
        /// All language server processes. Key is Solution Path.
        /// </summary>
        private readonly ConcurrentDictionary<string, Process> Processes = new ConcurrentDictionary<string, Process>();
        
        /// <summary>
        /// Locks for language server process. Key is Solution Path.
        /// </summary>
        private readonly ConcurrentDictionary<string, 
            (object lockLanguageServer, SpinLock stoppingLanguageServer)> ProcessLocks = new ConcurrentDictionary<string, (object lockLanguageServer, SpinLock stoppingLanguageServer)>();
        
        /// <summary>
        /// License server executable name
        /// </summary>
        private const string LsExecutable = "gitlab-lsp-win-x64.exe";

        private AutoResetEvent _startupBannerEvent = new AutoResetEvent(false);
        //private bool _seenStartupBanner = false;

        private bool _disposed;

        public string LanguageServerExecutable => LsExecutable;

        public LsProcessManagerTs(ILogger logger)
        {
            _logger = logger;
        }

        /// <summary>
        /// Finalizer to make sure we cleanup all of our resources
        /// </summary>
        ~LsProcessManagerTs()
        {
#pragma warning disable VSTHRD002 // Avoid problematic synchronous waits
            DisposeAsync().GetAwaiter().GetResult();
#pragma warning restore VSTHRD002 // Avoid problematic synchronous waits
        }

        /// <summary>
        /// Start a language server watching solutionPath.
        /// </summary>
        /// <param name="solutionPath"></param>
        /// <param name="gitlabUrl"></param>
        /// <param name="gitlabToken"></param>
        /// <param name="port">Port the server is listening on</param>
        /// <returns>True if a language server was started, false on error, or if the server is already started.
        /// If the language server was already started, port is set to the listing port.</returns>
        /// <exception cref="ObjectDisposedException"></exception>
        public bool StartLanguageServer(string solutionPath, string gitlabUrl, string gitlabToken, out int port)
        {
            throw new NotImplementedException();
        }

        public bool StartLanguageServerStdio(string solutionPath, string gitlabUrl, string gitlabToken, out Stream stdin, out Stream stdout)
        {
            if (_disposed)
                throw new ObjectDisposedException(nameof(LsProcessManagerGolang));

            _logger.Debug($"{nameof(StartLanguageServerStdio)}({{SolutionPath}}, {{GitLabUrl}}, ****)",
                solutionPath, gitlabUrl);

            // Get the lock for this solution, or create a new one
            if (!ProcessLocks.TryGetValue(solutionPath, out var locks))
            {
                locks = (new object(), new SpinLock());

                if(!ProcessLocks.TryAdd(solutionPath, locks))
                    ProcessLocks.TryGetValue(solutionPath, out locks);
            }

            lock (locks.lockLanguageServer)
            {
                try
                {
                    var newProcess = false;

                    if (!Processes.TryGetValue(solutionPath, out var lsProcess))
                    {
                        lsProcess = new Process();
                        newProcess = true;

                        if (!Processes.TryAdd(solutionPath, lsProcess))
                            Processes.TryGetValue(solutionPath, out lsProcess);
                    }
                    else if (!lsProcess.HasExited)
                    {
                        _logger.Debug($"{nameof(StartLanguageServerStdio)} !lsProcess.HasExited, returning false");

                        stdin = lsProcess.StandardInput.BaseStream;
                        stdout = lsProcess.StandardOutput.BaseStream;

                        return false;
                    }

                    // If we have an existing lsProcess instance, dispose it before
                    // starting a new language server
                    if (!newProcess && lsProcess.HasExited)
                    {
                        _logger.Debug($"{nameof(StartLanguageServerStdio)} Disposing exited lsProcess");

                        lsProcess.ErrorDataReceived -= _lsProcess_ErrorDataReceived;
                        lsProcess.Dispose();

                        lsProcess = new Process();
                        Processes[solutionPath] = lsProcess;
                    }

                    var lsPath = GetLsExecutablePath();
                    if (!File.Exists(lsPath))
                    {
                        _logger.Warning($"{nameof(StartLanguageServerStdio)}: Language server executable not found at {lsPath}");
                    }

                    var processStartInfo = new ProcessStartInfo
                    {
                        CreateNoWindow = true,
                        RedirectStandardError = true,
                        RedirectStandardInput = true,
                        RedirectStandardOutput = true,
                        UseShellExecute = false,
                        FileName = lsPath,
                        Arguments = $"--stdio",
                    };

                    _logger.Debug($"{nameof(StartLanguageServerStdio)} ==== STARTING {{SolutionPath}} ====",
                        solutionPath);

                    lsProcess.ErrorDataReceived += _lsProcess_ErrorDataReceived;
                    lsProcess.StartInfo = processStartInfo;
                    _logger.Debug($"{nameof(StartLanguageServerStdio)} ProcessStartInfo: FileName={processStartInfo.FileName}, Arguments={processStartInfo.Arguments}, UseShellExecute={processStartInfo.UseShellExecute}");
                    
                    try
                    {
                        lsProcess.Start();
                        _logger.Information($"{nameof(StartLanguageServerStdio)}: Language server process started successfully.");
                    }
                    catch (Exception ex)
                    {
                        _logger.Error(ex, $"{nameof(StartLanguageServerStdio)}: Failed to start process.");
                        throw;
                    }

                    lsProcess.BeginErrorReadLine();

                    stdin = lsProcess.StandardInput.BaseStream;
                    stdout = lsProcess.StandardOutput.BaseStream;

                    try
                    {
                        // Link our new child process to the visual studio process.
                        // This will guarantee that killing visual studio will also
                        // kill our children.
                        ChildProcessTracker.AddProcess(lsProcess);
                    }
                    catch
                    {
                        // Ignore exceptions.
                        // If visual studio is running in compatibility mode with Win7,
                        // we might get an exception. Nothing we can do if an exception
                        // is raised.
                    }

                    return true;
                }
                catch(Exception ex)
                {
                    _logger.Warning(ex, $"{nameof(StartLanguageServerStdio)} Exception");
                    throw;
                }
            }
        }

        private void _lsProcess_ErrorDataReceived(object sender, DataReceivedEventArgs e)
        {
            if (e.Data == null)
                return;

            if (string.IsNullOrWhiteSpace(e.Data))
            {
                _logger.Debug($"LS: {e.Data}");
                return;
            }

            foreach (var line in e.Data.SplitToLines())
                _logger.Debug($"LS: {line}");
        }

        /// <summary>
        /// Stop the language server associated with solutionPath.
        /// </summary>
        /// <param name="solutionPath"></param>
        /// <returns>Returns true if a server was killed, false otherwise.</returns>
        /// <exception cref="ObjectDisposedException"></exception>
        public async Task<bool> StopLanguageServerAsync(string solutionPath)
        {
            if (_disposed)
                throw new ObjectDisposedException(nameof(LsProcessManagerGolang));

            if (!ProcessLocks.TryGetValue(solutionPath, out var locks))
                return false;

            return await Task.Run<bool>(new Func<bool>(() =>
            {
                lock (locks.lockLanguageServer)
                {
                    var lockToken = false;
                    locks.stoppingLanguageServer.Enter(ref lockToken);
                    if (!lockToken)
                        return false;

                    try
                    {
                        if (!Processes.TryGetValue(solutionPath, out var lsProcess))
                            return true;

                        _logger.Debug($"{nameof(StopLanguageServerAsync)}({{SolutionPath}})",
                            solutionPath);

                        if (!lsProcess.HasExited)
                        {
                            try
                            {
                                lsProcess.Kill();
                                _logger.Information($"{nameof(StopLanguageServerAsync)}: Successfully terminated the language server process.");
                            }
                            catch (Win32Exception ex)
                            {
                                // Sometimes Access Denied
                                // exception is thrown if the process
                                // exits between the HasExited check
                                // and Kill()
                                _logger.Warning(ex, $"{nameof(StopLanguageServerAsync)}: Failed to kill process. Potential access restriction.");
                            }
                            
                            lsProcess.WaitForExit();
                        }

                        lsProcess.Dispose();
                        lsProcess = null;

                        Processes.TryRemove(solutionPath, out _);
                        ProcessLocks.TryRemove(solutionPath, out _);

                        return true;
                    }
                    catch(Exception ex)
                    {
                        _logger.Warning(ex, $"{nameof(StopLanguageServerAsync)} exception");
                        throw;
                    }
                    finally
                    {
                        locks.stoppingLanguageServer.Exit();
                    }
                }
            }));
        }

        private string GetLsExecutablePath()
        {
            var extensionPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);

            var lsPath = Path.Combine(extensionPath, "bin");
            lsPath = Path.Combine(lsPath, LsExecutable);

            // TODO convert this into a log statement
            if (!File.Exists(lsPath))
                _logger.Error($"{nameof(GetLsExecutablePath)} executable not found in path: {{ExtensionPath}}",
                    extensionPath);

            return lsPath;
        }

        /// <summary>
        /// Get an available (free) TCP port
        /// </summary>
        /// <returns>Returns an available (free) TCP port</returns>
        private int GetAvailablePort()
        {
            var loopbackEndpoint = new IPEndPoint(IPAddress.Loopback, port: 0);
            using (var socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp))
            {
                socket.Bind(loopbackEndpoint);
                return ((IPEndPoint)socket.LocalEndPoint).Port;
            }
        }

        protected async Task DisposeAsync(bool disposing)
        {
            if (_disposed)
                return;

            if (disposing)
            {
                foreach(var solution in Processes.Keys.ToArray())
                {
                    await StopLanguageServerAsync(solution);
                }
            }

            // TODO: free unmanaged resources (unmanaged objects) and override finalizer
            // TODO: set large fields to null
            _disposed = true;
        }

        /// <summary>
        /// Dispose of held resources.
        /// </summary>
        /// <param name="solutionPath">Dispose of just resources related to solutionPath. When null, all resources are freed</param>
        /// <returns></returns>
        public async ValueTask DisposeAsync()
        {
            await DisposeAsync(disposing: true);
            GC.SuppressFinalize(this);
        }
    }
}
