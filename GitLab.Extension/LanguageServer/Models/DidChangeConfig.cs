using System.Collections.Generic;
using Newtonsoft.Json;

namespace GitLab.Extension.LanguageServer.Models
{
    public class DidChangeConfig
    {
        [JsonProperty("settings")]
        public SettingsConfig Settings { get; set; } = new SettingsConfig();
        public class SettingsConfig
        {
            [JsonProperty("token")]
            public string Token { get; set; }

            [JsonProperty("baseUrl")]
            public string BaseUrl { get; set; }

            [JsonProperty("openTabsContext")]
            public bool OpenTabsContext { get; set; } = true;

            [JsonProperty("ignoreCertificateErrors")]
            public bool IgnoreCertificateErrors { get; set; } = false;

            [JsonProperty("codeCompletion")]
            public CodeCompletionConfig CodeCompletion { get; set; } = new CodeCompletionConfig();
            
            [JsonProperty("duoChat")]
            public DuoChatConfig DuoChat { get; set; } = new DuoChatConfig();

            [JsonProperty("telemetry")]
            public TelemetryConfig Telemetry { get; set; } = new TelemetryConfig();
            
            [JsonProperty("featureFlags")]
            public FeatureFlagsConfig FeatureFlags { get; set; } = new FeatureFlagsConfig();

            [JsonProperty("logLevel")]
            public string LogLevel { get; set; } = "debug";

            public class FeatureFlagsConfig
            {
                [JsonProperty("streamCodeGenerations")]
                public bool StreamCodeGenerations { get; set; } = true;
            }
            
            public class CodeCompletionConfig
            {
                [JsonProperty("enabled")]
                public bool Enabled { get; set; } = true;
                
                [JsonProperty("enableSecretRedaction")]
                public bool EnableSecretRedaction { get; set; } = true;
                
                [JsonProperty("additionalLanguages")]
                public List<string> AdditionalLanguages { get; set; } = new List<string>();
            }
            
            public class DuoChatConfig
            {
                [JsonProperty("enabled")]
                public bool Enabled { get; set; } = true;
            }
            
            public class TelemetryConfig
            {
                [JsonProperty("enabled")]
                public bool Enabled { get; set; } = true;

                [JsonProperty("actions")]
                public ActionConfig[] Actions { get; set; } =
                {
                    new ActionConfig("suggestion_shown"),
                    new ActionConfig("suggestion_rejected"),
                    new ActionConfig("suggestion_accepted"),
                    new ActionConfig("suggestion_cancelled"),
                    new ActionConfig("suggestion_not_provided"),
                };
                
                public class ActionConfig
                {
                    [JsonProperty("action")]
                    public string Action { get; set; }

                    public ActionConfig(string action)
                    {
                        Action = action;
                    }
                }
            }
        }
    }
}
