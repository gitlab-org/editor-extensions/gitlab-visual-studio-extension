using System.Collections.Generic;
using Newtonsoft.Json;

namespace GitLab.Extension.LanguageServer.Models
{
    public class DidChangeTheme
    {
        public DidChangeTheme(Dictionary<string, string> styles)
        {
            Styles = styles;
        }
        [JsonProperty("styles")]
        public Dictionary<string, string> Styles { get; }
    }
}