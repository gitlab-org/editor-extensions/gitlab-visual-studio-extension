using System;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace GitLab.Extension.LanguageServer.Models
{
    public class EitherConverter<T1, T2> : JsonConverter<Either<T1, T2>>
    {
        public override Either<T1, T2> ReadJson(
            JsonReader reader,
            Type objectType,
            Either<T1, T2> existingValue,
            bool hasExistingValue,
            JsonSerializer serializer)
        {
            var token = JToken.ReadFrom(reader);

            try
            {
                // First try to deserialize as T1
                var value1 = token.ToObject<T1>(serializer);
                if (value1 != null)
                {
                    return Either<T1, T2>.FromT1(value1);
                }
            }
            catch
            {
                // Ignore the exception and try T2
            }

            try
            {
                // Then try to deserialize as T2
                var value2 = token.ToObject<T2>(serializer);
                if (value2 != null)
                {
                    return Either<T1, T2>.FromT2(value2);
                }
            }
            catch
            {
                // Ignore the exception
            }

            throw new JsonSerializationException($"Could not deserialize to either {typeof(T1)} or {typeof(T2)}");
        }

        public override void WriteJson(JsonWriter writer, Either<T1, T2> value, JsonSerializer serializer)
        {
            if (value == null)
            {
                writer.WriteNull();
                return;
            }

            if (value is T1)
            {
                serializer.Serialize(writer, (T1) value.Value);
            }
            else if (value is T2)
            {
                serializer.Serialize(writer, (T2) value.Value);
            }
            else
            {
                throw new JsonSerializationException($"Could not serialize instance of Either<{typeof(T1)},{typeof(T2)}>");
            }
        }
    }
    
    public class Either<T1, T2>
    {
        public object Value { get; }

        private Either(T1 value)
        {
            Value = value;
        }

        private Either(T2 value)
        {
            Value = value;
        }

        public static Either<T1, T2> FromT1(T1 value) => new Either<T1, T2>(value);
        public static Either<T1, T2> FromT2(T2 value) => new Either<T1, T2>(value);

        public static implicit operator T1(Either<T1, T2> either)
        {
            if (either.Value is T1 value)
                return value;
            throw new InvalidCastException($"Cannot cast {typeof(Either<T1, T2>)} to {typeof(T1)}");
        }

        public static implicit operator T2(Either<T1, T2> either)
        {
            if (either.Value is T2 value)
                return value;
            throw new InvalidCastException($"Cannot cast {typeof(Either<T1, T2>)} to {typeof(T2)}");
        }
    }
}