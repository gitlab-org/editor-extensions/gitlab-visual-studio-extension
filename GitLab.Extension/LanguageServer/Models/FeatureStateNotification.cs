using System.Collections.Generic;
using Newtonsoft.Json;

namespace GitLab.Extension.LanguageServer.Models
{
    public class FeatureStateNotification
    {
        [JsonProperty("featureId")]
        public string FeatureId { get; set; }
        
        [JsonProperty("engagedChecks")]
        public List<FeatureStateCheckNotification> EngagedChecks { get; set; }
    }

    public class FeatureStateCheckNotification
    {
        [JsonProperty("checkId")]
        public string CheckId { get; set; }
        
        [JsonProperty("details")]
        public string Details { get; set; }
    }
}
