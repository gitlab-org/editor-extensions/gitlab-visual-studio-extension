using Newtonsoft.Json.Linq;

namespace GitLab.Extension.LanguageServer.Models
{
    public class PluginMessage
    {
        public string PluginId { get; set; }
        public string Type { get; set; }
        public JToken Payload { get; set; }
    }
}
