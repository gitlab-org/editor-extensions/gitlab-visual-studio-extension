using Newtonsoft.Json;

namespace GitLab.Extension.LanguageServer.Models
{
    public class StreamingCancelledNotification
    {
        [JsonProperty("id")]
        public string Id { get; set; }
    }
}