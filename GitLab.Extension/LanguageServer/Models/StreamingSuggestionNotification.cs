using Newtonsoft.Json;

namespace GitLab.Extension.LanguageServer.Models
{
    public class StreamingSuggestionNotification
    {
        [JsonProperty("id")]
        public string Id { get; }
        [JsonProperty("completion")]
        public string Completion { get; }
        [JsonProperty("done")]
        public bool Done { get; }

        public StreamingSuggestionNotification(string id, string completion, bool done)
        {
            Id = id;
            Completion = completion;
            Done = done;
        }
    }
}