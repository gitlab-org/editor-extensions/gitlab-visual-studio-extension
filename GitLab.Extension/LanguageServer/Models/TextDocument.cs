﻿using System;
using System.Collections.Generic;
using GitLab.Extension.LanguageServer.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace GitLab.Extension.LanguageServer.Models
{
    public enum TextDocumentSyncKind
    {
        None = 0,
        Full = 1,
        Incremental = 2
    }

    public class Position
    {
        [JsonProperty("line")]
        public uint Line { get; set; }
        
        [JsonProperty("character")]
        public uint Character { get; set; }
    }

    public class Range
    {
        [JsonProperty("start")]
        public Position Start { get; set; }

        [JsonProperty("end")]
        public Position End { get; set; }
    }

    public class TextDocumentContentChangeEvent
    {
        [JsonProperty("range")]
        public Range Range { get; set; }

        [JsonProperty("rangeLength")]
        public uint? RangeLength { get; set; }

        [JsonProperty("text")]
        public string Text { get; set; }
    }

    public class InlineCompletionParams
    {
        [JsonProperty("textDocument")]
        public TextDocumentIdentifier TextDocument { get; set; }
        
        [JsonProperty("position")]
        public Position Position { get; set; }
        
        [JsonProperty("context")]
        public InlineCompletionContext Context { get; set; }
    }

    public class InlineCompletionContext
    {
        [JsonProperty("triggerKind")]
        public CompletionTriggerKind TriggerKind { get; set; }
    }

    public class TextDocumentIdentifier
    {
        [JsonProperty("uri")]
        public string Uri { get; set; }
    }

    public class CompletionItemList
    {
        [JsonProperty("isIncomplete")]
        public bool IsIncomplete { get; set; }

        [JsonProperty("items")]
        public List<CompletionItem> Items { get; set; }

        public static CompletionItemList Empty()
        {
            return new CompletionItemList
            {
                IsIncomplete = false,
                Items = new List<CompletionItem>()
            };
        }
    }

    public class CompletionItem
    {
        [JsonProperty("label")]
        public string Label { get; set; }

        [JsonProperty("labelDetails")]
        public CompletionItemLabelDetails LabelDetails { get; set; }

        [JsonProperty("kind")]
        public CompletionItemKind Kind { get; set; }

        [JsonProperty("tags")]
        public List<CompletionItemTag> Tags { get; set; }

        [JsonProperty("detail")]
        public string Detail { get; set; }

        [JsonProperty("documentation")]
        [JsonConverter(typeof(EitherConverter<string, MarkupContent>))]
        public Either<string, MarkupContent> Documentation { get; set; }

        [JsonProperty("deprecated")]
        public bool? Deprecated { get; set; }

        [JsonProperty("preselect")]
        public bool? Preselect { get; set; }

        [JsonProperty("sortText")]
        public string SortText { get; set; }

        [JsonProperty("filterText")]
        public string FilterText { get; set; }

        [JsonProperty("insertText")]
        public string InsertText { get; set; }

        [JsonProperty("insertTextFormat")]
        public InsertTextFormat InsertTextFormat { get; set; }

        [JsonProperty("insertTextMode")]
        public InsertTextMode InsertTextMode { get; set; }

        [JsonProperty("textEdit")]
        [JsonConverter(typeof(EitherConverter<TextEdit, InsertReplaceEdit>))]
        public Either<TextEdit, InsertReplaceEdit> TextEdit { get; set; }

        [JsonProperty("textEditText")]
        public string TextEditText { get; set; }

        [JsonProperty("additionalTextEdits")]
        public List<TextEdit> AdditionalTextEdits { get; set; }

        [JsonProperty("commitCharacters")]
        public List<string> CommitCharacters { get; set; }

        [JsonProperty("command")]
        public Command Command { get; set; }

        [JsonProperty("data")]
        public object Data { get; set; }
    }
    
    public class Command
    {
        [JsonProperty("title")]
        public string Title { get; set; }
        
        [JsonProperty("command")]
        public string CommandName { get; set; }
        
        [JsonProperty("arguments")]
        public List<object> Arguments { get; set; }
    }
    
    public class TextEdit
    {
        [JsonProperty("range")]
        public Range Range { get; set; }
        
        [JsonProperty("newText")]
        public string NewText { get; set; }
    }
    
    public class CompletionItemLabelDetails
    {
        [JsonProperty("detail")]
        public string Detail { get; set; }
        
        [JsonProperty("description")]
        public string Description { get; set; }
    }
    
    public class MarkupContent
    {
        [JsonProperty("kind")]
        public string Kind { get; set; }
        
        [JsonProperty("value")]
        public string Value { get; set; }
    }
    
    public class InsertReplaceEdit
    {
        [JsonProperty("newText")]
        public string NewText { get; set; }
        
        [JsonProperty("insert")]
        public Range Insert { get; set; }
        
        [JsonProperty("replace")]
        public Range Replace { get; set; }
    }
}
