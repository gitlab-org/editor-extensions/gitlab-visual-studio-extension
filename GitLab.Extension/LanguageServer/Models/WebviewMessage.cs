using Newtonsoft.Json;

namespace GitLab.Extension.LanguageServer.Models
{
    public class WebviewMessage
    {
        [JsonProperty("pluginId")]
        public string PluginId { get; set; }
        
        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("payload")]
        public object Payload { get; set; }
    }
}
