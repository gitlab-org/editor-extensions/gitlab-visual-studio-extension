using System;
using System.Collections.Generic;
using System.Linq;

namespace GitLab.Extension.LanguageServer.Models
{
    /// <summary>
    /// Contains information about all available webviews.
    /// </summary>
    public class WebviewMetadata
    {
    }

    /// <summary>
    /// Contains information about a webview.
    /// </summary>
    public class WebviewInfo
    {
        public string Id { get; }
        public string Title { get; }
        public IReadOnlyList<string> Uris { get; }

        public WebviewInfo(string id, string title, IEnumerable<string> uris)
        {
            Id = id ?? throw new ArgumentNullException(nameof(id));
            Title = title ?? throw new ArgumentNullException(nameof(title));
            Uris = (uris ?? throw new ArgumentNullException(nameof(uris))).ToList().AsReadOnly();
        }
    }
}