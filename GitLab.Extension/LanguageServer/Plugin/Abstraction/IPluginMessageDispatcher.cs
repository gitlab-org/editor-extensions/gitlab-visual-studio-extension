using System.Threading.Tasks;
using GitLab.Extension.LanguageServer.Plugin.Model;
using Newtonsoft.Json.Linq;

namespace GitLab.Extension.LanguageServer.Plugin
{
    public interface IPluginMessageDispatcher
    {
        Task<JToken> DispatchAsync(PluginMessageRoute route, JToken payload);
    }
}
