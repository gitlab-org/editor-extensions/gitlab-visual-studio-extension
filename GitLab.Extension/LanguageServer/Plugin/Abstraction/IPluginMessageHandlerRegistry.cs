using GitLab.Extension.LanguageServer.Plugin.Model;
using GitLab.Extension.LanguageServer.Plugin.Utilities;

namespace GitLab.Extension.LanguageServer.Plugin
{
    public interface IPluginMessageHandlerRegistry
    {
        void RegisterMessageHandler(PluginMessageRoute route, PluginMessageHandlerDelegate handler);
    }
}
