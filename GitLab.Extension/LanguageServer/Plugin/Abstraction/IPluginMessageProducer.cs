using System.Threading.Tasks;

namespace GitLab.Extension.LanguageServer.Plugin
{
    public interface IPluginMessagePublisher
    {
        Task NotifyAsync(string type);
        Task NotifyAsync<TPayload>(string type, TPayload payload);
    }
}
