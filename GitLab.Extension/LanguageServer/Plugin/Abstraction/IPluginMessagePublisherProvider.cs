namespace GitLab.Extension.LanguageServer.Plugin
{
    public interface IPluginMessagePublisherProvider
    {
        IPluginMessagePublisher Get(string pluginId);
    }
}
