using System;

namespace GitLab.Extension.LanguageServer.Plugin.Attributes
{
    [AttributeUsage(AttributeTargets.Parameter)]
    public sealed class PayloadAttribute : Attribute
    {
    }
}
