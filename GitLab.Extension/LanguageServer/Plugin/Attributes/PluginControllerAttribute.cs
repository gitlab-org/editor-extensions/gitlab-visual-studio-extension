using System;

namespace GitLab.Extension.LanguageServer.Plugin.Attributes
{
    public sealed class PluginControllerAttribute : Attribute
    {
        public string PluginId { get; }

        public PluginControllerAttribute(string pluginId)
        {
            PluginId = pluginId;
        }
    }
}
