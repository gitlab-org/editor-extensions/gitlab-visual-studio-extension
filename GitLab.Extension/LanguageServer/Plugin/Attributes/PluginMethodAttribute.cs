using System;
using GitLab.Extension.LanguageServer.Plugin.Model;

namespace GitLab.Extension.LanguageServer.Plugin.Attributes
{
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = true)]
    public class PluginMethodAttribute : Attribute
    {
        public PluginMessageType MessageType { get; }
        public string Method { get; }

        public PluginMethodAttribute(PluginMessageType type, string method)
        {
            MessageType = type;
            Method = method;
        }
    }
}
