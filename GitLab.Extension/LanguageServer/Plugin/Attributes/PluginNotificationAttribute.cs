using System;
using GitLab.Extension.LanguageServer.Plugin.Model;

namespace GitLab.Extension.LanguageServer.Plugin.Attributes
{
    [AttributeUsage(AttributeTargets.Method)]
    public sealed class PluginNotificationAttribute : PluginMethodAttribute
    {
        public PluginNotificationAttribute(string method) : base(PluginMessageType.Notification, method)
        {
        }
    }
}
