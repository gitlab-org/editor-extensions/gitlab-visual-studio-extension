using System;
using GitLab.Extension.LanguageServer.Plugin.Model;

namespace GitLab.Extension.LanguageServer.Plugin.Attributes
{
    [AttributeUsage(AttributeTargets.Method)]
    public sealed class PluginRequestAttribute : PluginMethodAttribute
    {
        public PluginRequestAttribute(string method) : base(PluginMessageType.Request, method)
        {
        }
    }
}
