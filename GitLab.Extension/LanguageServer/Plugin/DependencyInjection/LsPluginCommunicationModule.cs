using System.Reflection;
using Autofac;
using GitLab.Extension.LanguageServer.Plugin.Services;
using GitLab.Extension.LanguageServer.Plugin.Utilities;
using Module = Autofac.Module;

namespace GitLab.Extension.LanguageServer.Plugin.DependencyInjection
{
    public class LsPluginCommunicationModule : Module
    {
        private readonly Assembly[] _assemblies;

        public LsPluginCommunicationModule(params Assembly[] assemblies)
        {
            _assemblies = assemblies.Length == 0
                ? new[] { Assembly.GetExecutingAssembly() }
                : assemblies;
        }

        protected override void Load(ContainerBuilder builder)
        {
            RegisterCoreServices(builder);
            ControllerRegistrationUtilities.RegisterControllers(builder, _assemblies);
        }

        private static void RegisterCoreServices(ContainerBuilder builder)
        {
            builder
                .RegisterType<PluginMessagePublisherProvider>()
                .AsImplementedInterfaces()
                .SingleInstance();

            builder
                .RegisterType<PluginMessageService>()
                .AsSelf()
                .AsImplementedInterfaces()
                .SingleInstance();
        }
    }
}
