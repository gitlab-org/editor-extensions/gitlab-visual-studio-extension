using System;
using GitLab.Extension.LanguageServer.Plugin.Model;

namespace GitLab.Extension.LanguageServer.Plugin.Exceptions
{
    public class HandlerNotFoundException : Exception
    {
        public HandlerNotFoundException(PluginMessageRoute route)
            : base($"Cannot find handler for webview {route.PluginId} of type {route.Method}")
        {
        }
    }
}
