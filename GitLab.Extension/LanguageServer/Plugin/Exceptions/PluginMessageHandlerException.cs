using System;
using GitLab.Extension.LanguageServer.Plugin.Model;

namespace GitLab.Extension.LanguageServer.Plugin.Exceptions
{
    public class PluginMessageHandlerException : Exception
    {
        public PluginMessageRoute Route { get; }

        public PluginMessageHandlerException(PluginMessageRoute route, string message, Exception innerException)
            : base(message, innerException)
        {
            Route = route;
        }
    }
}
