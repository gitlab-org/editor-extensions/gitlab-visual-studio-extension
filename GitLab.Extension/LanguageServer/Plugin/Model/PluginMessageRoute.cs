using System;

namespace GitLab.Extension.LanguageServer.Plugin.Model
{
    public readonly struct PluginMessageRoute : IEquatable<PluginMessageRoute>
    {
        public string PluginId { get; }
        public PluginMessageType MessageType { get; }
        public string Method { get; }

        public PluginMessageRoute(string pluginId, PluginMessageType messageType, string method)
        {
            if (string.IsNullOrWhiteSpace(pluginId))
            {
                throw new ArgumentException("PluginId cannot be null or empty.", nameof(pluginId));
            }

            if (string.IsNullOrWhiteSpace(method))
            {
                throw new ArgumentException("Method cannot be null or empty.", nameof(method));
            }

            PluginId = pluginId;
            MessageType = messageType;
            Method = method;
        }

        public bool Equals(PluginMessageRoute other)
        {
            return PluginId == other.PluginId && MessageType == other.MessageType && Method == other.Method;
        }

        public override bool Equals(object obj)
        {
            return obj is PluginMessageRoute other && Equals(other);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(PluginId, (int)MessageType, Method);
        }

        public override string ToString()
        {
            return $"{PluginId}:{MessageType}:{Method}";
        }
    }
}
