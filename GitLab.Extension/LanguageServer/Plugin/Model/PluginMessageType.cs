namespace GitLab.Extension.LanguageServer.Plugin.Model
{
    public enum PluginMessageType
    {
        Notification,
        Request
    }
}
