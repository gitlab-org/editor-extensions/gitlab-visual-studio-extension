using System;
using GitLab.Extension.Workspace;
using System.Threading.Tasks;
using GitLab.Extension.LanguageServer.Models;
using Newtonsoft.Json.Linq;

namespace GitLab.Extension.LanguageServer.Plugin.Services
{
    public class PluginMessagePublisherProvider : IPluginMessagePublisherProvider
    {
        private readonly IActiveWorkspaceIdObservable _activeWorkspaceIdObservable;
        private readonly IWorkspaceLsClientProvider _lsClientProvider;

        public PluginMessagePublisherProvider(
            IActiveWorkspaceIdObservable activeWorkspaceIdObservable,
            IWorkspaceLsClientProvider lsClientProvider)
        {
            _activeWorkspaceIdObservable = activeWorkspaceIdObservable;
            _lsClientProvider = lsClientProvider;
        }

        public IPluginMessagePublisher Get(string pluginId)
        {
            return new PluginMessagePublisher(
                            pluginId,
                            _activeWorkspaceIdObservable,
                            _lsClientProvider);
        }
    }

    public class PluginMessagePublisher : IPluginMessagePublisher
    {
        private string _pluginId;
        private ILsClient _lsClient;

        public PluginMessagePublisher(
            string pluginId, 
            IActiveWorkspaceIdObservable activeWorkspaceIdObservable,
            IWorkspaceLsClientProvider lsClientProvider)
        {
            _pluginId = pluginId;

            activeWorkspaceIdObservable.Subscribe(async workspaceId =>
            {
                if (workspaceId.HasValue)
                {
                    _lsClient = await lsClientProvider.GetClientAsync(workspaceId.Value);
                } else
                {
                    _lsClient = null;
                }
            }); 
        }

        public async Task NotifyAsync(string type)
        {
            await _lsClient?.SendPluginNotification(new WebviewMessage
            {
                PluginId = _pluginId,
                Type = type
            });
        }

        public async Task NotifyAsync<TPayload>(string type, TPayload payload)
        {
            var payloadJToken = JToken.FromObject(payload);

            await _lsClient?.SendPluginNotification(new WebviewMessage
            {
                PluginId = _pluginId,
                Type = type,
                Payload = payloadJToken
            });
        }
    }
}
