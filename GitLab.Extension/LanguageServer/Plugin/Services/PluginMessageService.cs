using System;
using System.Collections.Concurrent;
using System.Threading.Tasks;
using GitLab.Extension.LanguageServer.Plugin.Exceptions;
using GitLab.Extension.LanguageServer.Plugin.Model;
using GitLab.Extension.LanguageServer.Plugin.Utilities;
using Newtonsoft.Json.Linq;
using Serilog;

namespace GitLab.Extension.LanguageServer.Plugin.Services
{
    public class PluginMessageService : IPluginMessageDispatcher, IPluginMessageHandlerRegistry
    {
        private readonly ConcurrentDictionary<PluginMessageRoute, PluginMessageHandlerDelegate> _handlers =
            new ConcurrentDictionary<PluginMessageRoute, PluginMessageHandlerDelegate>();

        private readonly ILogger _logger;

        public PluginMessageService(ILogger logger)
        {
            _logger = logger;
        }

        public async Task<JToken> DispatchAsync(PluginMessageRoute route, JToken payload)
        {
            if (!_handlers.TryGetValue(route, out var handler))
            {
                _logger.Warning("Handler not found for route: {Route}", route);
                throw new HandlerNotFoundException(route);
            }

            try
            {
                _logger.Debug("Dispatching message for route: {Route}", route);
                return await handler.Invoke(payload);
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Error occurred while handling message for route: {Route}", route);
                throw new PluginMessageHandlerException(route, "An error occurred while handling the plugin message",
                    ex);
            }
        }

        public void RegisterMessageHandler(PluginMessageRoute route, PluginMessageHandlerDelegate handler)
        {
            if (!_handlers.TryAdd(route, handler))
            {
                throw new InvalidOperationException($"Handler for route {route} already registered.");
            }

            _logger.Verbose("Registered handler for route: {Route}", route);
        }
    }
}
