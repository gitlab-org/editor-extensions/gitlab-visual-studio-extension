using System;
using System.Linq;
using System.Reflection;
using Autofac;
using GitLab.Extension.LanguageServer.Plugin.Attributes;
using GitLab.Extension.LanguageServer.Plugin.Model;

namespace GitLab.Extension.LanguageServer.Plugin.Utilities
{
    public static class ControllerRegistrationUtilities
    {
        public static void RegisterControllers(
            ContainerBuilder builder,
            Assembly[] assemblies)
        {
            var controllers =
                assemblies
                    .SelectMany(assembly => assembly.GetTypes())
                    .Where(type => type.GetCustomAttribute<PluginControllerAttribute>(true) != null);

            foreach (var controller in controllers) RegisterController(builder, controller);
        }

        private static void RegisterController(
            ContainerBuilder builder,
            Type controllerType)
        {
            var pluginControllerAttribute = controllerType.GetCustomAttribute<PluginControllerAttribute>(true);
            if (string.IsNullOrWhiteSpace(pluginControllerAttribute.PluginId))
            {
                return;
            }

            builder
                .RegisterType(controllerType)
                .AsSelf()
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();

            var methodInfos =
                controllerType
                    .GetMethods()
                    .Where(x => x.GetCustomAttribute<PluginMethodAttribute>() != null);

            foreach (var methodInfo in methodInfos)
            {
                var methodMetadata = methodInfo.GetCustomAttribute<PluginMethodAttribute>();

                builder.RegisterBuildCallback(scope =>
                {
                    var registry = scope.Resolve<IPluginMessageHandlerRegistry>();
                    var route = new PluginMessageRoute(pluginControllerAttribute.PluginId, methodMetadata.MessageType,
                        methodMetadata.Method);
                    var handler = PluginMessageHandlerDelegateUtilities.CreateHandlerDelegate(methodInfo, scope);

                    registry.RegisterMessageHandler(route, handler);
                });
            }
        }
    }
}
