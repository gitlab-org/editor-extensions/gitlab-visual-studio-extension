using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace GitLab.Extension.LanguageServer.Plugin.Utilities
{
    public delegate Task<JToken> PluginMessageHandlerDelegate(JToken jToken);
}
