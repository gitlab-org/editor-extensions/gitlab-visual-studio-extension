using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading.Tasks;
using Autofac;
using GitLab.Extension.LanguageServer.Plugin.Attributes;
using Newtonsoft.Json.Linq;

namespace GitLab.Extension.LanguageServer.Plugin.Utilities
{
    public static class PluginMessageHandlerDelegateUtilities
    {
        public static PluginMessageHandlerDelegate CreateHandlerDelegate(
            MethodInfo methodInfo,
            ILifetimeScope lifetimeScope)
        {
            var controllerType = methodInfo.DeclaringType;
            var parameters = methodInfo.GetParameters();

            var lambda = BuildLambdaExpression(methodInfo, controllerType, parameters);
            var compiled = lambda.Compile();

            return async payload =>
            {
                await using var scope = lifetimeScope.BeginLifetimeScope();
                var controller = lifetimeScope.Resolve(controllerType);
                return await compiled(controller, payload, scope);
            };
        }

        private static Expression<Func<object, object, ILifetimeScope, Task<JToken>>> BuildLambdaExpression(
            MethodInfo methodInfo,
            Type controllerType,
            ParameterInfo[] parameters)
        {
            // Define parameters for the lambda expression
            var controllerInstance = Expression.Parameter(typeof(object), "controller");
            var payloadParameter = Expression.Parameter(typeof(object), "payload");
            var scopeParameter = Expression.Parameter(typeof(ILifetimeScope), "lifetimeScope");

            // Convert controller to its specific type
            var controllerCast = Expression.Convert(controllerInstance, controllerType);

            // Create expressions for each parameter of the method
            var parameterExpressions = ResolveParameters(parameters, payloadParameter, scopeParameter);

            // Build the method call with the resolved parameters
            var call = Expression.Call(controllerCast, methodInfo, parameterExpressions);

            Expression body;
            if (methodInfo.ReturnType == typeof(void))
            {
                // For void methods, wrap the call in a Task<JToken> that returns null
                body = Expression.Block(
                    call,
                    Expression.Constant(Task.FromResult<JToken>(JValue.CreateNull())));
            }
            else if (typeof(Task).IsAssignableFrom(methodInfo.ReturnType))
            {
                // handle async return
                if (methodInfo.ReturnType.IsGenericType
                    && methodInfo.ReturnType.GetGenericTypeDefinition() == typeof(Task<>))
                {
                    var resultType = methodInfo
                        .ReturnType.GetGenericArguments()[0];
                    var taskResultParameter = Expression.Parameter(typeof(Task<>).MakeGenericType(resultType), "t");


                    if (resultType == typeof(JToken))
                    {
                        body = Expression.Convert(call, typeof(Task<JToken>));
                    }
                    else
                    {
                        // Handle the task and ensure we await it, then convert to JToken
                        var continuation = Expression.Call(
                            call,
                            nameof(Task.ContinueWith),
                            new[] { typeof(JToken) },
                            Expression.Lambda(
                                Expression.Convert(
                                    Expression.Call(
                                        typeof(JToken),
                                        nameof(JToken.FromObject),
                                        null,
                                        Expression.Convert(Expression.Property(taskResultParameter, "Result"),
                                            typeof(object))
                                    ),
                                    typeof(JToken)
                                ),
                                taskResultParameter
                            )
                        );
                        body = Expression.Convert(continuation, typeof(Task<JToken>));
                    }
                }
                else
                {
                    body = Expression.Convert(call, typeof(Task<JToken>));
                }
            }
            else
            {
                // For synchronous methods, convert the return type to JToken and wrap in Task<JToken>
                var jTokenConversion = Expression.Convert(
                    Expression.Call(typeof(JToken), nameof(JToken.FromObject), null, call),
                    typeof(JToken)
                );
                body = Expression.Convert(
                    Expression.Call(typeof(Task), "FromResult", new[] { typeof(JToken) }, jTokenConversion),
                    typeof(Task<JToken>));
            }

            return Expression.Lambda<Func<object, object, ILifetimeScope, Task<JToken>>>(body, controllerInstance,
                payloadParameter, scopeParameter);
        }

        private static List<Expression> ResolveParameters(
            ParameterInfo[] parameters,
            Expression payloadParameter,
            Expression scopeParameter)
        {
            return parameters
                .Select(parameter =>
                    parameter.GetCustomAttribute<PayloadAttribute>() != null
                        ? BuildPayloadExpression(parameter, payloadParameter)
                        : BuildDependencyResolveExpression(parameter, scopeParameter)
                )
                .ToList();
        }

        private static Expression BuildPayloadExpression(ParameterInfo parameter, Expression payloadParameter)
        {
            return Expression.Convert(
                Expression.Call(
                    typeof(PluginMessageHandlerDelegateUtilities),
                    nameof(ConvertPayload),
                    new[] { parameter.ParameterType },
                    payloadParameter
                ),
                parameter.ParameterType
            );
        }

        private static Expression BuildDependencyResolveExpression(ParameterInfo parameter, Expression scopeParameter)
        {
            return Expression.Call(
                typeof(ResolutionExtensions),
                nameof(ResolutionExtensions.Resolve),
                new[] { parameter.ParameterType },
                scopeParameter
            );
        }

        public static object ConvertPayload<T>(object payload)
        {
            return payload switch
            {
                T _ => payload,
                JObject jObject => jObject.ToObject<T>(),
                JToken token => token.ToObject<T>(),
                _ => throw new ArgumentException(
                    $"Invalid payload type: expected {typeof(T)}, got {payload?.GetType()}")
            };
        }
    }
}
