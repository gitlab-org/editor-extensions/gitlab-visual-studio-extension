﻿using System;

namespace GitLab.Extension
{
    public static class PackageIds
    {
        // Package GUID
        public const string GitLabPackageIdString = "4c68a5e8-056b-4fe9-b381-50208f5e8f35";
        public static readonly Guid GitLabPackage = new Guid(GitLabPackageIdString);

        // Command Set GUID
        public static readonly Guid GitLabPackageCmdSet = new Guid("{114BEDCA-3334-4577-A410-16D264708C6A}");

        // Menu IDs
        public const int MenuGitLab = 0x0100;
        public const int MenuGitLabContext = 0x0101;

        // Group IDs
        public const int GroupGitLabTools = 0x0200;
        public const int GroupGitLabMainMenu = 0x0201;
        public const int GroupGitLabCodeWindow = 0x0202;
        public const int GroupGitLabCodeWindowMenu = 0x0203;

        // Command IDs
        public const int CmdOpenDuoChat = 0x0300;
        public const int CmdToggleCodeSuggestions = 0x0301;
        public const int CmdDuoChatExplainCode = 0x0302;
        public const int CmdDuoChatFixCode = 0x0303;
        public const int CmdDuoChatTestCode = 0x0304;
        public const int CmdDuoChatRefactorCode = 0x0305;
        public const int CmdNextSuggestion = 0x0306;
        public const int CmdPrevSuggestion = 0x0307;
    }
}
