﻿using System;
using System.Collections.Generic;
using GitLab.Extension.LanguageServer;
using Serilog.Events;

namespace GitLab.Extension.SettingsUtil
{
    public interface ISettings
    {
        /// <summary>
        /// Call before setting multiple settings. This
        /// prevents the SettingsChangedEvent from being
        /// triggered on each individual settings change.
        /// </summary>
        void StartBatchSettingsUpdate();

        /// <summary>
        /// Called to stop batch mode and send a 
        /// SettingsChangedEvent.
        /// </summary>
        void StopBatchSettingsUpdate();

        /// <summary>
        /// GitLab Access Token
        /// </summary>
        string GitLabAccessToken { get; set; }

        /// <summary>
        /// Is code suggestions enabled
        /// </summary>
        bool IsCodeSuggestionsEnabled { get; set; }

        /// <summary>
        /// Is sending telemetry data enabled
        /// </summary>
        bool IsTelemetryEnabled { get; set; }
        
        /// <summary>
        /// Is sending of open tabs contents as context enabled
        /// </summary>
        bool IsOpenTabsContextEnabled { get; set; }
        
        /// <summary>
        /// Are certificate errors ignored
        /// </summary>
        bool IgnoreCertificateErrors { get; set; }
        
        /// <summary>
        /// Is Duo Chat enabled
        /// </summary>
        bool IsDuoChatEnabled { get; set; }
        
        /// <summary>
        /// Is code generation streaming enabled
        /// </summary>
        bool IsSuggestionStreamingEnabled { get; set; }

        /// <summary>
        /// GitLab URL
        /// </summary>
        string GitLabUrl { get; set; }

        /// <summary>
        /// Get the current log level. Defaults to Warning.
        /// </summary>
        LogEventLevel LogLevel { get; set; }
        
        /// <summary>
        /// Additional languages
        /// </summary>
        List<string> AdditionalLanguages { get; }

        /// <summary>
        /// Do we have a valid looking configuration
        /// </summary>
        bool Configured { get; }

        /// <summary>
        /// Set the additional languages string.
        /// </summary>
        void SetAdditionalLanguages(string languagesString);

        /// <summary>
        /// Event triggered when settings change. The event args
        /// is an instance of Settings.SettingsEventArgs and contains
        /// the key for the setting that changed.
        /// </summary>
        event EventHandler SettingsChangedEvent;
    }
}
