﻿using System;
using System.Reactive.Subjects;

namespace GitLab.Extension.SettingsUtil.Observable
{
    public class SettingsObservable : IObservable<ISettings>
    {
        private readonly BehaviorSubject<ISettings> _subject;
        private readonly ISettings _settings;

        public SettingsObservable(
            ISettings settings)
        {
            _settings = settings;
            _settings.SettingsChangedEvent += OnSettingsChanged;
            _subject = new BehaviorSubject<ISettings>(settings);
        }

        public IDisposable Subscribe(IObserver<ISettings> observer) => 
            _subject.Subscribe(observer);
        
        private void OnSettingsChanged(
            object sender,
            EventArgs args)
        {
            _subject.OnNext(_settings);
        }
    }
}