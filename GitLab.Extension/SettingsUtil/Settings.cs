﻿using System;
using System.Collections.Generic;
using System.Linq;
using Serilog;
using Serilog.Events;

namespace GitLab.Extension.SettingsUtil
{
    /// <summary>
    /// Extension settings. Setting are stored in the registry.
    /// </summary>
    public class Settings : ISettings
    {
        public class SettingsEventArgs : EventArgs
        {
            public string ChangedSettingKey;

            public SettingsEventArgs(string changedSettingKey)
            {
                ChangedSettingKey = changedSettingKey;
            }
        }

        /// <summary>
        /// Old registry key from PoC extension
        /// </summary>
        public const string ApplicationNamePoC = "GitLabCodeSuggestionsVisualStudio";
        /// <summary>
        /// Registry key name for settings
        /// </summary>
        public const string ApplicationName = "GitLabExtensionVisualStudio";
        public const string GitLabAccessTokenKey = "GitLabAccessToken";
        public const string IsCodeSuggestionsEnabledKey = "IsCodeSuggestionsEnabled";
        public const string IsTelemetryEnabledKey = "IsTelemetryEnabled";
        public const string IsOpenTabsContextEnabledKey = "IsOpenTabsContextEnabled";
        public const string IgnoreCertificateErrorsKey = "IgnoreCertificateErrors";
        public const string IsDuoChatEnabledKey = "IsDuoChatEnabled";
        public const string IsSuggestionStreamingEnabledKey = "IsSuggestionStreamingEnabled";
        public const string GitLabUrlKey = "GitLabUrl";
        public const string LogLevelKey = "LogLevel";
        public const string AdditionalLanguagesKey = "AdditionalLanguages";

        private bool _batchSettingsUpdate = false;

        public ISettingsStorage Storage = null;
        public string GitlabAccessTokenValue = null;
        public bool IsCodeSuggestionsEnabledValue = true;
        public bool IsTelemetryEnabledValue = true;
        public bool IsOpenTabsContextEnabledValue = true;
        public bool IgnoreCertificateErrorsValue = true;
        public bool IsDuoChatEnabledValue = true;
        public bool IsSuggestionStreamingEnabledValue = true;
        public string GitLabUrlValue = string.Empty;
        public LogEventLevel LogLevelValue = LogEventLevel.Warning;
        public List<string> AdditionalLanguagesValue = new List<string>();

        public event EventHandler SettingsChangedEvent;

        private readonly ILogger _logger;

        public Settings(
            ISettingsStorage storage,
            ILogger logger)
        {
            _logger = logger;
            try
            {
                Storage = storage;
                Storage.Load(this);
            }
            catch
            {
                // Ignore any exceptions from initial loading.
                // The storage componet will already have logged
                // the loading error.
            }
        }

        /// <summary>
        /// Call before setting multiple settings. This
        /// prevents the SettingsChangedEvent from being
        /// triggered on each individual settings change.
        /// </summary>
        public void StartBatchSettingsUpdate()
        {
            _batchSettingsUpdate = true;
        }

        /// <summary>
        /// Called to stop batch mode and send a 
        /// SettingsChangedEvent.
        /// </summary>
        public void StopBatchSettingsUpdate()
        {
            _batchSettingsUpdate = false;
            Storage.Save(this);
            OnSettingsChanged(ApplicationName);
        }

        /// <summary>
        /// GitLab Access Token
        /// </summary>
        public string GitLabAccessToken
        {
            get { return GitlabAccessTokenValue; }
            set
            {
                GitlabAccessTokenValue = value;
                if (!_batchSettingsUpdate)
                {
                    Storage.Save(this);
                    OnSettingsChanged(GitLabAccessTokenKey);
                }
            }
        }

        /// <summary>
        /// Is code suggestions enabled
        /// </summary>
        public bool IsCodeSuggestionsEnabled
        {
            get { return IsCodeSuggestionsEnabledValue; }
            set
            {
                IsCodeSuggestionsEnabledValue = value;
                if (!_batchSettingsUpdate)
                {
                    Storage.Save(this);
                    OnSettingsChanged(IsCodeSuggestionsEnabledKey);
                }
            }
        }

        public bool IsTelemetryEnabled
        {
            get { return IsTelemetryEnabledValue; }
            set
            {
                IsTelemetryEnabledValue = value;
                if (!_batchSettingsUpdate)
                {
                    Storage.Save(this);
                    OnSettingsChanged(IsTelemetryEnabledKey);
                }
            }
        }

        public bool IsOpenTabsContextEnabled
        {
            get { return IsOpenTabsContextEnabledValue; }
            set
            {
                IsOpenTabsContextEnabledValue = value;
                if (!_batchSettingsUpdate)
                {
                    Storage.Save(this);
                    OnSettingsChanged(IsOpenTabsContextEnabledKey);
                }
            }
        }

        public bool IgnoreCertificateErrors
        {
            get { return IgnoreCertificateErrorsValue; }
            set
            {
                IgnoreCertificateErrorsValue = value;
                if (!_batchSettingsUpdate)
                {
                    Storage.Save(this);
                    OnSettingsChanged(IgnoreCertificateErrorsKey);
                }
            }
        }

        public bool IsDuoChatEnabled
        {
            get { return IsDuoChatEnabledValue; }
            set
            {
                IsDuoChatEnabledValue = value;
                if (!_batchSettingsUpdate)
                {
                    Storage.Save(this);
                    OnSettingsChanged(IsDuoChatEnabledKey);
                }
            }
        }

        public bool IsSuggestionStreamingEnabled
        {
            get { return IsSuggestionStreamingEnabledValue; }
            set
            {
                IsSuggestionStreamingEnabledValue = value;
                if (!_batchSettingsUpdate)
                {
                    Storage.Save(this);
                    OnSettingsChanged(IsSuggestionStreamingEnabledKey);
                }
            }
        }

        /// <summary>
        /// GitLab URL
        /// </summary>
        public string GitLabUrl
        {
            get { return GitLabUrlValue; }
            set
            {
                GitLabUrlValue = value;
                if (!_batchSettingsUpdate)
                {
                    Storage.Save(this);
                    OnSettingsChanged(GitLabUrlKey);
                }
            }
        }

        /// <summary>
        /// Logging level. Defualts to Warning
        /// </summary>
        public LogEventLevel LogLevel
        {
            get { return LogLevelValue; }
            set
            {
                LogLevelValue = value;
                if (!_batchSettingsUpdate)
                {
                    Storage.Save(this);
                    OnSettingsChanged(LogLevelKey);
                }
            }
        }

        public List<string> AdditionalLanguages => AdditionalLanguagesValue;

        public void SetAdditionalLanguages(string languagesString)
        {
            // split all languages ids by comma, remove any trailing spaces, deduplicate and warn about any non-alphanumeric ids and filter them
            var languages = languagesString.Split(',')
                .Select(l => l.Trim())
                .Where(l => !string.IsNullOrWhiteSpace(l))
                .Distinct()
                .ToList();

            var invalidLanguages = languages.Where(l => !l.All(char.IsLetterOrDigit)).ToList();
            if (invalidLanguages.Any())
            {
                _logger.Warning("The following language IDs contain non-alphanumeric characters and will be ignored: {InvalidLanguages}", string.Join(", ", invalidLanguages));
                languages = languages.Except(invalidLanguages).ToList();
            }

            AdditionalLanguagesValue = languages;
            
            if (!_batchSettingsUpdate)
            {
                Storage.Save(this);
                OnSettingsChanged(AdditionalLanguagesKey);
            }
        }

        /// <summary>
        /// Do we have a valid looking configuration
        /// </summary>
        public bool Configured =>
            !string.IsNullOrEmpty(GitLabUrl) &&
            !string.IsNullOrEmpty(GitLabAccessToken);

        /// <summary>
        /// Raise the SettingsChangedEvent
        /// </summary>
        private void OnSettingsChanged(string changedSettingKey)
        {
            // Don't trigger a SettingsChangedEvnet
            // if we are batch updating settings.
            if (_batchSettingsUpdate)
                return;

            var settingsChangedEvent = SettingsChangedEvent;
            if (settingsChangedEvent == null)
                return;

            settingsChangedEvent(this, new SettingsEventArgs(changedSettingKey));
        }
    }
}
