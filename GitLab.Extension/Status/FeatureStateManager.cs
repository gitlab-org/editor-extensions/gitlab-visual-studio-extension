using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using GitLab.Extension.Status.Models;

namespace GitLab.Extension.Status
{
    public interface IFeatureStateManager
    {
        void Update(FeatureState featureState);
        IObservable<FeatureState> GetObservable(Feature feature);
        FeatureState GetCurrentState(Feature feature);
        bool IsAvailable(Feature feature);
    }
    
    public class FeatureStateManager : IFeatureStateManager
    {
        private readonly Dictionary<Feature, BehaviorSubject<FeatureState>> _featureStateObservables = new Dictionary<Feature, BehaviorSubject<FeatureState>>();

        public FeatureStateManager()
        {
            foreach (Feature feature in Enum.GetValues(typeof(Feature)))
            {
                _featureStateObservables[feature] = new BehaviorSubject<FeatureState>(new FeatureState(feature, new List<FeatureStateCheck>()));
            }
        }

        public void Update(FeatureState featureState)
        {
            if (_featureStateObservables.TryGetValue(featureState.FeatureId, out var subject))
            {
                subject.OnNext(featureState);
            }
            else
            {
                throw new ArgumentException($"Feature {featureState.FeatureId} not found in the manager.");
            }
        }

        public IObservable<FeatureState> GetObservable(Feature feature)
        {
            if (_featureStateObservables.TryGetValue(feature, out var subject))
            {
                return subject.AsObservable().DistinctUntilChanged();
            }
            
            throw new ArgumentException($"Feature {feature} not found in the manager.");
        }
        
        public FeatureState GetCurrentState(Feature feature)
        {
            if (_featureStateObservables.TryGetValue(feature, out var subject))
            {
                return subject.Value;
            }
            
            throw new ArgumentException($"Feature {feature} not found in the manager.");
        }

        public bool IsAvailable(Feature feature)
        {
            return GetCurrentState(feature).Checks.Count == 0;
        }
    }
}
