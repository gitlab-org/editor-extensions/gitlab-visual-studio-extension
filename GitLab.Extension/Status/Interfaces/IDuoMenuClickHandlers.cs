using System;

namespace GitLab.Extension.Status.Interfaces
{
    public interface IDuoMenuClickHandlers
    {
        public Action ToggleCodeSuggestionsClickHandler { get; }
        public Action ToggleDuoChatClickHandler { get; }
        public Action OpenSettingsClickHandler { get; }
        public Action OpenDocumentationClickHandler { get; }
        public Action OpenForumClickHandler { get; }
    }
}