using System.Windows;

namespace GitLab.Extension.Status.Interfaces
{
    public interface IExtensionStatusControl
    {
        public IStatusBarState StatusBarState { get; }
        public UIElement UIElement { get; }
    }
}
