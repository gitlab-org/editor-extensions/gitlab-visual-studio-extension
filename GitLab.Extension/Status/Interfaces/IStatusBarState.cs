using System.ComponentModel;
using GitLab.Extension.Status.Models;

namespace GitLab.Extension.Status.Interfaces
{
    public interface IStatusBarState : INotifyPropertyChanged
    {
        RootState RootState { get; }
        string StatusMessage { get; }
        ChatState ChatState { get; }
        CodeSuggestionsState CodeSuggestionsState { get; }
        bool IsStatusBarDarkMode { get; set; }
        bool IsMenuDarkMode { get; set; }
        bool IsConfigured { set; }
        void UpdateFeatureState(FeatureState featureState);
    }
}
