namespace GitLab.Extension.Status.Interfaces
{
    public interface IStatusControlFactory
    {
        IExtensionStatusControl Create(IDuoMenuClickHandlers clickHandlers);
    }
}
