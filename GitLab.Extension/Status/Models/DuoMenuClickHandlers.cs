using System;
using GitLab.Extension.Status.Interfaces;

namespace GitLab.Extension.Status.Models
{
    public class DuoMenuClickHandlers : IDuoMenuClickHandlers
    {
        public Action ToggleCodeSuggestionsClickHandler { get; }
        public Action ToggleDuoChatClickHandler { get; }
        public Action OpenSettingsClickHandler { get; }
        public Action OpenDocumentationClickHandler { get; }
        public Action OpenForumClickHandler { get; }

        public DuoMenuClickHandlers(
            Action toggleCodeSuggestionsClickHandler,
            Action toggleDuoChatClickHandler,
            Action openSettingsClickHandler,
            Action openDocumentationClickHandler,
            Action openForumClickHandler)
        {
            ToggleCodeSuggestionsClickHandler = toggleCodeSuggestionsClickHandler;
            ToggleDuoChatClickHandler = toggleDuoChatClickHandler;
            OpenSettingsClickHandler = openSettingsClickHandler;
            OpenDocumentationClickHandler = openDocumentationClickHandler;
            OpenForumClickHandler = openForumClickHandler;
        }
    }
}