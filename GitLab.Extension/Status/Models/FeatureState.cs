using System;
using System.Collections.Generic;
using System.Linq;

namespace GitLab.Extension.Status.Models
{
    public class FeatureState : IEquatable<FeatureState>
    {
        public Feature FeatureId { get; }
        public List<FeatureStateCheck> Checks { get; }
        
        public FeatureState(Feature feature, List<FeatureStateCheck> checks)
        {
            FeatureId = feature;
            Checks = checks;
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as FeatureState);
        }

        public bool Equals(FeatureState other)
        {
            if (other is null)
                return false;

            if (ReferenceEquals(this, other))
                return true;

            return FeatureId.Equals(other.FeatureId) &&
                   Checks.SequenceEqual(other.Checks);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hash = 17;
                hash = hash * 23 + FeatureId.GetHashCode();
                foreach (var check in Checks)
                {
                    hash = hash * 23 + check.GetHashCode();
                }
                return hash;
            }
        }

        public static bool operator ==(FeatureState left, FeatureState right)
        {
            if (left is null)
                return right is null;
            return left.Equals(right);
        }

        public static bool operator !=(FeatureState left, FeatureState right)
        {
            return !(left == right);
        }
    }

    public enum Feature
    {
        AUTHENTICATION,
        CODE_SUGGESTIONS,
        CHAT,
        WORKFLOW
    }

    public class FeatureStateCheck : IEquatable<FeatureStateCheck>
    {
        public StateCheckId CheckId { get; }
        public string Details { get; }
        
        public FeatureStateCheck(StateCheckId checkId, string details)
        {
            CheckId = checkId;
            Details = details;
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as FeatureStateCheck);
        }

        public bool Equals(FeatureStateCheck other)
        {
            if (other is null)
                return false;

            if (ReferenceEquals(this, other))
                return true;

            return CheckId.Equals(other.CheckId) &&
                   Details == other.Details;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(CheckId, Details);
        }

        public static bool operator ==(FeatureStateCheck left, FeatureStateCheck right)
        {
            if (left is null)
                return right is null;
            return left.Equals(right);
        }

        public static bool operator !=(FeatureStateCheck left, FeatureStateCheck right)
        {
            return !(left == right);
        }
    }

    public class StateCheckId
    {
        private string _value;
        public string Value { get => _value; }
        
        public StateCheckId(string value)
        {
            _value = value;
        }
        
        public static bool operator ==(StateCheckId left, StateCheckId right)
        {
            if (ReferenceEquals(left, null))
            {
                return ReferenceEquals(right, null);
            }
            return left.Equals(right);
        }
        
        public static bool operator !=(StateCheckId left, StateCheckId right)
        {
            return !(left == right);
        }
        
        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }
            
            StateCheckId other = (StateCheckId)obj;
            return Value == other.Value;
        }
        
        public override int GetHashCode()
        {
            return Value.GetHashCode();
        }
        
        public static StateCheckId AUTHENTICATION_REQUIRED = new StateCheckId("authentication-required");
        public static StateCheckId INVALID_TOKEN = new StateCheckId("invalid-token");
        public static StateCheckId SUGGESTIONS_NO_LICENSE = new StateCheckId("code-suggestions-no-license");
        public static StateCheckId CHAT_NO_LICENSE = new StateCheckId("chat-no-license");
        public static StateCheckId DUO_DISABLED_FOR_PROJECT = new StateCheckId("duo-disabled-for-project");
        public static StateCheckId UNSUPPORTED_GITLAB_VERSION = new StateCheckId("code-suggestions-unsupported-gitlab-version");
        public static StateCheckId UNSUPPORTED_LANGUAGE = new StateCheckId("code-suggestions-document-unsupported-language");
        public static StateCheckId SUGGESTIONS_API_ERROR = new StateCheckId("code-suggestions-api-error");
        public static StateCheckId DISABLED_LANGUAGE = new StateCheckId("code-suggestions-document-disabled-language");
        public static StateCheckId SUGGESTIONS_DISABLED_BY_USER = new StateCheckId("code-suggestions-disabled-by-user");
        public static StateCheckId CHAT_DISABLED_BY_USER = new StateCheckId("chat-disabled-by-user");
    }
}
