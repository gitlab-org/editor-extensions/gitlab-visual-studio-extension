using System;
using System.Linq;
using GitLab.Extension.LanguageServer.Models;

namespace GitLab.Extension.Status.Models
{
    public static class FeatureStateExtensions
    {
        public static FeatureState Map(this FeatureStateNotification featureStateNotification)
        {
            if (Enum.TryParse<Feature>(featureStateNotification.FeatureId, true, out var feature))
            {
                return new FeatureState(
                    feature,
                    featureStateNotification.EngagedChecks.Select(check => new FeatureStateCheck(
                        new StateCheckId(check.CheckId),
                        check.Details
                    )).ToList()
                );
            }

            return null;
        }
    }
}
