using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using GitLab.Extension.Status.Interfaces;

namespace GitLab.Extension.Status.Models
{
    /// <summary>
    /// Provides a state mapping between Language Server state checks and the status bar user control.
    /// Implements INotifyPropertyChanged to allow dynamic display of state information.
    /// </summary>
    public class StatusBarState : IStatusBarState
    {
        private readonly Dictionary<Feature, FeatureState> _featureStates = new Dictionary<Feature, FeatureState>();
        private bool _isConfigured;
        
        private bool _statusBarDarkMode = true;
        private bool _menuDarkMode = true;

        public event PropertyChangedEventHandler PropertyChanged;

        // Defines state of the status bar button
        public RootState RootState => CalculateRootState();
        
        // Defines status message in the Duo Menu
        public string StatusMessage => CalculateStatusMessage();
        
        // Defines state of the Duo Chat button in the Duo Menu
        public ChatState ChatState => CalculateChatState();
        
        // Defines state of the Code Suggestions button in the Duo Menu
        public CodeSuggestionsState CodeSuggestionsState => CalculateCodeSuggestionsState();

        // Status bar can be of different color from all other elements thus it has its own state
        public bool IsStatusBarDarkMode
        {
            get => _statusBarDarkMode;
            set => SetProperty(ref _statusBarDarkMode, value);
        }

        // Used to display either dark or light icons
        public bool IsMenuDarkMode
        {
            get => _menuDarkMode;
            set => SetProperty(ref _menuDarkMode, value);
        }
        
        // Defines the state without any token configured for first time users
        public bool IsConfigured
        {
            set => SetProperty(ref _isConfigured, value);
        }

        public void UpdateFeatureState(FeatureState featureState)
        {
            _featureStates[featureState.FeatureId] = featureState;
            OnPropertyChanged(nameof(RootState));
            OnPropertyChanged(nameof(StatusMessage));
            OnPropertyChanged(nameof(ChatState));
            OnPropertyChanged(nameof(CodeSuggestionsState));
        }

        private RootState CalculateRootState()
        {
            if (!_isConfigured)
            {
                return RootState.NOT_CONFIGURED;
            }
            if (!_featureStates.ContainsKey(Feature.AUTHENTICATION) ||
                !_featureStates.ContainsKey(Feature.CODE_SUGGESTIONS) ||
                !_featureStates.ContainsKey(Feature.CHAT))
            {
                return RootState.LOADING;
            }

            if (_featureStates.Values.Any(fs => fs.Checks.Any(c => c.CheckId == StateCheckId.DUO_DISABLED_FOR_PROJECT)))
                return RootState.DISABLED;

            if (_featureStates.Values.Any(fs => fs.Checks.Any(c => c.CheckId == StateCheckId.AUTHENTICATION_REQUIRED)))
                return RootState.ERROR;

            if (_featureStates.Values.Any(fs => fs.Checks.Any(c => c.CheckId == StateCheckId.DISABLED_LANGUAGE || c.CheckId == StateCheckId.UNSUPPORTED_LANGUAGE)))
                return RootState.LANGUAGE;

            var chatEnabled = ChatState == ChatState.ENABLED;
            var codeSuggestionsEnabled = CodeSuggestionsState == CodeSuggestionsState.ENABLED;

            if (chatEnabled && codeSuggestionsEnabled) return RootState.ENABLED;
            if (!chatEnabled && !codeSuggestionsEnabled) return RootState.DISABLED;
            return RootState.PARTIAL_DISABLED;
        }

        private string CalculateStatusMessage()
        {
            if (!_isConfigured)
            {
                return "Setup required.";
            }
            
            var errorCheck = _featureStates.Values.SelectMany(fs => fs.Checks).FirstOrDefault(c => IsError(c.CheckId));
            if (errorCheck != null) return errorCheck.Details;

            var duoDisabledCheck = _featureStates.Values.SelectMany(fs => fs.Checks).FirstOrDefault(c => c.CheckId == StateCheckId.DUO_DISABLED_FOR_PROJECT);
            if (duoDisabledCheck != null) return duoDisabledCheck.Details;

            return "No problems detected.";
        }

        private ChatState CalculateChatState()
        {
            if (!_isConfigured)
            {
                return ChatState.DISABLED;
            }
            
            if (!_featureStates.TryGetValue(Feature.CHAT, out var state)) return ChatState.LOADING;

            var chatChecks = state.Checks;
            if (chatChecks.Any(c => c.CheckId == StateCheckId.CHAT_NO_LICENSE)) return ChatState.ERROR;
            if (chatChecks.Any(c => c.CheckId == StateCheckId.CHAT_DISABLED_BY_USER)) return ChatState.DISABLED;
            return ChatState.ENABLED;
        }

        private CodeSuggestionsState CalculateCodeSuggestionsState()
        {
            if (!_isConfigured)
            {
                return CodeSuggestionsState.DISABLED;
            }
            
            if (!_featureStates.TryGetValue(Feature.CODE_SUGGESTIONS, out var state)) return CodeSuggestionsState.LOADING;

            var codeSuggestionsChecks = state.Checks;
            if (codeSuggestionsChecks.Any(c => c.CheckId == StateCheckId.SUGGESTIONS_NO_LICENSE || c.CheckId == StateCheckId.SUGGESTIONS_API_ERROR))
                return CodeSuggestionsState.ERROR;
            if (codeSuggestionsChecks.Any(c => c.CheckId == StateCheckId.SUGGESTIONS_DISABLED_BY_USER))
                return CodeSuggestionsState.DISABLED;
            if (codeSuggestionsChecks.Any(c => c.CheckId == StateCheckId.DISABLED_LANGUAGE))
                return CodeSuggestionsState.LANGUAGE_DISABLED;
            if (codeSuggestionsChecks.Any(c => c.CheckId == StateCheckId.UNSUPPORTED_LANGUAGE))
                return CodeSuggestionsState.LANGUAGE_NOT_SUPPORTED;
            return CodeSuggestionsState.ENABLED;
        }

        private bool IsError(StateCheckId stateCheckId)
        {
            return stateCheckId == StateCheckId.INVALID_TOKEN ||
                   stateCheckId == StateCheckId.AUTHENTICATION_REQUIRED ||
                   stateCheckId == StateCheckId.SUGGESTIONS_API_ERROR ||
                   stateCheckId == StateCheckId.UNSUPPORTED_GITLAB_VERSION ||
                   stateCheckId == StateCheckId.SUGGESTIONS_NO_LICENSE ||
                   stateCheckId == StateCheckId.CHAT_NO_LICENSE;
        }

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private void SetProperty<T>(ref T field, T value, [CallerMemberName] string propertyName = null)
        {
            if (EqualityComparer<T>.Default.Equals(field, value)) return;
            field = value;
            OnPropertyChanged(propertyName);
        }
    }

    public enum RootState
    {
        NOT_CONFIGURED,
        LOADING,
        ENABLED,
        DISABLED,
        ERROR,
        PARTIAL_DISABLED,
        LANGUAGE
    }

    public enum ChatState
    {
        LOADING,
        ENABLED,
        DISABLED,
        ERROR
    }

    public enum CodeSuggestionsState
    {
        LOADING,
        ENABLED,
        DISABLED,
        ERROR,
        LANGUAGE_NOT_SUPPORTED,
        LANGUAGE_DISABLED
    }
}
