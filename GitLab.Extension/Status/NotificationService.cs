﻿using GitLab.Extension.InfoBar;
using GitLab.Extension.SettingsUtil;
using Microsoft.VisualStudio.Shell;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using GitLab.Extension.Status;
using GitLab.Extension.Status.Models;

namespace GitLab.Extension
{
    public interface INotificationService
    {
        public void Initialize();
    }
    public class NotificationService : INotificationService, IDisposable
    {
        public const string AUTHENTICATION_REQUIRED_MESSAGE = "Please setup your instance url and access token to start using GitLab Duo features.";
        public const string INVALID_ACCESS_TOKEN_MESSAGE = "Invalid access token or instance url. To continue using GitLab Duo features, update your settings.";
        public const string OPEN_SETTINGS_OPTION_LABEL = "Open Settings";

        private readonly ISettings _settings;
        private readonly IInfoBarFactory _infoBarFactory;
        private readonly IFeatureStateManager _featureStateManager;
        private IDisposable _tokenValidationNotificationSubscription;

        private IInfoBar _infoBar = null;

        public NotificationService(
            ISettings settings,
            IInfoBarFactory infoBarFactory,
            IFeatureStateManager featureStateManager)
        {
            _settings = settings;
            _infoBarFactory = infoBarFactory;
            _featureStateManager = featureStateManager;
        }
        
        public void Initialize()
        {
            _tokenValidationNotificationSubscription = _featureStateManager.GetObservable(Feature.AUTHENTICATION)
                .Subscribe(async f => await HandleAuthorizationFeature(f));

            _settings.SettingsChangedEvent += HandleSettingsUpdated;
        }

        private void HandleSettingsUpdated(
            object sender, 
            EventArgs e)
        {
            _infoBar?.Close();
        }

        private async Task HandleAuthorizationFeature(FeatureState featureState)
        {
            try
            {
                await ThreadHelper.JoinableTaskFactory.SwitchToMainThreadAsync();
                
                if (featureState.Checks.Count == 0)
                {
                    _infoBar?.Close();
                    return;
                }

                if (_infoBar != null)
                {
                    return;
                }
                
                var message = AUTHENTICATION_REQUIRED_MESSAGE;
                if (_settings.Configured)
                {
                    message = INVALID_ACCESS_TOKEN_MESSAGE;
                }

                _infoBar = _infoBarFactory.AttachInfoBarToMainWindow(
                    message,
                    new List<InfoBarAction>
                    {
                        new InfoBarAction(OPEN_SETTINGS_OPTION_LABEL, () =>
                        {
                            GitLabOptionsPackageActions.ShowOptionsPage();
                            _infoBar?.Close();
                        })
                    });

                _infoBar.Closed += (i, args) =>
                {
                    ThreadHelper.JoinableTaskFactory.Run(async delegate
                    {
                        await ThreadHelper.JoinableTaskFactory.SwitchToMainThreadAsync();
                        _infoBar = null;
                    });
                };
            }
            catch (Exception)
            {
                // ignored, do not crash Visual Studio on async void event handler
            }
        }

        public void Dispose()
        {
            _settings.SettingsChangedEvent -= HandleSettingsUpdated;
            _tokenValidationNotificationSubscription?.Dispose();
        }
    }
}
