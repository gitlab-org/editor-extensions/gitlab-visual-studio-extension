﻿using Microsoft.VisualStudio.PlatformUI;
using System;
using System.Windows;
using System.Windows.Controls;
using GitLab.Extension.SettingsUtil;
using GitLab.Extension.Status.Interfaces;
using GitLab.Extension.Status.Models;
using GitLab.Extension.Workspace;
using GitLab.Extension.Workspace.Model;
using Microsoft.VisualStudio.Shell;
using Serilog;

namespace GitLab.Extension.Status
{
    /// <summary>
    /// Display a GitLab status icon in the status bar and allow to open Duo Menu on click
    /// </summary>
    public class StatusBar : IDisposable
    {
        private readonly ISettings _settings;
        private readonly IFeatureStateManager _featureStateManager;
        private readonly IStatusControlFactory _statusControlFactory;
        private readonly IThemeManager _themeManager;
        private readonly ILogger _logger;
        
        private readonly IDisposable _authSubscription;
        private readonly IDisposable _codeSuggestionsSubscription;
        private readonly IDisposable _chatSubscription;
        private readonly IDisposable _themeSubscription;
        
        private IExtensionStatusControl _statusControl;
        
        public  StatusBar(
            ISettings settings,
            IStatusControlFactory statusControlFactory,
            IFeatureStateManager featureStateManager,
            IThemeManager themeManager,
            ILogger logger)
        {
            _settings = settings;
            _featureStateManager = featureStateManager;
            _statusControlFactory = statusControlFactory;
            _themeManager = themeManager;
            _logger = logger;
            
            _authSubscription = featureStateManager.GetObservable(Feature.AUTHENTICATION).Subscribe(OnFeatureStateUpdate);
            _codeSuggestionsSubscription = featureStateManager.GetObservable(Feature.CODE_SUGGESTIONS).Subscribe(OnFeatureStateUpdate);
            _chatSubscription = featureStateManager.GetObservable(Feature.CHAT).Subscribe(OnFeatureStateUpdate);
            _themeSubscription = themeManager.ThemeUpdates.Subscribe(OnThemeUpdate);
        }
        
        public void InitializeDisplay(AsyncPackage package)
        {
            try
            {
                _logger.Debug($"{nameof(InitializeDisplay)} called");
                ThreadHelper.ThrowIfNotOnUIThread();
                
                _statusControl = _statusControlFactory.Create(CreateClicksHandler(package));
                _statusControl.StatusBarState.IsConfigured = _settings.Configured;
                
                // Theme update does not trigger on the Visual Studio start up, so we do it manually
                _themeManager.UpdateTheme();

                // In case Language Server starts before status bar is initialized we need to manually check the state
                OnFeatureStateUpdate(_featureStateManager.GetCurrentState(Feature.AUTHENTICATION));
                OnFeatureStateUpdate(_featureStateManager.GetCurrentState(Feature.CODE_SUGGESTIONS));
                OnFeatureStateUpdate(_featureStateManager.GetCurrentState(Feature.CHAT));
                
                var app = Application.Current;
                var statusBarPanel = app.MainWindow.FindDescendant<DockPanel>(p => p.Name == "StatusBarPanel");

                DockPanel.SetDock(_statusControl.UIElement, Dock.Right);
                statusBarPanel.Children.Insert(2, _statusControl.UIElement);
            }
            catch (Exception ex)
            {
                _logger.Debug(ex, $"{nameof(InitializeDisplay)} exception");
            }
        }

        private void OnFeatureStateUpdate(FeatureState featureState)
        {
            try
            {
                if (_statusControl == null)
                {
                    // Language Server started up and sent the update before the status bar was initialized
                    return;
                }
                _statusControl.StatusBarState.UpdateFeatureState(featureState);
            }
            catch (Exception ex)
            {
                _logger.Error(ex, $"Error while handling feature state change for {featureState.FeatureId}");
            }
        }

        private void OnThemeUpdate(ThemeUpdate themeUpdate)
        {
            // There is no definition of "dark" or "light" theme for the Visual Studio,
            // there can also be sophisticated user themes installed, thus we try to guess
            // how bright is the background and use the correct icons
            _statusControl.StatusBarState.IsStatusBarDarkMode = themeUpdate.StatusBarColor.GetBrightness() < 0.5;
            _statusControl.StatusBarState.IsMenuDarkMode = themeUpdate.BackgroundColor.GetBrightness() < 0.5;
        }

        private DuoMenuClickHandlers CreateClicksHandler(AsyncPackage package)
        {
            return new DuoMenuClickHandlers(
                toggleCodeSuggestionsClickHandler: () =>
                {
                    _settings.IsCodeSuggestionsEnabled = !_settings.IsCodeSuggestionsEnabled;
                },
                toggleDuoChatClickHandler: () =>
                {
                    _settings.IsDuoChatEnabled = !_settings.IsDuoChatEnabled;
                },
                openSettingsClickHandler: () => package.ShowOptionPage(typeof(OptionPageGrid)),
                openDocumentationClickHandler: () => System.Diagnostics.Process.Start(new System.Diagnostics.ProcessStartInfo
                {
                    FileName = "https://docs.gitlab.com/ee/user/gitlab_duo/",
                    UseShellExecute = true
                }),
                openForumClickHandler: () => System.Diagnostics.Process.Start(new System.Diagnostics.ProcessStartInfo
                {
                    FileName = "https://forum.gitlab.com/c/gitlab-duo/52",
                    UseShellExecute = true
                }));
        }

        public void Dispose()
        {
            _authSubscription.Dispose();
            _codeSuggestionsSubscription.Dispose();
            _chatSubscription.Dispose();
            _themeSubscription.Dispose();
        }
    }
}
