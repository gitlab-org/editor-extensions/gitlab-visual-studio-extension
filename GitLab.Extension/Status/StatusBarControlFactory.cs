using GitLab.Extension.Status.Interfaces;

namespace GitLab.Extension.Status
{
    public class StatusBarControlFactory : IStatusControlFactory
    {
        public IExtensionStatusControl Create(IDuoMenuClickHandlers clickHandlers)
        {
            return new UserControls.StatusBarControl(clickHandlers);
        }
    }
}
