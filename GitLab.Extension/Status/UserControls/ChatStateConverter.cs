using System;
using System.Globalization;
using System.Windows.Data;
using GitLab.Extension.Status.Models;

namespace GitLab.Extension.Status.UserControls
{
    public class ChatStateConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is ChatState state)
            {
                switch (state)
                {
                    case ChatState.ENABLED:
                        return "Enabled";
                    case ChatState.DISABLED:
                        return "Disabled";
                    case ChatState.LOADING:
                        return "Loading";
                    case ChatState.ERROR:
                        return "Error";
                    default:
                        return "Unknown";
                }
            }
            return "Unknown";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}