using System;
using System.Globalization;
using System.Windows.Data;
using GitLab.Extension.Status.Models;

namespace GitLab.Extension.Status.UserControls
{
    public class CodeSuggestionsStateConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is CodeSuggestionsState state)
            {
                switch (state)
                {
                    case CodeSuggestionsState.ENABLED:
                        return "Enabled";
                    case CodeSuggestionsState.DISABLED:
                        return "Disabled";
                    case CodeSuggestionsState.ERROR:
                        return "Error";
                    case CodeSuggestionsState.LANGUAGE_DISABLED:
                        return "Language disabled by user";
                    case CodeSuggestionsState.LANGUAGE_NOT_SUPPORTED:
                        return "Unsupported language (override in settings)";
                    case CodeSuggestionsState.LOADING:
                        return "Loading";
                    default:
                        return "Unknown";
                }
            }
            return "Unknown";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}