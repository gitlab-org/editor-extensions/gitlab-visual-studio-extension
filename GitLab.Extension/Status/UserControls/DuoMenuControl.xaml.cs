﻿using System.Windows;

namespace GitLab.Extension.Status.UserControls
{
    public partial class DuoMenuControl
    {
        private StatusBarControl StatusBarControl => DataContext as StatusBarControl;
        public DuoMenuControl()
        {
            InitializeComponent();
        }
        
        private void OnSettingsClick(object sender, RoutedEventArgs e)
        {
            StatusBarControl?.DuoMenuClickHandlers.OpenSettingsClickHandler();
        }
        
        private void OnToggleCodeSuggestionsClick(object sender, RoutedEventArgs e)
        {
            StatusBarControl?.DuoMenuClickHandlers.ToggleCodeSuggestionsClickHandler();
        }
        
        private void OnToggleDuoChatClickHandler(object sender, RoutedEventArgs e)
        {
            StatusBarControl?.DuoMenuClickHandlers.ToggleDuoChatClickHandler();
        }
        
        private void OnOpenDocumentationClick(object sender, RoutedEventArgs e)
        {
            StatusBarControl?.DuoMenuClickHandlers.OpenDocumentationClickHandler();
        }

        private void OnOpenForumClick(object sender, RoutedEventArgs e)
        {
            StatusBarControl?.DuoMenuClickHandlers.OpenForumClickHandler();
        }
    }
}
