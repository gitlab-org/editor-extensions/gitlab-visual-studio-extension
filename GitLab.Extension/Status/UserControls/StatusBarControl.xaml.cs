﻿using Serilog;
using System.Windows;
using GitLab.Extension.Status.Interfaces;
using GitLab.Extension.Status.Models;

namespace GitLab.Extension.Status.UserControls
{
    /// <summary>
    /// Interaction logic for CodeSuggestionsStatusControl.xaml
    /// </summary>
    public partial class StatusBarControl : IExtensionStatusControl
    {
        public IStatusBarState StatusBarState { get; } = new StatusBarState();
        public UIElement UIElement => this;
        
        public IDuoMenuClickHandlers DuoMenuClickHandlers { get; }

        public StatusBarControl(IDuoMenuClickHandlers clickHandlers)
        {
            DuoMenuClickHandlers = clickHandlers;
            
            Log.Debug($"{nameof(StatusBarControl)} initializing component.");
            
            InitializeComponent();
            
            Log.Debug($"{nameof(StatusBarControl)} component initialized.");
        }

        private void StatusClickHandler(object sender, RoutedEventArgs e)
        {
            Log.Debug($"{nameof(StatusClickHandler)} clicked!");
            
            DuoMenu.IsOpen = true;
        }
    }
}
