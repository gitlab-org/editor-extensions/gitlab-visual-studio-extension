﻿using Autofac;

namespace GitLab.Extension.Utility.Git.DependencyInjection
{
    public static class ContainerBuilderExtensions
    {
        public static ContainerBuilder AddGitClient(
            this ContainerBuilder builder)
        {
            builder
                .RegisterType<RepositoryFactory>()
                .As<IRepositoryFactory>();

            return builder;
        }
    }
}