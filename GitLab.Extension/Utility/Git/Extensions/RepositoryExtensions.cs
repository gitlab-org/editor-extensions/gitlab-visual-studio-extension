﻿using System;
using GitLab.Extension.Utility.Git.Model;
using GitLab.Extension.Utility.Results;
using GitLab.Extension.Utility.Results.Errors;
using LibGit2Sharp;

namespace GitLab.Extension.Utility.Git
{
    public static class RepositoryExtensions
    {
        public static Result<RemoteInfo> GetRemoteInfo(
            this IRepository repository,
            string remoteName = "origin")
        {
            if (repository == null)
            {
                return new ExceptionalError(
                    new ArgumentException("The repository cannot be null.", nameof(repository)));
            }

            if (string.IsNullOrWhiteSpace(remoteName))
            {
                return new ExceptionalError(
                    new ArgumentException("The remote name must not be null or empty.", nameof(remoteName)));
            }
            
            try
            {
                var remote = repository.Network.Remotes[remoteName];
                if (remote == null)
                {
                    return new Error($"Remote '{remoteName}' not found");
                }
                    
                return RemoteInfo.ParseRemoteUrl(remote?.Url);
            }
            catch (Exception ex)
            {
                return new ExceptionalError(ex.Message, ex);
            }
        }
    }
}