using System;
using System.IO;
using System.Windows.Media.Imaging;

namespace GitLab.Extension.Utility
{
    public static class GitLabResources
    {
        public static BitmapImage GetImage(string fileName)
        {
            try
            {
                var bmi = new BitmapImage(new Uri($"pack://application:,,,/GitLab.Extension;component/Resources/{fileName}", UriKind.Absolute));
            
                return bmi;
            }
            catch (FileNotFoundException)
            {
                return null;
            }
        }
    }
}