using System;

namespace GitLab.Extension.Utility
{
    public interface IValueObservable<out T> : IObservable<T>
    {
        T Value { get; }
    }
}