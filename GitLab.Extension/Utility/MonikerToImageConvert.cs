using System;
using System.Drawing.Imaging;
using System.Globalization;
using System.Runtime.InteropServices;
using System.Windows.Data;
using System.Windows.Media.Imaging;
using Microsoft.VisualStudio.Imaging.Interop;
using Microsoft.VisualStudio.Shell;
using Microsoft.VisualStudio.Shell.Interop;
using ImageAttributes = Microsoft.VisualStudio.Imaging.Interop.ImageAttributes;

namespace GitLab.Extension.Utility
{
    public class MonikerToImageConvert :  IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is ImageMoniker moniker)
            {
                return MonikerToBitmapSource(moniker);
            }
            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
        
        public static BitmapSource MonikerToBitmapSource(ImageMoniker moniker)
        {
            // Get the Visual Studio Image Service
            var imageService = Package.GetGlobalService(typeof(SVsImageService)) as IVsImageService2;
            if (imageService == null) return null;

            // Prepare attributes
            var attrs = new ImageAttributes
            {
                Flags = (uint)_ImageAttributesFlags.IAF_RequiredFlags,
                ImageType = (uint)_UIImageType.IT_Bitmap,       // Request a bitmap
                Format = (uint)_UIDataFormat.DF_WPF,
                LogicalWidth = 16,
                LogicalHeight = 16,
                Dpi = 96,
                StructSize = Marshal.SizeOf(typeof(ImageAttributes)),
            };

            // Get the IVsUIObject from the moniker
            IVsUIObject uiObject = imageService.GetImage(moniker, attrs);
            if (uiObject == null) return null;

            // Extract the actual image data
            uiObject.get_Data(out object data);
            return data as BitmapSource;
        }
    }
}
