using System;
using System.IO;

namespace GitLab.Extension.Utility
{
    public class PathUtils
    {
        public static string GetRelativePath(string fromPath, string toPath)
        {
            var fromUri = new Uri(fromPath);
            var toUri = new Uri(toPath);

            if (fromUri.Scheme != toUri.Scheme)
            {
                return toPath;
            }

            var relativeUri = fromUri.MakeRelativeUri(toUri);
            var relativePath = Uri.UnescapeDataString(relativeUri.ToString());
            
            relativePath = relativePath.Substring(relativePath.IndexOf('/') + 1);

            if (Path.DirectorySeparatorChar == '\\' && relativePath.Contains("/"))
            {
                relativePath = relativePath.Replace('/', Path.DirectorySeparatorChar);
            }

            return relativePath;
        }
    }
}