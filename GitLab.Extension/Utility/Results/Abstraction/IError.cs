using System.Collections.Generic;

namespace GitLab.Extension.Utility.Results
{
    public interface IError : IReason
    {
        IReadOnlyCollection<IError> Reasons { get; }
    }
}