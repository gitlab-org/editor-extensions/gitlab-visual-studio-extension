using System.Collections.Generic;
using GitLab.Extension.LanguageServer.Models;
using Newtonsoft.Json;

namespace GitLab.Extension.LanguageServer.Models
{
    public class Serializer
    {
        public static JsonSerializer Default = JsonSerializer.CreateDefault(new JsonSerializerSettings
        {
            Converters = new List<JsonConverter> { new EitherConverter<CompletionItemList, List<CompletionItem>>() }
        });
    }
}