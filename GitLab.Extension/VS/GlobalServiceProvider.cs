﻿using Microsoft.VisualStudio.Shell;
using System;

namespace GitLab.Extension.VS
{
    public static class GlobalServiceProvider
    {
        public static object GetService(Type serviceType)
        {
            return Package.GetGlobalService(serviceType);
        }

        public static T GetService<T>() 
        {
            return (T) GetService(typeof(T));
        }
    }
}
