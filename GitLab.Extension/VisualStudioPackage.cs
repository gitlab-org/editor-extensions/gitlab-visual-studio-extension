﻿using EnvDTE80;
using Microsoft.VisualStudio;
using Microsoft.VisualStudio.Shell;
using Microsoft.VisualStudio.Shell.Events;
using Microsoft.VisualStudio.Shell.Interop;
using System;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Reactive.Disposables;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using Task = System.Threading.Tasks.Task;
using GitLab.Extension.SettingsUtil;
using Serilog;
using Autofac;
using GitLab.Extension.CodeSuggestions;
using GitLab.Extension.LanguageServer;
using GitLab.Extension.Command;
using GitLab.Extension.Status;
using GitLab.Extension.VS;
using GitLab.Extension.Webviews;
using GitLab.Extension.Webviews.DuoChat;
using GitLab.Extension.Workspace;
using Microsoft.VisualStudio.ComponentModelHost;
using Microsoft.VisualStudio.Language.Suggestions;

namespace GitLab.Extension
{
    /// <summary>
    /// Listen for solution open/closed events and perform any actions needed.
    /// </summary>
    /// <remarks>
    /// Handle extension initialization:
    /// 
    /// 1. Start logging and add output window
    /// 2. Display status bar icon
    /// 3. Start a language server for this solution
    /// </remarks>
    [PackageRegistration(UseManagedResourcesOnly = true, AllowsBackgroundLoading = true)]
    [Guid(PackageIds.GitLabPackageIdString)]
    [ProvideMenuResource("Menus.ctmenu", 1)]
    [SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1650:ElementDocumentationMustBeSpelledCorrectly", Justification = "pkgdef, VS and vsixmanifest are valid VS terms")]
    // This autoload doesn't happen when opening a solution file directly.
    [ProvideAutoLoad(VSConstants.UICONTEXT.SolutionOpening_string, PackageAutoLoadFlags.BackgroundLoad)]
    // This autoload occurs after Visual Studio has completely started. It would be nice
    // if the autoload would occur earlier.
    [ProvideAutoLoad(VSConstants.UICONTEXT.ShellInitialized_string, PackageAutoLoadFlags.BackgroundLoad)]
    [ProvideToolWindow(
        typeof(DuoChatToolWindow),
        Style = VsDockStyle.Tabbed,
        Window = "3ae79031-e1bc-11d0-8f78-00a0c9110057",
        Transient = true)]
    public sealed class VisualStudioPackage : AsyncPackage, IDisposable
    {
        private readonly ILifetimeScope _packageScope;
        private ISettings _settings;
        private IWorkspaceLsClientProvider _lsClientProvider;
        private ILsProcessManager _lsProcessManager;
        private ICommandInitializer _commandInitializer;
        private IWebviewControllerInitializer _webviewControllerInitializer;
        private StatusBar _statusBar;
        private INotificationService _notificationService;

        private readonly CompositeDisposable _subscriptions =
            new CompositeDisposable();

        private readonly Lazy<IWorkspaceEventHandler> _workspaceEventHandler =
            new Lazy<IWorkspaceEventHandler>(() =>
                DependencyInjection.Instance.Scope.Resolve<IWorkspaceEventHandler>());

        private bool _initialized = false;

        /// <summary>
        /// Initializes a new instance of the <see cref="VisualStudioPackage"/> class.
        /// </summary>
        public VisualStudioPackage()
        {
            // Inside this method you can place any initialization code that does not require
            // any Visual Studio service because at this point the package object is created but
            // not sited yet inside Visual Studio environment. The place to do all the other
            // initialization is the Initialize method.

            _packageScope = DependencyInjection.Instance.Scope.BeginLifetimeScope(builder =>
            {
                builder.Register(_ => this).As<AsyncPackage>().SingleInstance();
                builder.RegisterWebviewControllers();
            });
        }

        public void Dispose()
        {
            _packageScope.Dispose();
        }

        protected override async Task InitializeAsync(CancellationToken cancellationToken, IProgress<ServiceProgressData> progress)
        {
            await ThreadHelper.JoinableTaskFactory.SwitchToMainThreadAsync();
            
            // Configure logging. This will create the output window in Visual Studio
            Logging.ConfigureLogging();
            
            _lsClientProvider = _packageScope.Resolve<IWorkspaceLsClientProvider>();
            _lsProcessManager = _packageScope.Resolve<ILsProcessManager>();
            _settings = _packageScope.Resolve<ISettings>();
            _statusBar = _packageScope.Resolve<StatusBar>();
            _notificationService = _packageScope.Resolve<INotificationService>();
            _commandInitializer = _packageScope.Resolve<ICommandInitializer>();
            _webviewControllerInitializer = _packageScope.Resolve<IWebviewControllerInitializer>();
            
            // Run this code in the background to avoid VS notifying the user a background
            // task is still running. Also makes VS start faster than if it was inline.
            var task = new Func<Task>(async () => {
                if (_initialized)
                    return;

                // Since this package might not be initialized until after a solution has finished loading,
                // we need to check if a solution has already been loaded and then handle it.
                bool isSolutionLoaded = await IsSolutionLoadedAsync();

                if (isSolutionLoaded)
                {
                    HandleAfterOpenSolution();
                }

                // Listen for subsequent solution events
                SolutionEvents.OnAfterOpenSolution += HandleAfterOpenSolution;
                SolutionEvents.OnBeforeCloseSolution += HandleBeforeCloseSolution;

                _initialized = true;
            });
            
            InitializeSuggestionServiceTelemetryAdapter();
            
            await _commandInitializer.InitializeAsync(this);
            await task.Invoke();
        }

        private static void InitializeSuggestionServiceTelemetryAdapter()
        {
            Log.Debug("Initializing suggestion service telemetry");

            if (!TryGetSuggestionService(out var suggestionService))
            {
                Log.Warning(
                    "Could not initialize suggestion service telemetry. SuggestionServiceBase could not be resolved.");
                return;
            }

            DependencyInjection
                .Instance.Scope.Resolve<SuggestionServiceListener>()
                .Initialize(suggestionService);
        }

        private string GetSolutionName(string solutionFullName)
        {
            var solutionName = solutionFullName;

            if (solutionName.ToLower().EndsWith(".sln"))
                solutionName = Path.GetFileNameWithoutExtension(solutionName);

            return solutionName;
        }

        private void HandleBeforeCloseSolution(object sender, EventArgs e)
        {
            if (!_settings.Configured)
                return;

            Log.Debug($"{nameof(VisualStudioPackage)}.{nameof(HandleBeforeCloseSolution)}");

            var l = new Func<Task>(async () =>
            {
                await JoinableTaskFactory.SwitchToMainThreadAsync();

                var dte2 = (DTE2)GetGlobalService(typeof(SDTE));
                if (dte2 == null)
                    return;

                var solutionName = GetSolutionName(dte2.Solution.FullName);
                var solutionPath = Path.GetDirectoryName(dte2.Solution.FileName);

                await _lsClientProvider.DisposeClientAsync(new WorkspaceId(solutionName, solutionPath));
                await _lsProcessManager.StopLanguageServerAsync(solutionPath);
                
                _workspaceEventHandler.Value.OnWorkspaceClose(
                    new WorkspaceId(solutionName, solutionPath));
            });

            _ = l.Invoke();
        }

        private async Task<bool> IsSolutionLoadedAsync()
        {
            await JoinableTaskFactory.SwitchToMainThreadAsync();
            var solService = await GetServiceAsync(typeof(SVsSolution)) as IVsSolution;

            if (solService == null)
                return false;

            ErrorHandler.ThrowOnFailure(solService.GetProperty((int)__VSPROPID.VSPROPID_IsSolutionOpen, out object value));

            return value is bool isSolOpen && isSolOpen;
        }

        private void HandleAfterOpenSolution(object sender = null, EventArgs e = null)
        {
            // Handle the open solution and try to do as much work
            // on a background thread as possible

            Log.Debug($"{nameof(VisualStudioPackage)}.{nameof(HandleAfterOpenSolution)}");

            var l = new Func<Task>(async () =>
            {
                await JoinableTaskFactory.SwitchToMainThreadAsync();
                
                Log.Debug($"{nameof(VisualStudioPackage)}.{nameof(HandleAfterOpenSolution)}");

                // Display status bar icon
                _statusBar.InitializeDisplay(this);
                
                // Initialize notifications
                _notificationService.Initialize();

                // Start the language server, but only if we are configured
                if (!_settings.Configured)
                {
                    Log.Warning("This extension will not function until it's configured: Tools -> Options -> GitLab");
                    return;
                }

                var dte2 = (DTE2)GetGlobalService(typeof(SDTE));
                if (dte2 == null)
                    return;

                var solutionName = GetSolutionName(dte2.Solution.FullName);
                var solutionPath = Path.GetDirectoryName(dte2.Solution.FileName);
                
                _workspaceEventHandler.Value.OnWorkspaceOpen(
                    new WorkspaceId(solutionName, solutionPath));
                
                await _webviewControllerInitializer.InitializeAsync();
            });

            _ = l.Invoke();
        }
        
        private static bool TryGetSuggestionService(out SuggestionServiceBase suggestionService)
        {
            suggestionService = null;
                
            try
            {
                suggestionService = (GlobalServiceProvider.GetService(typeof(SComponentModel)) as IComponentModel)
                    ?.GetService<SuggestionServiceBase>();
                    
                return suggestionService != null;
            } catch (Exception ex)
            {
                suggestionService = null;
                return false;
            }
        }

        protected override void Dispose(
            bool disposing)
        {
            base.Dispose(disposing);
            _subscriptions.Dispose();
        }
    }
}
