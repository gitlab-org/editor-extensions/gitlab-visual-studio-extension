using Autofac;
using GitLab.Extension.Webviews.DuoChat;
using GitLab.Extension.Webviews.DuoChat.Abstraction;
using GitLab.Extension.Webviews.Themes;

namespace GitLab.Extension.Webviews
{
    public static class DependencyInjection
    {
        public static ContainerBuilder RegisterWebviewControllers(this ContainerBuilder builder)
        {
            builder.RegisterType<WebviewControllerInitializer>().As<IWebviewControllerInitializer>();
            
            // register controllers for each of the web views
            builder.RegisterType<DuoChatController>()
                .AsSelf()
                .As<IWebviewController<DuoChatToolWindow>>()
                .As<IWebviewController>().SingleInstance();
            
            // register themes
            builder.RegisterType<VisualStudioThemeProvider>().As<IThemeProvider>().SingleInstance();
            builder.RegisterType<ThemeManager>().As<IThemeManager>().SingleInstance();
            
            // register duo chat services
            builder.RegisterType<DuoChatClient>().As<IDuoChatClient>();
            
            return builder;
        }
    }
}