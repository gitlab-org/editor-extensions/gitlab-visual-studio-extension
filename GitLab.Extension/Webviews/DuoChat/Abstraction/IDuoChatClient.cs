using System.Threading.Tasks;

namespace GitLab.Extension.Webviews.DuoChat.Abstraction
{
    public interface IDuoChatClient
    {
        Task SendNewPromptAsync(string prompt);
    }
}
