using System;
using System.ComponentModel.Design;
using System.Threading.Tasks;
using GitLab.Extension.Command;
using GitLab.Extension.Status;
using GitLab.Extension.Status.Models;
using GitLab.Extension.Webviews.DuoChat.Abstraction;
using GitLab.Extension.Webviews.DuoChat.Utils;
using Microsoft.VisualStudio.Shell;

namespace GitLab.Extension.Webviews.DuoChat.Commands
{
    public abstract class BaseSelectedCodeCommand : ICommand
    {
        private readonly IFeatureStateManager _featureStateManager;

        protected BaseSelectedCodeCommand(
            IFeatureStateManager featureStateManager)
        {
            _featureStateManager = featureStateManager;
        }

        public abstract CommandID CommandID { get; }

        public async Task InitializeAsync(IServiceProvider serviceProvider)
        {
            await ThreadHelper.JoinableTaskFactory.SwitchToMainThreadAsync();

            var oleMenuItem = new OleMenuCommand(
                (obj, args) => ThreadHelper.JoinableTaskFactory.Run(ExecuteAsync),
                CommandID);

            oleMenuItem.BeforeQueryStatus += BeforeQueryStatus;

            var commandService = serviceProvider.GetService(typeof(IMenuCommandService)) as OleMenuCommandService;

            commandService?.AddCommand(oleMenuItem);
        }

        protected abstract Task ExecuteAsync();

        private void BeforeQueryStatus(object sender, EventArgs e)
        {
            if (sender is OleMenuCommand command)
            {
                command.Visible = _featureStateManager.IsAvailable(Feature.CHAT);
                command.Enabled = SelectionUtilities.EditorHasSelection() && _featureStateManager.IsAvailable(Feature.CHAT);
            }
        }
    }
}
