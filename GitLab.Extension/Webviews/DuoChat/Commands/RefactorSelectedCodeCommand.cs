using System.ComponentModel.Design;
using System.Threading.Tasks;
using GitLab.Extension.Status;
using GitLab.Extension.Webviews.DuoChat.Abstraction;

namespace GitLab.Extension.Webviews.DuoChat.Commands
{
    public class RefactorSelectedCodeCommand : BaseSelectedCodeCommand
    {
        public override CommandID CommandID { get; } = new CommandID(
            PackageIds.GitLabPackageCmdSet,
            PackageIds.CmdDuoChatRefactorCode);

        private readonly DuoChatController _duoChatController;
        private readonly IDuoChatClient _duoChatClient;

        public RefactorSelectedCodeCommand(
            DuoChatController duoChatController,
            IDuoChatClient duoChatClient,
            IFeatureStateManager featureStateManager
        ) : base(featureStateManager)
        {
            _duoChatController = duoChatController;
            _duoChatClient = duoChatClient;
        }

        protected override async Task ExecuteAsync()
        {
            await _duoChatController.ShowAsync();
            await _duoChatClient.SendNewPromptAsync(SlashCommands.Refactor);
        }
    }
}