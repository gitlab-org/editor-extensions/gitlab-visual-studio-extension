using System.ComponentModel.Design;
using System.Threading.Tasks;
using GitLab.Extension.Status;
using GitLab.Extension.Webviews.DuoChat.Abstraction;

namespace GitLab.Extension.Webviews.DuoChat.Commands
{
    public class TestSelectedCodeCommand : BaseSelectedCodeCommand
    {
        public override CommandID CommandID { get; } = new CommandID(
            PackageIds.GitLabPackageCmdSet,
            PackageIds.CmdDuoChatTestCode);

        private readonly DuoChatController _duoChatController;
        private readonly IDuoChatClient _duoChatClient;

        public TestSelectedCodeCommand(
            DuoChatController duoChatController,
            IDuoChatClient duoChatClient,
            IFeatureStateManager featureStateManager
        ) : base(featureStateManager)
        {
            _duoChatController = duoChatController;
            _duoChatClient = duoChatClient;
        }

        protected override async Task ExecuteAsync()
        {
            await _duoChatController.ShowAsync();
            await _duoChatClient.SendNewPromptAsync(SlashCommands.GenerateTests);
        }
    }
}