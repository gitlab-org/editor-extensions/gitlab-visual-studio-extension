namespace GitLab.Extension.Webviews.DuoChat
{
    public class Constants
    {
        public const string TOOL_WINDOW_GUID = "09726FE2-7067-469C-AB83-A771A5F732ED";
        public const string WINDOW_CAPTION = "GitLab Duo Chat";
        public const string WEBVIEW_FOLDER_NAME = "GitLabDuoChat";
    }
}