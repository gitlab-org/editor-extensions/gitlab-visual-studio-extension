using System.Threading.Tasks;
using GitLab.Extension.LanguageServer.Plugin;
using GitLab.Extension.Webviews.DuoChat.Abstraction;
using GitLab.Extension.Webviews.DuoChat.Model;

namespace GitLab.Extension.Webviews.DuoChat
{
    public class DuoChatClient : IDuoChatClient
    {
        private readonly IPluginMessagePublisher _notificationProducer;

        public DuoChatClient(
            IPluginMessagePublisherProvider pluginMessagePublisherProvider)
        {
            _notificationProducer = pluginMessagePublisherProvider.Get("duo-chat");
        }

        public async Task SendNewPromptAsync(
            string prompt)
        {
            await _notificationProducer.NotifyAsync("newPrompt", new
            {
                prompt = prompt,
                fileContext = await GitLabChatFileContext.CreateFromCurrentContextAsync()
            });
        }
    }
}
