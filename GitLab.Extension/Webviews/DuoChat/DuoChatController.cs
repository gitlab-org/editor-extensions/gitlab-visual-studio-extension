using System;
using System.Threading.Tasks;
using GitLab.Extension.Status;
using GitLab.Extension.Status.Models;
using GitLab.Extension.Workspace;
using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.Shell;

namespace GitLab.Extension.Webviews.DuoChat
{
    public class DuoChatController : WebviewController<DuoChatToolWindow>, IDisposable
    {
        private readonly IFeatureStateManager _featureStateManager;
        private readonly IDisposable _featureStateSubscription;

        public DuoChatController(
            AsyncPackage package,
            IWorkspaceLsClientProvider lsClientProvider,
            IFeatureStateManager featureStateManager,
            ILogger<WebviewController<DuoChatToolWindow>> logger)
            : base(package, lsClientProvider, logger)
        {
            _featureStateManager = featureStateManager;
            _featureStateSubscription = featureStateManager.GetObservable(Feature.CHAT).Subscribe(async s => await OnFeatureStateUpdate(s));
        }

        protected override string WebviewId => "duo-chat";

        public override bool IsEnabled => _featureStateManager.IsAvailable(Feature.CHAT);
        
        public void Dispose()
        {
            _featureStateSubscription?.Dispose();
        }
        
        private async Task OnFeatureStateUpdate(FeatureState featureState)
        {
            // if we don't check this there is a risk of running the code with no solution open
            if (!IsInitialized)
            {
                return;
            }
            try
            {
                if (!_featureStateManager.IsAvailable(Feature.CHAT))
                {
                    await ThreadHelper.JoinableTaskFactory.SwitchToMainThreadAsync();
                    Hide();
                }
                else
                {
                    await ShowAsync();
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"Error while handling feature state change for {featureState.FeatureId}");
            }
        }
    }
}