using System.Threading.Tasks;
using GitLab.Extension.LanguageServer.Plugin.Attributes;
using GitLab.Extension.Webviews.DuoChat.Model;

namespace GitLab.Extension.Webviews.DuoChat
{
    [PluginController("duo-chat")]
    public class DuoChatPluginController
    {        
        [PluginRequest("getCurrentFileContext")]
        public Task<GitLabChatFileContext> GetCurrentFileContextAsync()
        {
            return GitLabChatFileContext.CreateFromCurrentContextAsync();
        }
    }
}
