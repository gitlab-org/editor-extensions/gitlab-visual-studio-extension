using System.Runtime.InteropServices;
using Microsoft.VisualStudio.Shell;

namespace GitLab.Extension.Webviews.DuoChat
{
    [Guid(Constants.TOOL_WINDOW_GUID)]
    public class DuoChatToolWindow : ToolWindowPane
    {
        public DuoChatToolWindow() : base(null)
        {
            Caption = Constants.WINDOW_CAPTION;
            Content = new DuoChatToolWindowControl();
        }
    }
}