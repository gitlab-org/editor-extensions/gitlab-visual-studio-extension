using System;
using System.Diagnostics;
using System.Threading.Tasks;
using Microsoft.IO;
using Microsoft.Web.WebView2.Core;
using Serilog;

namespace GitLab.Extension.Webviews.DuoChat
{
    public partial class DuoChatToolWindowControl : IWebviewToolWindowControl
    {
        private string _userDataFolder;
        private bool _isViewInitialized;
        
        public bool IsViewInitialized => _isViewInitialized;
        public DuoChatToolWindowControl()
        {
            InitializeComponent();
        }

        public async Task InitializeAsync(Uri uri)
        { 
            // WebView2 runtime always runs as a regular user
            // ensure we create a folder in a place where a regular user can access it
            _userDataFolder = Path.Combine(Path.GetTempPath(), Constants.WEBVIEW_FOLDER_NAME);
            
            Log.Debug("Initializing WebView2 control with data folder: {webViewDataFolder}", _userDataFolder);
            
            var environment = await CoreWebView2Environment.CreateAsync(null, _userDataFolder);

            // this will make sure all browser cache gets cleared
            var options = environment.CreateCoreWebView2ControllerOptions();
            options.IsInPrivateModeEnabled = true;
            
            await webView.EnsureCoreWebView2Async(environment, options);
            
            // source should be always specified after the EnsureCoreWebView2Async
            // see https://github.com/MicrosoftEdge/WebView2Feedback/issues/340#issuecomment-659865180
            webView.Source = uri;
            Log.Debug("WebView2 source set to {WebViewSourceUri}", uri);
            
            webView.CoreWebView2.NewWindowRequested += WebView_NewWindowRequested;
            webView.CoreWebView2.NavigationStarting += WebView_NavigationStarting;
            
            _isViewInitialized = true;
        }

        public void Reload()
        {
            webView.Reload();
        }
        
        // Prevent the new windows from opening from the webview.
        private void WebView_NewWindowRequested(object sender, CoreWebView2NewWindowRequestedEventArgs e)
        {
            e.Handled = true;
            var uri = e.Uri;
            Process.Start(new ProcessStartInfo(uri) { UseShellExecute = true });
        }
        
        // Prevent navigating to the external links and open them in the browser instead.
        private void WebView_NavigationStarting(object sender, CoreWebView2NavigationStartingEventArgs e)
        {
            var uri = new Uri(e.Uri);
            if (!uri.IsLoopback && !uri.IsFile)
            {
                e.Cancel = true;
                Process.Start(new ProcessStartInfo(e.Uri) { UseShellExecute = true });
            }
        }
    }
}