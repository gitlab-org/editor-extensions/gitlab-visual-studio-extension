namespace GitLab.Extension.Webviews.DuoChat.Abstraction
{
    public class DuoChatStatus
    {
        public bool IsActive { get; }

        public DuoChatStatus(bool isActive)
        {
            IsActive = isActive;
        }
    }
}