using System;
using System.Threading.Tasks;
using EnvDTE;
using EnvDTE80;
using Microsoft.VisualStudio.Shell;
using Newtonsoft.Json;

namespace GitLab.Extension.Webviews.DuoChat.Model
{
    public class GitLabChatFileContext
    {
        [JsonProperty("fileName")] public string FileName { get; }

        [JsonProperty("selectedText")] public string SelectedText { get; }

        [JsonProperty("contentAboveCursor")] public string ContentAboveCursor { get; }

        [JsonProperty("contentBelowCursor")] public string ContentBelowCursor { get; }

        public GitLabChatFileContext(
            string fileName,
            string selectedText,
            string contentAboveCursor,
            string contentBelowCursor)
        {
            FileName = fileName;
            SelectedText = selectedText;
            ContentAboveCursor = contentAboveCursor;
            ContentBelowCursor = contentBelowCursor;
        }

        public static async Task<GitLabChatFileContext> CreateFromCurrentContextAsync()
        {
            await ThreadHelper.JoinableTaskFactory.SwitchToMainThreadAsync();

            if (!(Package.GetGlobalService(typeof(DTE)) is DTE2 dte))
            {
                throw new InvalidOperationException("Unable to get DTE service.");
            }

            var activeDocument = dte.ActiveDocument;
            if (activeDocument == null)
            {
                throw new InvalidOperationException("No active document.");
            }

            if (!(activeDocument.Selection is TextSelection selection))
            {
                throw new InvalidOperationException("Unable to get text selection.");
            }

            var fileName = activeDocument.FullName;
            var selectedText = selection.Text;

            if (string.IsNullOrEmpty(selectedText))
            {
                var caretPosition = selection.ActivePoint.CreateEditPoint();
                // Create an edit point at the start of the document and at the caret position
                var startPoint = caretPosition.CreateEditPoint();
                startPoint.StartOfDocument(); // Move to the start of the document

                // Get content above the cursor from the start of the document to the caret
                var contentAboveCursor = startPoint.GetText(caretPosition);

                // For content below cursor, create another point at the caret position
                var endPoint = caretPosition.CreateEditPoint();
                endPoint.EndOfDocument(); // Move to the end of the document

                // Get content below the cursor from the caret to the end of the document
                var contentBelowCursor = caretPosition.GetText(endPoint);

                return new GitLabChatFileContext(fileName, selectedText: "", // No selection, so this remains empty
                    contentAboveCursor, contentBelowCursor);
            }
            else
            {
                // When text is selected, get the text around the selection
                var startPoint = selection.TopPoint.CreateEditPoint();
                startPoint.StartOfDocument(); // Move to the start of the document

                // Get content above the selection
                var contentAboveCursor = startPoint.GetText(selection.TopPoint);

                var endPoint = selection.BottomPoint.CreateEditPoint();
                endPoint.EndOfDocument(); // Move to the end of the document

                // Get content below the selection
                var contentBelowCursor = selection
                    .BottomPoint.CreateEditPoint()
                    .GetText(endPoint);

                return new GitLabChatFileContext(fileName, selectedText, contentAboveCursor, contentBelowCursor);
            }
        }
    }
}
