﻿namespace GitLab.Extension.Webviews.DuoChat
{
    public static class SlashCommands
    {
        public const string ExplainCode = "/explain";    
        public const string FixCode = "/fix";
        public const string GenerateTests = "/tests";
        public const string Refactor = "/refactor";
    }
}
