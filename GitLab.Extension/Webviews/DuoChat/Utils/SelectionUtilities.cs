﻿using Microsoft.VisualStudio.Shell;
using Microsoft.VisualStudio.TextManager.Interop;

namespace GitLab.Extension.Webviews.DuoChat.Utils
{
    public static class SelectionUtilities
    {
        public static bool EditorHasSelection()
        {
            ThreadHelper.ThrowIfNotOnUIThread();

            // Get the IVsTextManager service
            var textManager = (IVsTextManager)ServiceProvider.GlobalProvider.GetService(typeof(SVsTextManager));
            if (textManager == null)
                return false;

            // Get the active view
            IVsTextView activeView;
            var hr = textManager.GetActiveView(1, null, out activeView);
            if (hr != Microsoft.VisualStudio.VSConstants.S_OK || activeView == null)
                return false;

            // Get the selection positions
            int startLine, startIndex, endLine, endIndex;
            hr = activeView.GetSelection(out startLine, out startIndex, out endLine, out endIndex);
            if (hr != Microsoft.VisualStudio.VSConstants.S_OK)
                return false;

            return (startLine != endLine) || (startIndex != endIndex);
        }
    }
}
