using System;
using System.Threading.Tasks;

namespace GitLab.Extension.Webviews
{
    public interface IWebviewToolWindowControl
    {
        bool IsViewInitialized { get; }
        Task InitializeAsync(Uri uri);
        void Reload();
    }
}