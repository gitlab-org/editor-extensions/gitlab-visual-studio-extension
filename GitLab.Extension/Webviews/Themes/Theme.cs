using System.Collections.Generic;

namespace GitLab.Extension.Webviews.Themes
{
    public class Theme
    {
        public Theme(Dictionary<string, string> styles)
        {
            Styles = styles;
        }
        
        public Dictionary<string, string> Styles { get; }
    }
}