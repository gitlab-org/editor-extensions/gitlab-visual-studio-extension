using System;
using System.Threading.Tasks;
using GitLab.Extension.LanguageServer.Models;
using GitLab.Extension.Workspace;
using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.PlatformUI;
using Microsoft.VisualStudio.Shell;

namespace GitLab.Extension.Webviews.Themes
{
    public interface IThemeManager
    {
        Theme GetCurrentTheme();
        
        Task<bool> SendThemeUpdateAsync(Theme theme);
    }
    public class ThemeManager: IThemeManager, IDisposable
    {
        private readonly IThemeProvider _themeProvider;
        private readonly IWorkspaceLsClientProvider _lsClientProvider;
        private readonly ILogger<ThemeManager> _logger;
        
        public ThemeManager(
            IThemeProvider themeProvider,
            IWorkspaceLsClientProvider lsClientProvider,
            ILogger<ThemeManager> logger)
        {
            _themeProvider = themeProvider;
            _logger = logger;
            _lsClientProvider = lsClientProvider;
            VSColorTheme.ThemeChanged += HandleThemeChange;
        }

        public void Dispose()
        {
            VSColorTheme.ThemeChanged -= HandleThemeChange;
        }
        
        public Theme GetCurrentTheme()
        {
            ThreadHelper.ThrowIfNotOnUIThread();

            return _themeProvider.GetCurrentTheme();
        }

        public async Task<bool> SendThemeUpdateAsync(Theme theme)
        {
            _logger.LogDebug("Sending theme update to the language server");
            
            var lsClient = await _lsClientProvider.GetClientAsync(await WorkspaceUtilities.GetCurrentWorkspaceIdAsync());

            return await lsClient.SendDidThemeChangeAsync(new DidChangeTheme(theme.Styles));
        }

        private void HandleThemeChange(ThemeChangedEventArgs args)
        {
            _logger.LogDebug("Handling ThemeChanged event");

            var theme = GetCurrentTheme();

            Task.Run(() => SendThemeUpdateAsync(theme));
        }
    }
}