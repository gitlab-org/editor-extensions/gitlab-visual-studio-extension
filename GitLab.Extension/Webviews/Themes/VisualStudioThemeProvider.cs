using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using Microsoft.VisualStudio.PlatformUI;
using Microsoft.VisualStudio.Shell;

namespace GitLab.Extension.Webviews.Themes
{
    public interface IThemeProvider
    {
        Theme GetCurrentTheme();
    }
    public class VisualStudioThemeProvider : IThemeProvider
    {
        private static readonly Dictionary<string, ThemeResourceKey> ThemeMap = new Dictionary<string, ThemeResourceKey>
        {
            // GLOBAL STYLES
            {"--editor-font-family", EnvironmentColors.SystemWindowTextColorKey},
            {"--editor-background", EnvironmentColors.ToolWindowBackgroundColorKey},
            {"--editor-border", EnvironmentColors.ToolWindowBorderColorKey},
            {"--editor-foreground", EnvironmentColors.ToolWindowTextColorKey},
            {"--editor-foreground--muted", EnvironmentColors.CommandBarTextInactiveColorKey},
            {"--editor-foreground--disabled", EnvironmentColors.CommandBarTextInactiveColorKey},

            // BUTTON
            {"--editor-button-background", EnvironmentColors.CommandBarSelectedColorKey},
            {"--editor-button-foreground", EnvironmentColors.CommandBarTextActiveColorKey},
            {"--editor-button-border", EnvironmentColors.CommandBarMenuBorderColorKey},
            {"--editor-button-background--hover", EnvironmentColors.CommandBarHoverColorKey},
            {"--editor-button-foreground--hover", EnvironmentColors.CommandBarTextHoverColorKey},

            // INPUT
            {"--editor-input-background", EnvironmentColors.DropDownBackgroundColorKey},
            {"--editor-input-foreground", EnvironmentColors.DropDownTextColorKey},
            {"--editor-input-border", EnvironmentColors.DropDownBorderColorKey},
            {"--editor-input-background--focus", EnvironmentColors.DropDownMouseDownBackgroundColorKey},
            {"--editor-input-foreground--focus", EnvironmentColors.DropDownMouseOverTextColorKey},
            {"--editor-input-border--focus", EnvironmentColors.DropDownMouseDownBorderColorKey},

            // LINK
            {"--editor-link-foreground", EnvironmentColors.PanelHyperlinkColorKey},
            {"--editor-link-foreground--hover", EnvironmentColors.PanelHyperlinkHoverColorKey},
            {"--editor-link-foreground--focus", EnvironmentColors.PanelHyperlinkPressedColorKey}
        };
        
        public Theme GetCurrentTheme()
        {
            return new Theme(ThemeMap
                .Select(kvp => new KeyValuePair<string, string>(kvp.Key, ToHex(VSColorTheme.GetThemedColor(kvp.Value))))
                .ToDictionary(kvp => kvp.Key, kvp => kvp.Value));
        }
        
        private static string ToHex(Color color) {
            return $"#{color.R:X2}{color.G:X2}{color.B:X2}";
        }
    }
}