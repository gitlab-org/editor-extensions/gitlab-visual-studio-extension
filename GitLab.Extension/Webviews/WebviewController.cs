using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using GitLab.Extension.Workspace;
using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.Shell;
using Microsoft.VisualStudio.Shell.Interop;
using Microsoft.VisualStudio.Threading;

namespace GitLab.Extension.Webviews
{
    public interface IWebviewController
    {
        Task InitializeAsync();
    }
    public interface IWebviewController<T> : IWebviewController where T : ToolWindowPane
    {
        Task ShowAsync();
        void Hide();
        bool IsEnabled { get; }
        bool IsInitialized { get; }
    }
    public abstract class WebviewController<T> : IWebviewController<T>
        where T : ToolWindowPane
    {
        protected readonly AsyncPackage _package;
        protected readonly IWorkspaceLsClientProvider _lsClientProvider;
        protected readonly ILogger<WebviewController<T>> _logger;
        
        protected bool _initialized;
        protected T _toolWindowPane;

        protected WebviewController(
            AsyncPackage package,
            IWorkspaceLsClientProvider lsClientProvider,
            ILogger<WebviewController<T>> logger)
        {
            _package = package;
            _lsClientProvider = lsClientProvider;
            _logger = logger;
        }
        
        protected abstract string WebviewId { get; }
        public abstract bool IsEnabled { get; }
        
        public bool IsInitialized => _initialized;

        public async Task InitializeAsync()
        {
            _initialized = true;
            if (!IsEnabled)
            {
                return;
            }
            _logger.LogInformation("Initializing webview controller <{T}>.", typeof(T));
            
            _toolWindowPane = await GetToolWindowPaneAsync();
            
            if (_toolWindowPane == null)
            {
                await TaskScheduler.Default;
                await CreateAndShowToolWindowPaneAsync();
            }

            await InitializeControlAsync();
        }

        public async Task ShowAsync()
        {
            await ThreadHelper.JoinableTaskFactory.SwitchToMainThreadAsync();
            if (!IsEnabled)
            {
                return;
            }
            
            await CreateAndShowToolWindowPaneAsync();

            await InitializeControlAsync();
        }
        
        public void Hide()
        {
            ThreadHelper.ThrowIfNotOnUIThread();
            (_toolWindowPane.Frame as IVsWindowFrame)?.CloseFrame((uint) __FRAMECLOSE.FRAMECLOSE_NoSave);
        }
        
        public void Reload()
        {
            var control = _toolWindowPane?.Content as IWebviewToolWindowControl;
            
            if (control == null || control.IsViewInitialized)
            {
                return;
            }
            
            control.Reload();
        }

        private async Task InitializeControlAsync()
        {
            var control = _toolWindowPane?.Content as IWebviewToolWindowControl;
            
            if (control == null)
            {
                _logger.LogError("Control of type {T} should be of type {IWebviewToolWindowControl}.", typeof(T), typeof(IWebviewToolWindowControl));
                return;
            }

            if (control.IsViewInitialized)
            {
                _logger.LogInformation("Not initializing view, because {T} view is already initialized.", typeof(T));
                return;
            }
            
            var lsClient = await _lsClientProvider.GetClientAsync(await WorkspaceUtilities.GetCurrentWorkspaceIdAsync());
            var metadata = await lsClient.GetWebviewMetadataAsync();
            var webviewInfo = metadata.FirstOrDefault(info => info.Id.Equals(WebviewId));
            if (webviewInfo == null)
            {
                _logger.LogError("Failed to fetch DuoChat url.");
                return;
            }
            _logger.LogInformation("Available URIs for the DuoChat: {URIs}", webviewInfo.Uris);
            
            await ThreadHelper.JoinableTaskFactory.SwitchToMainThreadAsync();
            
            await control.InitializeAsync(new Uri(webviewInfo.Uris.FirstOrDefault()));
        }
        
        private async Task<T> GetToolWindowPaneAsync()
        {
            return await _package.FindToolWindowAsync(typeof(T), 0, false, CancellationToken.None) as T;
        }
        
        private async Task CreateAndShowToolWindowPaneAsync()
        {
            _toolWindowPane = (T) await _package.ShowToolWindowAsync(
                typeof(T),
                0,
                true,
                _package.DisposalToken);
        }
    }
}