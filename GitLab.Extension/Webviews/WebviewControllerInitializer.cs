using System.Collections.Generic;
using System.Threading.Tasks;
using GitLab.Extension.Webviews.Themes;

namespace GitLab.Extension.Webviews
{
    public interface IWebviewControllerInitializer
    {
        Task InitializeAsync();
    }
    public class WebviewControllerInitializer : IWebviewControllerInitializer
    {
        private readonly IEnumerable<IWebviewController> _webviewControllers;
        private readonly IThemeManager _themeManager;

        public WebviewControllerInitializer(
            IEnumerable<IWebviewController> webviewControllers,
            IThemeManager themeManager)
        {
            _webviewControllers = webviewControllers;
            _themeManager = themeManager;
        }

        public async Task InitializeAsync()
        {
            foreach (var controller in _webviewControllers)
            {
                await controller.InitializeAsync();
            }

            await _themeManager.SendThemeUpdateAsync(_themeManager.GetCurrentTheme());
        }
    }
}