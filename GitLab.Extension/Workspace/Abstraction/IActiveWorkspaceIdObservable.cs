using System;

namespace GitLab.Extension.Workspace
{
    public interface IActiveWorkspaceIdObservable : IObservable<WorkspaceId?>
    {}
}
