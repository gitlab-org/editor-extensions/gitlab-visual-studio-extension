using System;
using GitLab.Extension.Workspace.Model;

namespace GitLab.Extension.Workspace
{
    public interface IThemeManager
    {
        IObservable<ThemeUpdate> ThemeUpdates { get; }
        
        // Triggers theme update for the manager
        void UpdateTheme();
    }
}
