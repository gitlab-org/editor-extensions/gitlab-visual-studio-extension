namespace GitLab.Extension.Workspace
{
    public interface IWorkspaceEventHandler
    {
        void OnWorkspaceOpen(WorkspaceId workspaceId);
        void OnWorkspaceClose(WorkspaceId workspaceId);
    }
}
