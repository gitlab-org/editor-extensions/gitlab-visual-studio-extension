using System.Threading.Tasks;
using GitLab.Extension.LanguageServer;

namespace GitLab.Extension.Workspace
{
    public interface IWorkspaceLsClientProvider
    {
        Task<ILsClient> GetClientAsync(
            WorkspaceId workspaceId);
        
        Task DisposeClientAsync(WorkspaceId workspaceId);
    }
}
