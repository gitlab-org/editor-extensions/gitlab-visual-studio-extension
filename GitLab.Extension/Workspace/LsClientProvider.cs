using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using GitLab.Extension.LanguageServer;

namespace GitLab.Extension.Workspace
{
    public class LsClientProvider : IWorkspaceLsClientProvider
    {
        private readonly Dictionary<WorkspaceId, Task<ILsClient>> _clients;
        private readonly SemaphoreSlim _semaphore = new SemaphoreSlim(1, 1);
        private readonly ILsClientManager _clientManager;

        public LsClientProvider(
            ILsClientManager clientManager)
        {
            _clientManager = clientManager;
            _clients = new Dictionary<WorkspaceId, Task<ILsClient>>();
        }

        public async Task<ILsClient> GetClientAsync(
            WorkspaceId workspaceId)
        {
            await _semaphore.WaitAsync();
            try
            {
                if (_clients.TryGetValue(workspaceId, out var clientTask))
                {
                    return await clientTask;
                }

                var newClientTask = GetAndConnectAsync(workspaceId);
                _clients[workspaceId] = newClientTask;
                return await newClientTask;
            }
            finally
            {
                _semaphore.Release();
            }
        }

        public async Task DisposeClientAsync(WorkspaceId workspaceId)
        {
            await _semaphore.WaitAsync();
            try
            {
                if (_clients.TryGetValue(workspaceId, out var clientTask))
                {
                    await clientTask;
                    await _clientManager.DisposeClientAsync(workspaceId.SolutionName);
                    _clients.Remove(workspaceId);
                }
            }
            finally
            {
                _semaphore.Release();
            }
        }

        private async Task<ILsClient> GetAndConnectAsync(WorkspaceId workspaceId)
        {
            var lsCLient = _clientManager.GetClient(workspaceId.SolutionName, workspaceId.SolutionPath);
            await lsCLient.ConnectAsync();
            return lsCLient;
        }
    }
}
