using Microsoft.VisualStudio.Text.Editor;

namespace GitLab.Extension.Workspace.Model
{
    public class TextDocumentInfo
    {
        public IWpfTextView TextView { get; private set; }
        public WorkspaceId Workspace { get; private set; }
        public string RelativePath { get; private set; }
        public string TypeName { get; private set; }
        
        public TextDocumentInfo(IWpfTextView textView,
            WorkspaceId workspace,
            string relativePath,
            string typeName)
        {
            RelativePath = relativePath;
            Workspace = workspace;
            TypeName = typeName;
            TextView = textView;
        }
    }
}
