using GitLab.Extension.Utility;
using Microsoft.VisualStudio.Shell;
using Microsoft.VisualStudio.Shell.Interop;
using Microsoft.VisualStudio.Text;
using Microsoft.VisualStudio.Text.Editor;
using Microsoft.VisualStudio.TextManager.Interop;

namespace GitLab.Extension.Workspace.Model
{
    public static class TextDocumentInfoExtensions
    {
        public static TextDocumentInfo GetTextViewInfo(
            this IWpfTextView textView,
            WorkspaceId workspaceId)
        {
            var path = GetFilePath(textView);
            
            var relativePath = PathUtils.GetRelativePath(workspaceId.SolutionPath, path);

            return new TextDocumentInfo(textView, workspaceId, relativePath, textView.TextBuffer.ContentType.TypeName);
        }
        
        public static TextDocumentInfo GetTextViewInfo(this ITextBuffer textBuffer,
            ITextDocumentFactoryService textDocumentFactory,
            WorkspaceId workspaceId)
        {
            ThreadHelper.ThrowIfNotOnUIThread();

            textDocumentFactory.TryGetTextDocument(textBuffer, out var textDocument);
            
            if (textDocument == null)
                return null;
            
            var relativePath = PathUtils.GetRelativePath(workspaceId.SolutionPath, textDocument.FilePath);

            return new TextDocumentInfo(null, workspaceId, relativePath, textBuffer.ContentType.TypeName);
        }
        
        public static string GetFilePath(this ITextView textView)
        {
            ThreadHelper.ThrowIfNotOnUIThread();

            textView.TextBuffer.Properties.TryGetProperty(typeof(IVsTextBuffer), out object bufferAdapter);

            var persistFileFormat = bufferAdapter as IPersistFileFormat;
            if (persistFileFormat == null)
            {
                return null;
            }

            persistFileFormat.GetCurFile(out var filepath, out _);
            return string.IsNullOrEmpty(filepath) ? null : filepath;
        }
    }
}
