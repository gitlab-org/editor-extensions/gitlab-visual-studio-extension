using System.Drawing;

namespace GitLab.Extension.Workspace.Model
{
    public sealed class ThemeUpdate
    {
        public Color BackgroundColor { get; }
        public Color StatusBarColor { get; }

        public ThemeUpdate(Color backgroundColor, Color statusBarColor)
        {
            BackgroundColor = backgroundColor;
            StatusBarColor = statusBarColor;
        }
    }
}