using System;
using System.ComponentModel.Composition;
using System.Threading.Tasks;
using Autofac;
using GitLab.Extension.CodeSuggestions;
using GitLab.Extension.CodeSuggestions.Model;
using GitLab.Extension.LanguageServer;
using GitLab.Extension.LanguageServer.Models;
using GitLab.Extension.Workspace.Model;
using Microsoft.VisualStudio.Editor;
using Microsoft.VisualStudio.Shell;
using Microsoft.VisualStudio.Text;
using Microsoft.VisualStudio.Text.Editor;
using Microsoft.VisualStudio.Threading;
using Microsoft.VisualStudio.Utilities;
using Serilog;

namespace GitLab.Extension.Workspace
{
    [Export(typeof(IWpfTextViewCreationListener))]
    [ContentType("code")]
    [TextViewRole(PredefinedTextViewRoles.Editable)]
    public class TextViewListener : IWpfTextViewCreationListener
    {
        private readonly ITextDocumentFactoryService _textDocumentFactory;
        private readonly IWorkspaceLsClientProvider _lsClientProvider;
        private readonly ILanguageManager _languageManager;
        private readonly ISuggestionEventBroker _eventBroker;
        private readonly ILogger _logger;

        [ImportingConstructor]
        public TextViewListener(IVsEditorAdaptersFactoryService adaptersFactory, ITextDocumentFactoryService textDocumentFactory)
        {
            _textDocumentFactory = textDocumentFactory;
            _languageManager = Extension.DependencyInjection.Instance.Scope.Resolve<ILanguageManager>();
            _lsClientProvider = Extension.DependencyInjection.Instance.Scope.Resolve<IWorkspaceLsClientProvider>();
            _eventBroker = Extension.DependencyInjection.Instance.Scope.Resolve<ISuggestionEventBroker>();
            _logger = Extension.DependencyInjection.Instance.Scope.Resolve<ILogger>();
        }
        
        public void TextViewCreated(IWpfTextView textView)
        {
            // This is a workaround to a race condition where the extension package did not start its
            // initialization yet before the listener was registered as the VS component.
            Logging.ConfigureLogging();
            
            var workspaceId = WorkspaceUtilities.GetCurrentWorkspaceId();
            var textDocumentInfo = textView.GetTextViewInfo(workspaceId);

            Task.Run(() => OnTextDocumentOpenedAsync(textDocumentInfo));
            
            textView.TextBuffer.Changed += OnTextDocumentChange;
            textView.GotAggregateFocus += OnTextDocumentFocusChange;
            textView.Closed += OnTextDocumentClosed;
            textView.LayoutChanged += OnTextDocumentLayoutChanged;
        }
        
        private void OnTextDocumentLayoutChanged(object sender, TextViewLayoutChangedEventArgs args)
        {
            var textView = sender as IWpfTextView;

            if (textView == null)
            {
                return;
            }
            
            _eventBroker.Publish(new TextViewLayoutChangedEvent(textView));
        }
        
        private async Task OnTextDocumentOpenedAsync(TextDocumentInfo textDocumentInfo)
        {
            try
            {
                var lsClient = await _lsClientProvider.GetClientAsync(textDocumentInfo.Workspace);
                var language = _languageManager.GetLanguage(textDocumentInfo.TypeName,
                    _languageManager.GetExtensionFromFilename(textDocumentInfo.RelativePath));

                var version = textDocumentInfo.TextView.TextBuffer.CurrentSnapshot.Version.VersionNumber;
                var text = textDocumentInfo.TextView.TextBuffer.CurrentSnapshot.GetText();
                
                await lsClient.SendTextDocumentDidOpenAsync(
                    textDocumentInfo.RelativePath,
                    language.LanguageId,
                    version,
                    text);
                
                // we need to resend this notification as VS may execute OnTextDocumentFocusChange before the document is open
                await SendFocusChanged(textDocumentInfo);
            
                _logger.Debug("Sent document open notification to language server for {FilePath}.", textDocumentInfo.RelativePath);
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Exception occurred while sending open document notification to language server for {FilePath}.", textDocumentInfo.RelativePath);
            }
        }

        private async void OnTextDocumentClosed(object sender, EventArgs args)
        {
            var textDocumentInfo = null as TextDocumentInfo;

            try
            {
                var textView = sender as IWpfTextView;

                if (textView == null)
                {
                    return;
                }
                
                var workspaceId = WorkspaceUtilities.GetCurrentWorkspaceId();
                textDocumentInfo = textView.GetTextViewInfo(workspaceId);
                
                _eventBroker.Publish(new TextViewClosedEvent(textView));

                await TaskScheduler.Default;

                var lsClient = await _lsClientProvider.GetClientAsync(textDocumentInfo.Workspace);

                await lsClient.SendTextDocumentDidCloseAsync(textDocumentInfo.RelativePath);
                _logger.Debug("Closed document {FilePath} and notified language server.",
                    textDocumentInfo.RelativePath);
            }
            catch (Exception ex)
            {
                _logger.Error(ex,
                    "Exception occurred while sending close document notification to language server for {FilePath}.",
                    textDocumentInfo?.RelativePath);
            }
            finally
            {
                try
                {
                    await ThreadHelper.JoinableTaskFactory.SwitchToMainThreadAsync();

                    UnregisterEventHandlers(textDocumentInfo);
                }
                catch (Exception ex)
                {
                    _logger.Error(ex, "Exception occurred while unregistering event handlers for {FilePath}.", textDocumentInfo?.RelativePath);
                }
            }
        }
        
        private async void OnTextDocumentChange(object sender, TextContentChangedEventArgs args)
        {
            var textDocumentInfo = null as TextDocumentInfo;
            
            try
            {
                var textBuffer = sender as ITextBuffer;
                if (textBuffer == null)
                {
                    return;
                }
                var workspaceId = WorkspaceUtilities.GetCurrentWorkspaceId();
                textDocumentInfo = textBuffer.GetTextViewInfo(_textDocumentFactory, workspaceId);

                await TaskScheduler.Default;
                
                var lsClient = await _lsClientProvider.GetClientAsync(textDocumentInfo.Workspace);
                
                _logger.Debug("Text buffer changed for {FilePath}. Processing changes...", textDocumentInfo.RelativePath);
                
                switch(lsClient.TextDocumentSyncKind)
                {
                    case TextDocumentSyncKind.Full:
                        await SendChangedFullAsync(lsClient, textDocumentInfo, args);
                        return;
                    case TextDocumentSyncKind.Incremental:
                        await SendChangedIncrementalAsync(lsClient, textDocumentInfo, args);
                        return;
                    case TextDocumentSyncKind.None:
                    default:
                        return;
                }
            }
            catch (Exception ex)
            {
                // Don't crash visual studio by handling all exceptions
                _logger.Error(ex, "Exception occurred while sending change document notification to language server for {FilePath}.", textDocumentInfo?.RelativePath);
            }
        }

        private async void OnTextDocumentFocusChange(object sender, EventArgs args)
        {
            try
            {
                var textView = sender as IWpfTextView;

                if (textView == null)
                {
                    return;
                }

                var workspaceId = WorkspaceUtilities.GetCurrentWorkspaceId();
                var textDocumentInfo = textView.GetTextViewInfo(workspaceId);

                await TaskScheduler.Default;

                await SendFocusChanged(textDocumentInfo);

                _logger.Debug("Sent didChangeDocumentInActiveEditor notification for {FilePath}", textDocumentInfo.RelativePath);
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Exception occurred while sending didChangeDocumentInActiveEditor notification");
            }
        }

        private async Task SendFocusChanged(TextDocumentInfo textDocumentInfo)
        {
            var lsClient = await _lsClientProvider.GetClientAsync(textDocumentInfo.Workspace);
                
            await lsClient.SendDidChangeDocumentInActiveEditor(textDocumentInfo.RelativePath);
        }

        private async Task SendChangedIncrementalAsync(ILsClient lsClient, TextDocumentInfo textDocumentInfo, TextContentChangedEventArgs e)
        {
            _logger.Debug("Starting to send incremental changes for {FilePath}.", textDocumentInfo.RelativePath);

            if (e.Changes.Count == 0)
            {
                _logger.Debug("No changes to send for {FilePath}.", textDocumentInfo.RelativePath);
                return;
            }
            
            try
            {
                var versionNumber = e.AfterVersion.VersionNumber;

                var changes = new TextDocumentContentChangeEvent[e.Changes.Count];

                for (int changeIndex = 0; changeIndex < e.Changes.Count; changeIndex++)
                {
                    var oldStartPosition = e.Changes[changeIndex].OldSpan.Start;
                    var oldEndPosition = e.Changes[changeIndex].OldSpan.End;
                    var changedText = e.Changes[changeIndex].NewText;

                    var start = PositionToLineAndCharacter(e.Before, oldStartPosition);
                    var end = PositionToLineAndCharacter(e.Before, oldEndPosition);

                    changes[changeIndex] = new TextDocumentContentChangeEvent
                    {
                        Range = new Range
                        {
                            Start = new Position
                            {
                                Line = (uint)start.line,
                                Character = (uint)start.character,
                            },
                            End = new Position
                            {
                                Line = (uint)end.line,
                                Character = (uint)end.character,
                            }
                        },
                        Text = changedText,
                    };
                    
                    _logger.Debug("Recorded change for {FilePath}: Start {StartLine}:{StartChar} End {EndLine}:{EndChar} Text [{Text}].", 
                        textDocumentInfo.RelativePath, start.line, start.character, end.line, end.character, changedText);
                }

                await lsClient.SendTextDocumentDidChangeAsync(
                    textDocumentInfo.RelativePath, versionNumber, changes);
                
                _logger.Debug("Incremental changes sent successfully for {FilePath}.", textDocumentInfo.RelativePath);
            }
            catch (Exception ex)
            {
                // Don't crash visual studio by handling all exceptions
                _logger.Error(ex, "Error sending incremental changes for {FilePath}.", textDocumentInfo.RelativePath);
            }
        }
        
        private async Task SendChangedFullAsync(ILsClient lsClient, TextDocumentInfo textDocumentInfo, TextContentChangedEventArgs e)
        {
            _logger.Debug("Starting to send full text changes for {FilePath}.", textDocumentInfo.RelativePath);
            
            try
            {
                var fullText = e.After.GetText();
                var versionNumber = e.AfterVersion.VersionNumber;
                
                _logger.Debug("Sending full text (length {Length}) to language server for {FilePath} at version {Version}.", 
                    fullText.Length, textDocumentInfo.RelativePath, versionNumber);

                await lsClient.SendTextDocumentDidChangeAsync(
                    textDocumentInfo.RelativePath,
                    versionNumber,
                    fullText);
                
                _logger.Debug("Full text changes sent successfully for {FilePath}.", textDocumentInfo.RelativePath);
            }
            catch (Exception ex)
            {
                // Don't crash visual studio by handling all exceptions
                _logger.Error(ex, "Error sending full text changes for {FilePath}.", textDocumentInfo.RelativePath);
            }
        }
        
        private (int line, int character) PositionToLineAndCharacter(ITextSnapshot snapshot, int position)
        {
            var line = snapshot.GetLineFromPosition(position);
            var lineNumber = line.LineNumber;
            var character = position - line.Start;

            return (lineNumber, character);
        }

        private void UnregisterEventHandlers(TextDocumentInfo textDocumentInfo)
        {
            if (textDocumentInfo?.TextView== null) return;
            
            textDocumentInfo.TextView.Closed -= OnTextDocumentClosed;
            textDocumentInfo.TextView.TextBuffer.Changed -= OnTextDocumentChange;
            textDocumentInfo.TextView.GotAggregateFocus -= OnTextDocumentFocusChange;
            textDocumentInfo.TextView.LayoutChanged -= OnTextDocumentLayoutChanged;
        }
    }
}
