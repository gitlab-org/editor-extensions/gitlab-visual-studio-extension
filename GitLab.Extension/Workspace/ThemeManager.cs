using System;
using System.Reactive.Subjects;
using GitLab.Extension.Workspace.Model;
using Microsoft.VisualStudio.PlatformUI;

namespace GitLab.Extension.Workspace
{
    public class ThemeManager : IThemeManager, IDisposable
    {
        private readonly Subject<ThemeUpdate> _themeUpdateSubject = new Subject<ThemeUpdate>();

        public IObservable<ThemeUpdate> ThemeUpdates => _themeUpdateSubject;
        public void UpdateTheme()
        {
            NotifyThemeUpdate();
        }

        public ThemeManager()
        {
            VSColorTheme.ThemeChanged += HandleThemeChange;
        }
        
        public void Dispose()
        {
            VSColorTheme.ThemeChanged -= HandleThemeChange;
            _themeUpdateSubject.Dispose();
        }

        private void HandleThemeChange(ThemeChangedEventArgs args)
        {
            NotifyThemeUpdate();
        }
        
        private void NotifyThemeUpdate()
        {
            _themeUpdateSubject.OnNext(new ThemeUpdate
            (
                VSColorTheme.GetThemedColor(EnvironmentColors.SystemBackgroundColorKey),
                VSColorTheme.GetThemedColor(EnvironmentColors.StatusBarDefaultColorKey)
            ));
        }
    }
}
