<!-- markdownlint-disable MD013 -->

# <img src="https://gitlab.com/gitlab-org/editor-extensions/gitlab-visual-studio-extension/-/raw/18c5fd65285b4c2ae30356e81105854ee7ef5213/GitLab.Extension/gitlab-logo.png" width="64" align="center" alt="GitLab logo"/> GitLab Extension for Visual Studio IDE

[This extension](https://gitlab.com/gitlab-org/editor-extensions/gitlab-visual-studio-extension) integrates GitLab Duo
with Visual Studio (Community, Pro, and Enterprise). It supports GitLab Duo Code Suggestions and GitLab Duo Chat.

With **Code Suggestions**, you get:

- **Code completion**, which suggests completions for the current line you are typing.
  These suggestions usually have low latency.
- **Code generation**, which generates code based on a natural-language code comment block.
  Responses for code generation are slower than code completion, and returned in a single block.

With **Chat**, you get the ability to ask general or GitLab specific questions in the main chat window, as well as the following slash commands:

- **Explain code**, which provides an explanation of selected code snippets from your editor.
- **Refactor code**, which provides refactoring suggestions for selected code snippets from your editor.
- **Generate tests**, which provides test generation suggestions for selected code snippets from your editor.
- **Fix code**, which provides quick fixes for selected code snippets from your editor.

To learn more, see:

- [GitLab Duo Chat](https://docs.gitlab.com/ee/user/gitlab_duo_chat/)
- [Code Suggestions documentation](https://docs.gitlab.com/ee/user/project/repository/code_suggestions/)
- [Supported languages](https://docs.gitlab.com/ee/user/project/repository/code_suggestions/supported_extensions.html#supported-languages)

## Version compatibility

This extension requires:

- Visual Studio 2022 version 17.6 or later.
- GitLab version 16.1 and later.
  - GitLab Duo Code Suggestions requires GitLab version 16.8 or later.

Visual Studio for Mac is not supported.

## Setup

Download the extension from the [Visual Studio Marketplace](https://marketplace.visualstudio.com/items?itemName=GitLab.GitLabExtensionForVisualStudio).

For setup instructions, see [Configure the extension](https://docs.gitlab.com/ee/editor_extensions/visual_studio/#configure-the-extension).

## Visual Studio Code

For the _Visual Studio Code_ extension, see
[GitLab Workflow](https://marketplace.visualstudio.com/items?itemName=GitLab.gitlab-workflow).

## Roadmap

To learn more about this project's team, processes, and plans, see
the [Create:Editor Extensions Group](https://handbook.gitlab.com/handbook/engineering/development/dev/create/editor-extensions/)
page in the GitLab handbook.

For a list of all open issues in this project, see the
[issues page](https://gitlab.com/gitlab-org/editor-extensions/gitlab-visual-studio-extension/-/issues/)
for this project.

## Troubleshooting

See the [troubleshooting documentation](https://docs.gitlab.com/ee/editor_extensions/visual_studio/visual_studio_troubleshooting.html)
in the GitLab documentation. If those instructions don't solve your problem, [report an issue in the project](https://gitlab.com/gitlab-org/editor-extensions/gitlab-visual-studio-extension/-/issues/new).

## Feedback

We'd love to hear from you. If you've found a bug, or have an idea,
[open an issue](https://gitlab.com/gitlab-org/editor-extensions/gitlab-visual-studio-extension/-/issues/new).

## Contributing

This extension is open source and
[hosted on GitLab](https://gitlab.com/gitlab-org/editor-extensions/gitlab-visual-studio-extension).
Contributions are more than welcome and subject to the terms set forth in
[CONTRIBUTING](https://gitlab.com/gitlab-org/editor-extensions/gitlab-visual-studio-extension/-/blob/main/CONTRIBUTING.md).
Feel free to fork and add new features or submit bug reports. See
[CONTRIBUTING](https://gitlab.com/gitlab-org/editor-extensions/gitlab-visual-studio-extension/-/blob/main/CONTRIBUTING.md)
for more information.
