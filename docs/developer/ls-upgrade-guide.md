# Language Server upgrade guide

Language Server binaries are installed with the NuGet package
`GitLab.LanguageServer`. The version of the NuGet matches the
version of the Language Server.
