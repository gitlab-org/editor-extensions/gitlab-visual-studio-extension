# Troubleshooting

Troubleshooting information for this extension is now available
[in the GitLab documentation](https://docs.gitlab.com/ee/editor_extensions/visual_studio/visual_studio_troubleshooting.html).
